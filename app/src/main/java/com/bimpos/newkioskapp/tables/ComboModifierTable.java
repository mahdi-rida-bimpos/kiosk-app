package com.bimpos.newkioskapp.tables;

public class ComboModifierTable {

    public static final String TABLE_NAME = "comboModifiers";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String PROD_NUM = "prodNum";
        public static final String DESCRIPTION = "description";
        public static final String PRICE = "price";
        public static final String COMBO_HEADER_ID = "comboHeaderId";
        public static final String COMBO_GROUP_ID = "comboGroupId";
        public static final String SEQ = "seq";
        public static final String TAX_1 = "tax1";
        public static final String TAX_2 = "tax2";
        public static final String TAX_3 = "tax3";
        public static final String TAX_EX = "taxEx";
        public static final String AVG_COST = "avgCost";
        public static final String LAST_COST = "lastCost";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
