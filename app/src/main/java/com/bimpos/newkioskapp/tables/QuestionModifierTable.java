package com.bimpos.newkioskapp.tables;

public class QuestionModifierTable {

    public static final String TABLE_NAME = "questionModifiers";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String PROD_NUM = "prodNum";
        public static final String DESCRIPTION = "description";
        public static final String PRICE = "price";
        public static final String SEQ = "seq";
        public static final String QUESTION_HEADER_ID = "questionHeaderId";
        public static final String QUESTION_GROUP_ID = "questionGroupId";
        public static final String TAX_1 = "tax1";
        public static final String TAX_2 = "tax2";
        public static final String TAX_3 = "tax3";
        public static final String TAX_EX = "taxEx";
        public static final String AVG_COST = "avgCost";
        public static final String LAST_COST = "lastCost";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
