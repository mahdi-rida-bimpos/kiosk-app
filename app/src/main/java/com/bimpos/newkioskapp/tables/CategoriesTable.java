package com.bimpos.newkioskapp.tables;

public class CategoriesTable {

    public static final String TABLE_NAME= "categories";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String CAT_ID = "catId";
        public static final String DESCRIPTION = "description";
        public static final String HEX_PICTURE = "hexPicture";
        public static final String SEQUENCE = "seq";
        public static final String SELECTED = "selected";

        private Columns() {
        }
    }
}
