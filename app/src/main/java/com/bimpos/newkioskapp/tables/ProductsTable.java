package com.bimpos.newkioskapp.tables;

public class ProductsTable {

    public static final String TABLE_NAME = "products";

    public static class Columns {
        public static final String INTERNAL_ID = "Internal_ID";
        public static final String PROD_NUM = "prodNum";
        public static final String CAT_ID = "catId";
        public static final String DESCRIPTION = "description";
        public static final String PROD_INFO = "prodInfo";
        public static final String PRICE = "price";
        public static final String AVG_COST = "avgCost";
        public static final String LAST_COST = "lastCost";
        public static final String HEX_PICTURE = "picture";
        public static final String QUESTION_GROUP_ID = "questionGroupId";
        public static final String IS_COMBO = "isCombo";
        public static final String TAX_1 = "tax1";
        public static final String TAX_2 = "tax2";
        public static final String TAX_3 = "tax3";
        public static final String TAX_EX = "taxEx";

        private Columns() {
            //private constructor to prevent instantiation
        }
    }
}
