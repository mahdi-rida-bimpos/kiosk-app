package com.bimpos.newkioskapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.bimpos.newkioskapp.networkRequests.ConnectionSingleton;


public class NetworkChangeReceiver extends BroadcastReceiver {
    public ConnectivityReceiverListener connectivityReceiverListener;

    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }

    public NetworkChangeReceiver(ConnectivityReceiverListener listener) {
        this.connectivityReceiverListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent arg1) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();

        if (connectivityReceiverListener != null) {
            if(!isConnected){
                ConnectionSingleton.resetConnection();
            }
            connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
        }
    }
}