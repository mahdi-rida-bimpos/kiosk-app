package com.bimpos.newkioskapp.printing;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.bimpos.newkioskapp.BuildConfig;
import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.Helpers.Constant;
import com.bimpos.newkioskapp.Helpers.FormatPrice;
import com.bimpos.newkioskapp.Helpers.GetBitmap;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.activities.MainActivity;
import com.bimpos.newkioskapp.models.Cart;
import com.bimpos.newkioskapp.models.CartComboModifier;
import com.bimpos.newkioskapp.models.CartProduct;
import com.bimpos.newkioskapp.models.CartQuestionModifier;
import com.caysn.autoreplyprint.AutoReplyPrint;
import com.google.zxing.WriterException;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.LongByReference;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class PrinterClass {

    private MainActivity activity;
    private Cart cart;
    private int transactId;
    private SharedPreferences preferences;
    private static final String TAG = "PrinterClass";

    public static class PortParam {
        public boolean bUSB = true;
        public String strUSBPort = "VID:0x4B43,PID:0x3830";
    }

    public void printReceipt(MainActivity activity, int transactId) {
        Log.d(TAG, "printReceipt: start");
        try {
            this.activity = activity;
            this.transactId = transactId;
            preferences = activity.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
            cart = Cart.getCartData();
            Log.d(TAG, "printReceipt: cart size " + cart.getCartItemsList().size());
            Method m = PrinterClass.class.getDeclaredMethod("Test_Pos_SampleTicket_58MM_1", Activity.class, PrinterClass.PortParam.class);
            final PrinterClass.PortParam port = new PrinterClass.PortParam();
            m.invoke(this, activity, port);
        } catch (Throwable tr) {
            Log.d(TAG, "run: error " + tr.getMessage() + ", " + tr.getLocalizedMessage());
        }
    }

    private Pointer OpenPort(Activity ctx, PortParam port) {
        Log.d(TAG, "OpenPort: start");
        Pointer h;
        h = AutoReplyPrint.INSTANCE.CP_Port_OpenUsb("VID:0x4B43,PID:0x3830", 1);
//        AutoReplyPrint.INSTANCE.CP_Printer_AddOnPrinterStatusEvent(status_callback, h);
        return h;
    }

    AutoReplyPrint.CP_OnPrinterStatusEvent_Callback status_callback = new AutoReplyPrint.CP_OnPrinterStatusEvent_Callback() {
        @Override
        public void CP_OnPrinterStatusEvent(Pointer h, final long printer_error_status, final long printer_info_status, Pointer private_data) {
            Log.d(TAG, "CP_OnPrinterStatusEvent: start " + printer_error_status);
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (printer_error_status != 0) {
                        if (activity.dialog.isAdded()) {
                            Log.d(TAG, "CP_OnPrinterStatusEvent: set false");
                            activity.dialog.changePositiveButton(false);
                        }
                    } else {
                        if (activity.dialog.isAdded()) {
                            Log.d(TAG, "CP_OnPrinterStatusEvent: set true");
                            activity.dialog.changePositiveButton(true);
                        }
                    }
                }
            });
        }
    };

    private void QueryPrintResult(Activity ctx, Pointer h) {

        boolean result = AutoReplyPrint.INSTANCE.CP_Pos_QueryPrintResult(h, 30000);
        Log.d(TAG, "QueryPrintResult: start result " + result);
        if (!result) {
            LongByReference printer_error_status = new LongByReference();
            LongByReference printer__info_status = new LongByReference();
            LongByReference timestamp_ms_printer_status = new LongByReference();
            if (AutoReplyPrint.INSTANCE.CP_Printer_GetPrinterStatusInfo(h, printer_error_status, printer__info_status, timestamp_ms_printer_status)) {
                AutoReplyPrint.CP_PrinterStatus status = new AutoReplyPrint.CP_PrinterStatus(printer_error_status.getValue(), printer__info_status.getValue());
                String error_status_string = String.format("Printer Error Status: 0x%04X", printer_error_status.getValue() & 0xffff);
                if (status.ERROR_OCCURED()) {
                    if (status.ERROR_CUTTER())
                        error_status_string += "[CUTTER PROBLEM]";
                    if (status.ERROR_FLASH())
                        error_status_string += "[FLASH ERROR]";
                    if (status.ERROR_NOPAPER())
                        error_status_string += "[NO PAPER IN PRINTER]";
                    if (status.ERROR_VOLTAGE())
                        error_status_string += "[VOLTAGE ERROR]";
                    if (status.ERROR_MARKER())
                        error_status_string += "[MARKER ERROR]";
                    if (status.ERROR_ENGINE())
                        error_status_string += "[ENGINE ERROR]";
                    if (status.ERROR_OVERHEAT())
                        error_status_string += "[OVERHEAT ERROR]";
                    if (status.ERROR_COVERUP())
                        error_status_string += "[COVERUP ERROR]";
                    if (status.ERROR_MOTOR())
                        error_status_string += "[MOTOR ERROR]";
                }
                sendMessage(0, error_status_string);
            } else {
                Log.d(TAG, "QueryPrintResult: failed");
                sendMessage(0, "Printer failed to start");
            }
        } else {
            Log.d(TAG, "QueryPrintResult: success");
            AutoReplyPrint.INSTANCE.CP_Port_Close(h);
//            AutoReplyPrint.INSTANCE.CP_Printer_RemoveOnPrinterStatusEvent(status_callback);
            sendMessage(1, "success");
        }
    }

    private void sendMessage(int status, String message) {
        Intent intent = new Intent(Constant.RECEIVER_PRINTER);
        intent.putExtra("status", status);
        intent.putExtra("message", message);
        activity.sendBroadcast(intent);
    }

    void Test_Pos_SampleTicket_58MM_1(Activity ctx, PortParam port) {
        Log.d(TAG, "Test_Pos_SampleTicket_58MM_1: start");
        Pointer h = OpenPort(ctx, port);
        if (h != Pointer.NULL) {
            AutoReplyPrint.INSTANCE.CP_Pos_ResetPrinter(h);
            AutoReplyPrint.INSTANCE.CP_Printer_ClearPrinterBuffer(h);
            AutoReplyPrint.INSTANCE.CP_Pos_SetMultiByteMode(h);
            AutoReplyPrint.INSTANCE.CP_Pos_SetMultiByteEncoding(h, AutoReplyPrint.CP_MultiByteEncoding_UTF8);
            AutoReplyPrint.INSTANCE.CP_Pos_SetAsciiTextFontType(h, 0);

            printImage(h);

            AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 0);

            AutoReplyPrint.INSTANCE.CP_Pos_SetAlignment(h, AutoReplyPrint.CP_Pos_Alignment_Left);
            AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "Order: " + transactId);
            AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);
            AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "Order Date: " + AppSettings.TIM_STRING);
            AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);
            AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "Order type: " + AppSettings.ORDER_TYPE_MAP.get(cart.getOrderType()));
            AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);

            if (preferences.getInt(Constant.PRINTER_ARABIC, 0) == 0) {
                printDefaultReceipt(h);
            } else {
                printArabicReceipt(h);
            }

            AutoReplyPrint.INSTANCE.CP_Pos_SetTextScale(h, 0, 0);

            if (AppSettings.TAX1_AMOUNT > 0 && cart.getTax1() > 0) {
                AutoReplyPrint.INSTANCE.CP_Pos_SetAlignment(h, AutoReplyPrint.CP_Pos_Alignment_Left);
                AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 0);
                AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "Total VAT " + AppSettings.TAX1_AMOUNT + "%: " + FormatPrice.getFormattedPrice(cart.getTax1()));
                AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);
                AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "------------------------------------------------");
                AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);
            }

            AutoReplyPrint.INSTANCE.CP_Pos_SetTextScale(h, 1, 1);
            AutoReplyPrint.INSTANCE.CP_Pos_SetAlignment(h, AutoReplyPrint.CP_Pos_Alignment_HCenter);
            AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "Total: " + FormatPrice.getFormattedPrice(cart.getCartTotalPrice()));

            AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 3);

            if (preferences.getInt(Constant.ENABLE_QR_CODE, 1) == 1) {
                AutoReplyPrint.INSTANCE.CP_Pos_SetAlignment(h, AutoReplyPrint.CP_Pos_Alignment_HCenter);
                AutoReplyPrint.INSTANCE.CP_Pos_SetBarcodeUnitWidth(h, 10);
                AutoReplyPrint.INSTANCE.CP_Pos_SetBarcodeHeight(h, 10);
                AutoReplyPrint.INSTANCE.CP_Pos_PrintQRCode(h, 0, AutoReplyPrint.CP_QRCodeECC_H, "Mahdi Rida");
                AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 2);
            }

            AutoReplyPrint.INSTANCE.CP_Pos_SetAlignment(h, AutoReplyPrint.CP_Pos_Alignment_HCenter);
            AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, AppSettings.SHORT_TICKET_ID + "\n");

            AutoReplyPrint.INSTANCE.CP_Pos_Beep(h, 1, 500);
            AutoReplyPrint.INSTANCE.CP_Pos_FeedAndHalfCutPaper(h);
            QueryPrintResult(ctx, h);
        }
    }

    private void printDefaultReceipt(Pointer h) {
        AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "================================================");
        AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);
        AutoReplyPrint.INSTANCE.CP_Pos_SetTextBold(h, 1);
        AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 0);
        AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "Quan");

        AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 80);
        AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "Description");

        AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 500);
        AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "Price");
        AutoReplyPrint.INSTANCE.CP_Pos_SetTextBold(h, 0);
        AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);
        AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "================================================");
        AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);

        for (int i = 0; i < cart.getCartItemsList().size(); i++) {

            CartProduct cartProduct = cart.getCartItemsList().get(i);

            AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 0);
            AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, String.valueOf(cartProduct.getQty()));

            AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 80);
            if (isProbablyArabic(cartProduct.getProd_desc())) {
                AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "---");
            } else {
                AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, formatString(cartProduct.getProd_desc()));
            }

            String price = FormatPrice.getReceiptPrice(cartProduct.getPrice());
            AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 550 - (price.length() * 10));
            AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, price);
            AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);

            if (cartProduct.getQuestionList().size() != 0) {
                for (int ii = 0; ii < cartProduct.getQuestionList().size(); ii++) {
                    for (int iii = 0; iii < cartProduct.getQuestionList().get(ii).getCartQuestionModifiers().size(); iii++) {
                        CartQuestionModifier modifier = cartProduct.getQuestionList().get(ii).getCartQuestionModifiers().get(iii);
                        if (modifier.getPrice() == 0) {
                            AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 80);
                            if (isProbablyArabic(modifier.getDescription())) {
                                AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "---");
                            } else {
                                AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "  " + formatString(modifier.getDescription()));
                            }
                        } else {
                            AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 80);
                            if (isProbablyArabic(modifier.getDescription())) {
                                AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "---");
                            } else {
                                AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "  " + formatString(modifier.getDescription()));
                            }
                            price = FormatPrice.getReceiptPrice(modifier.getPrice());
                            AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 550 - price.length() * 10);
                            AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, price);
                        }
                        AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);
                    }
                }
            }

            if (cartProduct.getComboList().size() != 0) {
                for (int ii = 0; ii < cartProduct.getComboList().size(); ii++) {
                    for (int iii = 0; iii < cartProduct.getComboList().get(ii).getCartComboModifiers().size(); iii++) {
                        CartComboModifier modifier = cartProduct.getComboList().get(ii).getCartComboModifiers().get(iii);
                        if (modifier.getPrice() == 0) {
                            AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 0);
                            AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, String.valueOf(modifier.getCount()));
                            AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 80);
                            if (isProbablyArabic(modifier.getDescription())) {
                                AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "---");
                            } else {
                                AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "  " + formatString(modifier.getDescription()));
                            }
                        } else {
                            AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 0);
                            AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, String.valueOf(modifier.getCount()));
                            AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 80);
                            if (isProbablyArabic(modifier.getDescription())) {
                                AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "---");
                            } else {
                                AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "  " + formatString(modifier.getDescription()));
                            }
                            price = FormatPrice.getReceiptPrice(modifier.getPrice());
                            AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 550 - price.length() * 10);
                            AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, price);
                        }
                        AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);
                    }
                }
            }

            if(cartProduct.getRemark().length()>0){
                AutoReplyPrint.INSTANCE.CP_Pos_SetHorizontalAbsolutePrintPosition(h, 80);
                AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "  " + formatString(cartProduct.getRemark()));
                AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);
            }

            AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, "------------------------------------------------");
            AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);

        }
    }

    private void printArabicReceipt(Pointer h) {
        Log.d(TAG, "testArabic: start");
        SalesTicket ticket = new SalesTicket();

        for (int i = 0; i < cart.getCartItemsList().size(); i++) {

            CartProduct cartProduct = cart.getCartItemsList().get(i);

            ticket.items.add(
                    new SalesItem(
                            String.valueOf(cartProduct.getQty()),
                            cartProduct.getProd_desc(),
                            FormatPrice.getReceiptPrice(cartProduct.getPrice()),
                            0));

            if (cartProduct.getQuestionList().size() != 0) {
                for (int ii = 0; ii < cartProduct.getQuestionList().size(); ii++) {
                    for (int iii = 0; iii < cartProduct.getQuestionList().get(ii).getCartQuestionModifiers().size(); iii++) {
                        CartQuestionModifier modifier = cartProduct.getQuestionList().get(ii).getCartQuestionModifiers().get(iii);
                        if (modifier.getPrice() == 0) {
                            ticket.items.add(
                                    new SalesItem(
                                            "",
                                            " " + modifier.getDescription(),
                                            "",
                                            1));
                        } else {
                            ticket.items.add(
                                    new SalesItem(
                                            "",
                                            " " + modifier.getDescription(),
                                            FormatPrice.getReceiptPrice(modifier.getPrice()),
                                            1));
                        }
                    }
                }
            }

            if (cartProduct.getComboList().size() != 0) {
                for (int ii = 0; ii < cartProduct.getComboList().size(); ii++) {
                    for (int iii = 0; iii < cartProduct.getComboList().get(ii).getCartComboModifiers().size(); iii++) {
                        CartComboModifier modifier = cartProduct.getComboList().get(ii).getCartComboModifiers().get(iii);
                        if (modifier.getPrice() == 0) {
                            ticket.items.add(
                                    new SalesItem(
                                            String.valueOf(modifier.getCount()),
                                            " " + modifier.getDescription(),
                                            "",
                                            1));
                        } else {
                            ticket.items.add(
                                    new SalesItem(
                                            String.valueOf(modifier.getCount()),
                                            " " + modifier.getDescription(),
                                            FormatPrice.getReceiptPrice(modifier.getPrice()),
                                            1));
                        }
                    }
                }
            }

            if(cartProduct.getRemark().length()>0){
                ticket.items.add(
                        new SalesItem(
                                "",
                                " "+cartProduct.getRemark(),
                                "",
                                1));
            }
        }

        Bitmap bitmap = getSampleBitmap(ticket);

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        AutoReplyPrint.INSTANCE.CP_Pos_SetAlignment(h, AutoReplyPrint.CP_Pos_Alignment_HCenter);
        AutoReplyPrint.CP_Pos_PrintRasterImageFromData_Helper.PrintRasterImageFromBitmap(h, width, height, bitmap, AutoReplyPrint.CP_ImageBinarizationMethod_Thresholding, AutoReplyPrint.CP_ImageCompressionMethod_None);

    }

    private void printImage(Pointer h) {
        Bitmap bitmap;
        if (AppSettings.RESTAURANT_IMAGE == null) {
            bitmap = getImageFromAssetsFile("RasterImage/bimposlogo.png");
        } else {
            bitmap = GetBitmap.getImageBitmap(AppSettings.RESTAURANT_IMAGE);
        }
        int dstw = 150;
        int dsth = 150;

        AutoReplyPrint.INSTANCE.CP_Pos_SetAlignment(h, AutoReplyPrint.CP_Pos_Alignment_HCenter);
        AutoReplyPrint.CP_Pos_PrintRasterImageFromData_Helper.PrintRasterImageFromBitmap(h, dstw, dsth, bitmap, AutoReplyPrint.CP_ImageBinarizationMethod_Thresholding, AutoReplyPrint.CP_ImageCompressionMethod_None);
        AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 1);
        AutoReplyPrint.INSTANCE.CP_Pos_SetTextScale(h, 1, 1);
        AutoReplyPrint.INSTANCE.CP_Pos_PrintText(h, AppSettings.RESTAURANT_NAME);
        AutoReplyPrint.INSTANCE.CP_Pos_SetTextScale(h, 0, 0);
        AutoReplyPrint.INSTANCE.CP_Pos_FeedLine(h, 2);
    }

    public Bitmap getImageFromAssetsFile(String fileName) {
        Bitmap image = null;
        AssetManager am = activity.getResources().getAssets();
        try {
            InputStream is = am.open(fileName);
            image = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            Log.d(TAG, "getImageFromAssetsFile: error " + e.getMessage());
        }
        return image;
    }

    private String formatString(String prod_desc) {
        if (prod_desc.length() >= 30) {
            return prod_desc.substring(0, 26) + "...";
        } else {
            return prod_desc;
        }
    }

    public static boolean isProbablyArabic(String s) {
        for (int i = 0; i < s.length(); ) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06E0)
                return true;
            i += Character.charCount(c);
        }
        return false;
    }

    public static class SalesItem {
        public int type = 0;
        public String name = "";
        public String price = "";
        public String qty = "";

        public SalesItem() {
        }

        public SalesItem(String qty, String name, String price, int type) {
            this.name = name;
            this.price = price;
            this.qty = qty;
            this.type = type;
        }
    }

    public static class SalesTicket {
        public int width = 576;
        public String head = "Quan   Description                     Price";
        public String doubleLine = "============================================";
        public String singleLine = "--------------------------------------------";
        public List<SalesItem> items = new ArrayList<>();
    }

    public Bitmap getSampleBitmap(@NonNull SalesTicket ticket) {
        Log.d(TAG, "getSampleBitmap: start");
        Typeface tfZFullGB = Typeface.createFromAsset(activity.getAssets(), "fonts" + File.separator + "Zfull-GB.ttf");

        // Head
        TextPaint headPaint = new TextPaint();
        headPaint.setTypeface(tfZFullGB);
        headPaint.setColor(Color.BLACK);
        headPaint.setTextSize(26);
        StaticLayout headLayout = new StaticLayout(ticket.head, headPaint, ticket.width, Layout.Alignment.ALIGN_NORMAL, 1, 0, false);

        TextPaint doubleLine = new TextPaint();
        doubleLine.setTypeface(tfZFullGB);
        doubleLine.setColor(Color.BLACK);
        doubleLine.setTextSize(24);
        StaticLayout doubleLineLayout = new StaticLayout(ticket.doubleLine, doubleLine, ticket.width, Layout.Alignment.ALIGN_NORMAL, 1, 0, false);

        TextPaint singleLine = new TextPaint();
        singleLine.setTypeface(tfZFullGB);
        singleLine.setColor(Color.BLACK);
        singleLine.setTextSize(24);
        StaticLayout singleLinLayout = new StaticLayout(ticket.singleLine, singleLine, ticket.width, Layout.Alignment.ALIGN_NORMAL, 1, 0, false);

        TextPaint productPaint = new TextPaint();
        productPaint.setTypeface(tfZFullGB);
        productPaint.setColor(Color.BLACK);
        productPaint.setTextSize(25);


        List<StaticLayout> itemNameLayoutList = new ArrayList<>();
        List<StaticLayout> itemPriceLayoutList = new ArrayList<>();
        List<StaticLayout> itemAmountLayoutList = new ArrayList<>();


        int itemPriceWidth = 150;//(int) (0.2 * ticket.width);
        int itemAmountWidth = 88;//(int) (0.2 * ticket.width);
        int itemNameWidth = ticket.width - itemPriceWidth - itemAmountWidth;

        for (SalesItem salesItem : ticket.items) {
            StaticLayout itemNameLayout = StaticLayout.Builder.obtain(salesItem.name, 0, salesItem.name.length(), productPaint, itemNameWidth)
                    .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                    .setLineSpacing(0, 1)
                    .setIncludePad(false)
                    .setTextDirection(TextDirectionHeuristics.LTR)
                    .build();
            StaticLayout itemAmountLayout = new StaticLayout(salesItem.qty, productPaint, itemAmountWidth, Layout.Alignment.ALIGN_NORMAL, 1, 0, false);
//            StaticLayout itemNameLayout = new StaticLayout(salesItem.name, productPaint, itemNameWidth, Layout.Alignment.ALIGN_NORMAL, 1, 0, false);
            StaticLayout itemPriceLayout = new StaticLayout(salesItem.price, productPaint, itemPriceWidth, Layout.Alignment.ALIGN_OPPOSITE, 1, 0, false);

            itemNameLayoutList.add(itemNameLayout);
            itemPriceLayoutList.add(itemPriceLayout);
            itemAmountLayoutList.add(itemAmountLayout);
        }

        TextPaint spaceLayout = new TextPaint();
        spaceLayout.setTypeface(tfZFullGB);
        spaceLayout.setColor(Color.BLACK);
        spaceLayout.setTextSize(10);
        StaticLayout spaceStaticLayout = new StaticLayout("\n", spaceLayout, ticket.width, Layout.Alignment.ALIGN_OPPOSITE, 1, 0, false);

        TextPaint itemTotalPaint = new TextPaint();
        itemTotalPaint.setTypeface(tfZFullGB);
        itemTotalPaint.setColor(Color.BLACK);
        itemTotalPaint.setTextSize(30);
        StaticLayout itemTotalLayout = new StaticLayout("", itemTotalPaint, ticket.width, Layout.Alignment.ALIGN_OPPOSITE, 1, 0, false);


        int bitmapWidth = ticket.width;
        int bitmapHeight = itemTotalLayout.getHeight();
        bitmapHeight += spaceStaticLayout.getHeight()*2;

        for (int row = 0; row < ticket.items.size(); row++) {
            bitmapHeight += itemNameLayoutList.get(row).getHeight();
            bitmapHeight += spaceStaticLayout.getHeight();
            if (row + 1 == ticket.items.size()) {
                bitmapHeight += singleLinLayout.getHeight();
            } else if (((row + 1) < ticket.items.size()) && ticket.items.get(row + 1).type == 0) {
                bitmapHeight += singleLinLayout.getHeight();
            }
        }
        bitmapHeight += doubleLineLayout.getHeight() * 2;

        Bitmap bitmap = Bitmap.createBitmap(bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);


        doubleLineLayout.draw(canvas);
        canvas.translate(0, doubleLineLayout.getHeight());

        spaceStaticLayout.draw(canvas);
        canvas.translate(0, spaceStaticLayout.getHeight());

        headLayout.draw(canvas);
        canvas.translate(0, headLayout.getHeight());

        spaceStaticLayout.draw(canvas);
        canvas.translate(0, spaceStaticLayout.getHeight());

        doubleLineLayout.draw(canvas);
        canvas.translate(0, doubleLineLayout.getHeight());

        for (int row = 0; row < ticket.items.size(); ++row) {

            StaticLayout itemNameLayout = itemNameLayoutList.get(row);
            StaticLayout itemPriceLayout = itemPriceLayoutList.get(row);
            StaticLayout itemAmountLayout = itemAmountLayoutList.get(row);

            itemAmountLayout.draw(canvas);
            canvas.translate(itemAmountLayout.getWidth(), 0);

            itemNameLayout.draw(canvas);
            canvas.translate(itemNameLayout.getWidth(), 0);

            itemPriceLayout.draw(canvas);
            canvas.translate(-itemNameLayout.getWidth() - itemAmountLayout.getWidth(), itemNameLayout.getHeight());

            spaceStaticLayout.draw(canvas);
            canvas.translate(0, spaceStaticLayout.getHeight());

            if (row + 1 == ticket.items.size()) {
                singleLinLayout.draw(canvas);
                canvas.translate(0, singleLinLayout.getHeight());
            } else if (((row + 1) < ticket.items.size()) && ticket.items.get(row + 1).type == 0) {
                singleLinLayout.draw(canvas);
                canvas.translate(0, singleLinLayout.getHeight());
            }


        }

        // 绘制列表项总金额
        itemTotalLayout.draw(canvas);
        canvas.translate(0, itemTotalLayout.getHeight());

        return bitmap;
    }


}
