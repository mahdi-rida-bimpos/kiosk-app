package com.bimpos.newkioskapp.Helpers;

import android.content.Context;
import android.text.SpannableString;
import android.util.Log;

import java.text.DecimalFormat;


public class FormatPrice {

    private static final String TAG = "FormatPrice";

//    public static String getFormattedPrice(int price) {
//
//        String formattedPrice = String.valueOf(price);
//        StringBuilder builder = new StringBuilder(formattedPrice);
//        Log.d(TAG, "getFormattedPrice: builder " + builder.toString());
//        if (Constant.SETTINGS_PRICE_DELIMITER == 1) {
//            if (builder.length() >= 4) {
//                for (int i = builder.length() - 3; i >= 1; i = i - 3) {
//                    Log.d(TAG, "getFormattedPrice: i= " + i);
//                    builder.replace(i, i, ",");
//                }
//            }
//        }
//        if (Constant.SETTINGS_PRICE_DECIMAL_COUNT > 0) {
//            Log.d(TAG, "getFormattedPrice: enter ");
//            builder.append(Constant.SETTINGS_PRICE_SEPARATED_COMMA);
//            for (int i = 0; i < Constant.SETTINGS_PRICE_DECIMAL_COUNT; i++) {
//                builder.append("0");
//            }
//        }
//
//        builder.append(" ").append(Constant.SETTINGS_PRICE_CURRENCY);
//
//        return builder.toString();
//    }

    public static String getFormattedPrice(double price){
        DecimalFormat df = new DecimalFormat("#,###,###");
        return AppSettings.SETTINGS_PRICE_CURRENCY + " " + df.format(price);
    }

    public static String getReceiptPrice(double price){
        DecimalFormat df = new DecimalFormat("#,###,###");
        return df.format(price);
    }
}
