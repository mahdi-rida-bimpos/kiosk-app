package com.bimpos.newkioskapp.Helpers;

public class Constant {


    public static int cartId = 0;

    public static String CAT_ORIENTATION = "h";
    public static String PICTURE_URL = "pictureUdl";
    public static long TIMEOUT = 110000;

    public static String FIRST_SETUP = "firstSetup";
    public static String SERVER_IP = "serverIp";
    public static String STATION_ID = "stationId";
    public static String SERVER_PORT = "serverPort";
    public static String DB_USER_NAME = "dbUserName";
    public static String DB_PASSWORD = "dbPassword";
    public static String SERVER_NAME = "serverName";
    public static String ROOT_FOLDER = "Kiosk";
    public static String BANNERS_FOLDER = "Banners";
    public static String RES_FOLDER = "Res";
    public static String PASSWORD = "password";
    public static String PRINTER_ARABIC = "printArabic";
    public static String ENABLE_QR_CODE = "qrCode";

    public static String RECEIVER_PRINTER = "INTENT_FILTER_PRINTER";
}
