package com.bimpos.newkioskapp.Helpers;

public class FormatString {

    public static String format(String s) {
        return s.trim()
                .replace("\"", "")
                .replace("\n", "")
                .replace("'","''");
    }
}
