package com.bimpos.newkioskapp.Helpers;

import android.graphics.Color;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseColor {

    public static int parse(String input) {
        input = input.replace(" ", "");
        Pattern c = Pattern.compile("([0-9]+),([0-9]+),([0-9]+),([0-9]+)");
        Matcher m = c.matcher(input);
        if (m.matches()) {
            return Color.rgb(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)));
        }
        return Color.rgb(255, 255, 255);
    }
}
