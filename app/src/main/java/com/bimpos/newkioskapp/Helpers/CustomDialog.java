package com.bimpos.newkioskapp.Helpers;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.bimpos.newkioskapp.R;
import com.github.nikartm.button.FitButton;



public class CustomDialog extends AppCompatDialogFragment {


    public static final String DIALOG_ID = "id";
    public static final String DIALOG_MESSAGE = "message";
    public static final String DIALOG_TITLE = "title";
    public static final String DIALOG_POSITIVE_BUTTON = "positiveButton";
    public static final String DIALOG_NEGATIVE_BUTTON = "negativeButton";
    private final DialogEvents listener;
    private AlertDialog dialog;
    private FitButton positiveButton;

    public interface DialogEvents {
        void onPositiveDialogResult(int dialogId, Bundle args);

        void onNegativeDialogResult(int dialogId, Bundle args);

        void onDialogCancelled(int dialogId);
    }

    public CustomDialog(DialogEvents listener) {
        this.listener = listener;
    }

    @Override
    public void onStart() {
        super.onStart();
        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 100);
        dialog.getWindow().setBackgroundDrawable(inset);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        @SuppressLint("InflateParams")
        View dialogView = getLayoutInflater().inflate(R.layout.custom_dialog, null, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(dialogView);

        final Bundle arguments = getArguments();
        final int dialogId;
        String messageString;
        String titleText;
        String positiveButtonText;
        String negativeButtonText;

        ImageView imageView = dialogView.findViewById(R.id.custom_dialog_image);
        imageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.kfc));
        TextView title = dialogView.findViewById(R.id.custom_dialog_title);
        TextView message = dialogView.findViewById(R.id.custom_dialog_message);
        ConstraintLayout titleLayout = dialogView.findViewById(R.id.custom_dialog_titleLayout);
        positiveButton = dialogView.findViewById(R.id.custom_dialog_positiveButton);
        FitButton negativeButton = dialogView.findViewById(R.id.custom_dialog_negativeButton);

        //check colors-------------------------------------
        title.setTextColor(AppSettings.DIALOG_TITLE_TEXT_COLOR);
        titleLayout.setBackgroundColor(AppSettings.DIALOG_TITLE_BACK_COLOR);
        positiveButton.setButtonColor(AppSettings.DIALOG_POSITIVE_BUTTON_BACK_COLOR);
        positiveButton.setTextColor(AppSettings.DIALOG_POSITIVE_BUTTON_TEXT_COLOR);
        negativeButton.setBackgroundColor(AppSettings.DIALOG_NEGATIVE_BUTTON_BACK_COLOR);
        negativeButton.setTextColor(AppSettings.DIALOG_NEGATIVE_BUTTON_TEXT_COLOR);
        //-------------------------------------------------

        if (arguments != null) {
            dialogId = arguments.getInt(DIALOG_ID);
            messageString = arguments.getString(DIALOG_MESSAGE);
            positiveButtonText = arguments.getString(DIALOG_POSITIVE_BUTTON);
            negativeButtonText = arguments.getString(DIALOG_NEGATIVE_BUTTON);
            titleText = arguments.getString(DIALOG_TITLE);

        } else {
            throw new IllegalArgumentException("Must pass Dialog_id and Dialog_message in the bundle");
        }

        message.setText(messageString);

        if (titleText.equalsIgnoreCase("null")) {
            titleLayout.setVisibility(View.INVISIBLE);
        } else {
            title.setText(titleText);
        }

        positiveButton.setText(positiveButtonText);
        positiveButton.setOnClickListener(v -> {
            if (listener != null) {
                listener.onPositiveDialogResult(dialogId, arguments);
            }
        });

        if (negativeButtonText.equalsIgnoreCase("null")) {
            negativeButton.setVisibility(View.INVISIBLE);
        } else {
            negativeButton.setText(negativeButtonText);
            negativeButton.setOnClickListener(v -> listener.onNegativeDialogResult(dialogId, arguments));
        }

        dialog = builder.create();
        dialog.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS |
                WindowManager.LayoutParams.SOFT_INPUT_MASK_ADJUST |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        dialog.getWindow().getAttributes().windowAnimations = R.style.animDialog;
        return dialog;
    }

    public void changePositiveButton(boolean status) {

        if(status){
            if(!positiveButton.isEnabled()){
                positiveButton.setEnabled(true);
            }
        }else{
            if(positiveButton.isEnabled()){
                positiveButton.setEnabled(false);
            }
        }
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        if (listener != null) {
            assert getArguments() != null;
            int dialogId = getArguments().getInt(DIALOG_ID);
            listener.onDialogCancelled(dialogId);
        }
    }
}
