package com.bimpos.newkioskapp.Helpers;

import android.util.Log;


public class GetTaxes {
    private static final String TAG = "GetTaxes";
    public static double getTaxAmount(double price, int type) {
        Log.d(TAG, "getTaxAmount: price "+price+", type "+type);

        switch (type) {
            case 3:
                return (price * AppSettings.TAX3_AMOUNT / 100);
            case 2:
                return (price * AppSettings.TAX2_AMOUNT / 100);
            case 1:
                Log.d(TAG, "getTaxAmount: "+(price * AppSettings.TAX1_AMOUNT / 100));
                return (price * AppSettings.TAX1_AMOUNT / 100);

            default: return 0.0;
        }
    }
}
