package com.bimpos.newkioskapp.Helpers;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FormatTime {

    @SuppressLint("SimpleDateFormat")
    public static int getOpenDate(String tim) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = fmt.parse(tim);
            Calendar c = Calendar.getInstance();
            assert date != null;
            c.setTimeInMillis(date.getTime());
            Date d = c.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            return Integer.parseInt(sdf.format(d));
        } catch (ParseException e) {
            return 0;
        }
    }

    @SuppressLint("SimpleDateFormat")
    public static long getOpenTime(String tim) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = fmt.parse(tim);
            Calendar c = Calendar.getInstance();
            assert date != null;
            c.setTimeInMillis(date.getTime());
            Date d = c.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmm");
            return Long.parseLong(sdf.format(d));
        } catch (ParseException e) {
            return 0;
        }
    }
}
