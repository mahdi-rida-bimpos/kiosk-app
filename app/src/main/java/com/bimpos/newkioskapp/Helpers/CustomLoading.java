package com.bimpos.newkioskapp.Helpers;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.bimpos.newkioskapp.R;

import java.util.Objects;

public class CustomLoading extends Dialog {

    public Activity activity;
    public Dialog d;
    private TextView message, title;
    private LottieAnimationView animationView;
    private static final String TAG = "CustomProgressBar";

    public CustomLoading(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    public void onStart() {
        super.onStart();
        getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 100);
        getWindow().setBackgroundDrawable(inset);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_loading);
        setCancelable(false);
        animationView = findViewById(R.id.custom_loading_animation);
        message = findViewById(R.id.custom_loading_message);
        title = findViewById(R.id.custom_loading_title);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS |
                WindowManager.LayoutParams.SOFT_INPUT_MASK_ADJUST |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        Objects.requireNonNull(getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


    }

    public void startAnimation(int raw) {
        animationView.setAnimation(raw);
        animationView.setRepeatCount(1000);
        animationView.playAnimation();
    }

    public void setMessage(String text) {
        message.setText(text);
    }

    public void setTitle(String text){
        title.setText(text);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        Log.d(TAG, "dismiss: start");
        animationView.cancelAnimation();
        message.setText("");
    }
}
