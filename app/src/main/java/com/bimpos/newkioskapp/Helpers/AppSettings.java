package com.bimpos.newkioskapp.Helpers;

import android.graphics.Color;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Date;
import java.util.HashMap;

public class AppSettings {

    public AppSettings() {
    }

    //values
    public static final String ENGLISHLANGUAGE = "ENGLISHLANGUAGE";
    public static final String ARABICLANGUAGE = "ARABICLANGUAGE";
    public static final String FRENCHLANGUAGE = "FRENCHLANGUAGE";

    public static final String BIGCATEGORIESCAROUSEL = "BIGCATEGORIESCAROUSEL";
    public static final String BIGCATEGORIESNORMAL = "BIGCATEGORIESNORMAL";

    //values that have to do with ordering
    public static String defaultLanguage = ENGLISHLANGUAGE;
    public static String CLIENT_ID = "BIM111";
    public static String SETTINGS_PRICE_CURRENCY = "LBP";
    public static String CURRENCY_STRING;

    public static String RESTAURANT_IMAGE=null;
    public static byte[] RESTAURANT_IMAGE1=null;
    public static String RESTAURANT_NAME;
    public static int UPDATE_LOOKUP=-1;

    public static HashMap<Integer, String> ORDER_TYPE_MAP = new HashMap<>();
    public static int TAKE_AWAY;
    public static int DINE_IN;

    public static int REMARK_NUM_VALUE;

    public static int CURRENCY_ID;
    public static int BRANCH_ID;
    public static int USER_ID = 100;

    public static Date TIM_DATE;
    public static int SHORT_TICKET_ID=0;
    public static String TIM_STRING;

    public static int OPEN_DATE;
    public static long OPEN_TIME;

    public static int REGULAR_PRODUCT=1;
    public static int COMBO=2;
    public static int QUESTION_REQUIRED=3;
    public static int QUESTION_NOT_REQUIRED=4;
    public static int MODIFIER=5;

    public static String GREETING_ORDER_TYPE_SCREEN;

    public static int TAX1_AMOUNT;
    public static String TAX1_DESCRIPTION;
    public static String TAX1_SYMBOL;
    public static int TAX2_AMOUNT;
    public static String TAX2_DESCRIPTION;
    public static String TAX2_SYMBOL;
    public static int TAX3_AMOUNT;
    public static String TAX3_DESCRIPTION;
    public static String TAX3_SYMBOL;


    //settings
    public static int BIG_CAT_SPAN_COUNT = 2;
    public static int PRODUCT_SPAN_COUNT = 2;


    //Color settings fetched in app startup
    public static int DIALOG_TITLE_BACK_COLOR; //implemented
    public static int DIALOG_TITLE_TEXT_COLOR; //implemented
    public static int DIALOG_POSITIVE_BUTTON_BACK_COLOR; //implemented
    public static int DIALOG_POSITIVE_BUTTON_TEXT_COLOR; //implemented
    public static int DIALOG_NEGATIVE_BUTTON_BACK_COLOR; //implemented
    public static int DIALOG_NEGATIVE_BUTTON_TEXT_COLOR; //implemented

    public static int BUTTON_PLUS_ENABLED_BACK_COLOR;
    public static int BUTTON_PLUS_ENABLED_TEXT_COLOR;
    public static int BUTTON_PLUS_DISABLED_BACK_COLOR;
    public static int BUTTON_PLUS_DISABLED_TEXT_COLOR;

    public static int BUTTON_MINUS_ENABLED_BACK_COLOR;
    public static int BUTTON_MINUS_ENABLED_TEXT_COLOR;
    public static int BUTTON_MINUS_DISABLED_BACK_COLOR;
    public static int BUTTON_MINUS_DISABLED_TEXT_COLOR;

    public static int BOTTOM_YES_ENABLED_BACK_COLOR; //implemented
    public static int BOTTOM_YES_ENABLED_TEXT_COLOR; //implemented
    public static int BOTTOM_YES_DISABLED_BACK_COLOR;
    public static int BOTTOM_YES_DISABLED_TEXT_COLOR;

    public static int BOTTOM_NO_ENABLED_BACK_COLOR; //implemented
    public static int BOTTOM_NO_ENABLED_TEXT_COLOR; //implemented
    public static int BOTTOM_NO_DISABLED_BACK_COLOR;
    public static int BOTTOM_NO_DISABLED_TEXT_COLOR;

    public static int PRICE_TEXT_COLOR;

    public static int CATEGORY_SELECTED_COLOR = Color.GREEN;

    public static int COMBO_HEADER_LEFT_BACK_COLOR;
    public static int COMBO_HEADER_RIGHT_BACK_COLOR;
    public static int COMBO_HEADER_TEXT_COLOR;

    public static int COMBO_ITEM_ENABLED_BACK_COLOR;
    public static int COMBO_ITEM_ENABLED_TEXT_COLOR;
    public static int COMBO_ITEM_DISABLED_TEXT_COLOR;

    public static int QUESTION_HEADER_LEFT_BACK_COLOR;
    public static int QUESTION_HEADER_RIGHT_BACK_COLOR;
    public static int QUESTION_HEADER_TEXT_COLOR;

    public static int QUESTION_ITEM_ENABLED_BACK_COLOR;
    public static int QUESTION_ITEM_ENABLED_TEXT_COLOR;
    public static int QUESTION_ITEM_DISABLED_BACK_COLOR;
    public static int QUESTION_ITEM_DISABLED_TEXT_COLOR;

}
