package com.bimpos.newkioskapp.Helpers;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.bimpos.newkioskapp.R;

public class MyAnimation {

    public static Animation getAnimation(Context context) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.bounce_animation);
        BounceInterpolator interpolator = new BounceInterpolator(0.025, 20);
        animation.setInterpolator(interpolator);
        return  animation;
    }
}
