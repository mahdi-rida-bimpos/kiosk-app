package com.bimpos.newkioskapp.Helpers;

import android.content.Context;
import android.util.Log;

public class GetPixel {

    private static final String TAG = "GetPixel";
    public static int getPixel(Context context, int x) {
        final float scale = context.getResources().getDisplayMetrics().density;
        Log.d(TAG, "getPixel: scale "+scale);
        Log.d(TAG, "getPixel: final "+(x * scale + 0.5f));
        return (int) (x * scale + 0.5f);
    }
}
