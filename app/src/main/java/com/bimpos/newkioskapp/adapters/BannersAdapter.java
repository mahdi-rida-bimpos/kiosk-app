package com.bimpos.newkioskapp.adapters;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.newkioskapp.R;

import java.io.File;
import java.util.List;

public class BannersAdapter extends RecyclerView.Adapter<BannersAdapter.ViewHolder> {

    private List<Uri> uriList;
    String projectRoot;
    private static final String TAG = "BannersAdapter";

    public BannersAdapter(List<Uri> uriList) {
        this.uriList = uriList;
        projectRoot = Environment.getExternalStorageDirectory().getPath();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_banners, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (uriList.size() == 0) {
            holder.imageLayout.setVisibility(View.GONE);
        } else {
            Uri uri = uriList.get(position);
            holder.imageLayout.setVisibility(View.VISIBLE);
            holder.imageView.setImageURI(uri);
            Log.d(TAG, "onBindViewHolder: "+uri.toString());
            holder.remove.setOnClickListener(v -> {
                Log.d(TAG, "onBindViewHolder: start");
                File fdelete = new File(uri.getPath());
                Log.d(TAG, "onBindViewHolder: uri "+fdelete);
                if (fdelete.exists()) {
                    if (fdelete.delete()) {
                        Log.d(TAG, "onBindViewHolder: deleted");
                        uriList.remove(holder.getAdapterPosition());
                        notifyDataSetChanged();
                    } else {

                        Log.d(TAG, "onBindViewHolder: not deleted");
                    }
                }else{
                    uriList.remove(holder.getAdapterPosition());
                    notifyDataSetChanged();
                    Log.d(TAG, "onBindViewHolder: file not exists");
                }

            });
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Uri> newList) {
        this.uriList = newList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (uriList != null && uriList.size() != 0) {
            return uriList.size();
        } else {
            return 1;
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView, remove;
        RelativeLayout imageLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageLayout = itemView.findViewById(R.id.recycler_banners_imageLayout);
            imageView = itemView.findViewById(R.id.recycler_banners_imageView);
            remove = itemView.findViewById(R.id.recycler_banners_remove);
        }
    }
}
