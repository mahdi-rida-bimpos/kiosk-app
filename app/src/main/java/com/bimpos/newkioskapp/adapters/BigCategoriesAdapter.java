package com.bimpos.newkioskapp.adapters;

import static com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.newkioskapp.Helpers.GetBitmap;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.models.Categories;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

public class BigCategoriesAdapter extends RecyclerView.Adapter<BigCategoriesAdapter.ViewHolder> {

    private final List<Categories> categoriesList;
    private final OnBigCategoryClicked listener;
    private final int spanCount;
    private Context context;

    public interface OnBigCategoryClicked {
        void onBigCategoryClicked(Categories categories, int position);
    }

    public BigCategoriesAdapter(List<Categories> categoriesList,Context context, OnBigCategoryClicked listener, int spanCount) {
        this.categoriesList = categoriesList;
        this.context=context;
        this.listener = listener;
        this.spanCount = spanCount;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (spanCount <= 2) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_big_category_span_2, parent, false);
        } else  {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_big_category_span_3, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Categories categories = categoriesList.get(position);

//        if(position==0){
//            holder.description.setText("Burger");
//            holder.image.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.burger));
//        }else if(position==1){
//            holder.description.setText("Beverage");
//            holder.image.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.beverage));
//        }
//        if(position==2){
//            holder.description.setText("Cold");
//            holder.image.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.cold));
//        }else if(position==3){
//            holder.description.setText("Pizza");
//            holder.image.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.pizza));
//        }
//        if(position==4){
//            holder.description.setText("Cakes");
//            holder.image.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.cakes));
//        }else if(position==5){
//            holder.description.setText("Sweets");
//            holder.image.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.sweets));
//        }

        holder.description.setText(categories.getDescription());
        if (categories.getHexPicture() != null) {
            if (categories.getHexPicture().length() != 0) {
//                holder.image.setImageBitmap(GetBitmap.parseLogo(categories.getHexPicture()));
                Glide.with(context)
                        .asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .load(GetBitmap.getImageBytes(categories.getHexPicture()))
                        .transition(withCrossFade())
                        .placeholder(R.drawable.category_placeholder)
                        .into(  holder.image);
            }
        }
        holder.itemView.setOnClickListener(view -> listener.onBigCategoryClicked(categories, holder.getAdapterPosition()));


    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
//        return 6;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView description;
        LinearLayout layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.recycler_big_category_image);
            description = itemView.findViewById(R.id.recycler_big_category_categoryName);
            layout = itemView.findViewById(R.id.recycler_categories_cardView);
        }
    }
}
