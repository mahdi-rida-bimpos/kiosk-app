package com.bimpos.newkioskapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.Helpers.CustomDialog;
import com.bimpos.newkioskapp.Helpers.FormatPrice;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.activities.MainActivity;
import com.bimpos.newkioskapp.models.Cart;
import com.bimpos.newkioskapp.models.CartComboHeader;
import com.bimpos.newkioskapp.models.CartComboModifier;
import com.bimpos.newkioskapp.models.CartProduct;
import com.bimpos.newkioskapp.models.CartQuestionHeader;
import com.bimpos.newkioskapp.models.CartQuestionModifier;
import com.hardik.clickshrinkeffect.ClickShrinkEffect;

import java.util.ArrayList;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> implements CustomDialog.DialogEvents {

    private final List<CartProduct> cartItemsList;
    private final Context context;
    private final Cart cartData;
    private final OnEmptyCard listener;
    private final CustomDialog dialog;
    private final MainActivity activity;
    private final int DIALOG_REMOVE_ITEM = 1;

    public interface OnEmptyCard {
        void onEmptyCard();

        void onPriceChanged();
    }

    public CartAdapter(List<CartProduct> cartItemsList, Context context, OnEmptyCard listener, MainActivity activity) {
        this.listener = listener;
        this.activity = activity;
        this.cartItemsList = cartItemsList;
        this.context = context;
        cartData = Cart.getCartData();
        dialog = new CustomDialog(this);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cart_order, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        CartProduct items = cartItemsList.get(position);

        //check colors-------------------------------------
        holder.plus.setBackgroundColor(AppSettings.BUTTON_PLUS_ENABLED_BACK_COLOR);
        holder.plus.getDrawable().setTint(AppSettings.BUTTON_PLUS_ENABLED_TEXT_COLOR);
        holder.minus.setBackgroundColor(AppSettings.BUTTON_MINUS_ENABLED_BACK_COLOR);
        holder.minus.getDrawable().setTint(AppSettings.BUTTON_MINUS_ENABLED_TEXT_COLOR);
        holder.productPrice.setTextColor(AppSettings.PRICE_TEXT_COLOR);
        holder.totalPrice.setTextColor(AppSettings.PRICE_TEXT_COLOR);
        //-------------------------------------------------

        double price = items.getPrice();
        holder.productPrice.setText(FormatPrice.getFormattedPrice(price));
        holder.productDesc.setText(items.getProd_desc());

        if (items.getRemark().equalsIgnoreCase("")) {
            holder.remarkLayout.setVisibility(View.GONE);
        } else {
            holder.remarkText.setText(items.getRemark());
            holder.remarkLayout.setVisibility(View.VISIBLE);
        }

        new ClickShrinkEffect(holder.plus);
        new ClickShrinkEffect(holder.minus);

        String qtyText = items.getQty() + "";
        holder.productQty.setText(qtyText);


        if (items.getQuestionList().size() != 0 || items.getComboList().size()!=0) {
            int count = items.getQty();
            double totalPrice = price * count;
            int modifierPrice = 0;
            for (int i = 0; i < items.getQuestionList().size(); i++) {
                CartQuestionHeader cartQuestionHeader = items.getQuestionList().get(i);
                for (int j = 0; j < cartQuestionHeader.getCartQuestionModifiers().size(); j++) {
                    CartQuestionModifier modifier = cartQuestionHeader.getCartQuestionModifiers().get(j);
                    modifierPrice += modifier.getPrice() * items.getQty();
                }
            }
            for (int ii = 0; ii < items.getComboList().size(); ii++) {
                CartComboHeader cartComboHeader = items.getComboList().get(ii);
                for (int iii = 0; iii < cartComboHeader.getCartComboModifiers().size(); iii++) {
                    CartComboModifier modifier = cartComboHeader.getCartComboModifiers().get(iii);
                    if(modifier.getPrice()>0){
                        modifierPrice += modifier.getPrice() * items.getQty() * modifier.getCount();
                    }
                }
            }
            totalPrice += modifierPrice;
            holder.totalPrice.setText(FormatPrice.getFormattedPrice(totalPrice));

        } else {
            int count = items.getQty();
            price = price * count;
            holder.totalPrice.setText(FormatPrice.getFormattedPrice(price));
        }


        holder.plus.setOnClickListener(view -> {
            activity.resetHandler();
            int count = cartData.getCartProductCount(items.getCartId());
            if(count<99){
                cartData.addItem(items.getCartId());
                listener.onPriceChanged();
                notifyDataSetChanged();
            }

        });

        holder.minus.setOnClickListener(view -> {
            activity.resetHandler();
            int qty = cartData.getCartProductCount(items.getCartId());
            if (qty == 1) {

                showRemoveItemDialog(items);

            } else {
                cartData.removeItem(items.getCartId());
            }
            if (cartItemsList.size() == 0) {
                listener.onEmptyCard();
            } else {
                listener.onPriceChanged();
                notifyDataSetChanged();
            }

        });

        if (items.getQuestionList().size() != 0 || items.getComboList().size() != 0) {
            List<Object> objectList = new ArrayList<>();
           if(items.getComboList().size()!=0){
               objectList.addAll(items.getComboList());
           }
           if(items.getQuestionList().size()!=0){
               objectList.addAll(items.getQuestionList());
           }
            CartOrderRecyclerAdapter adapter = new CartOrderRecyclerAdapter(context, objectList);
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            holder.recyclerView.setAdapter(adapter);
        }
    }

    private void showRemoveItemDialog(CartProduct items) {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "Remove " + items.getProd_desc() + " from cart?");
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "yes");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "no");
        arguments.putInt(CustomDialog.DIALOG_ID, DIALOG_REMOVE_ITEM);
        arguments.putSerializable(CartProduct.class.getSimpleName(), items);
        dialog.setArguments(arguments);
        dialog.show(activity.getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        if (dialogId == DIALOG_REMOVE_ITEM) {
            dialog.dismiss();
            CartProduct items = (CartProduct) args.getSerializable(CartProduct.class.getSimpleName());
            cartData.removeItem(items.getCartId());
            if (cartItemsList.size() == 0) {
                listener.onEmptyCard();
            } else {
                listener.onPriceChanged();
                notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        if (dialogId == DIALOG_REMOVE_ITEM) {
            dialog.dismiss();
        }
    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }

    @Override
    public int getItemCount() {
        return cartItemsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView plus, minus;
        RecyclerView recyclerView;
        LinearLayout remarkLayout;
        TextView productDesc, productPrice, totalPrice, productQty, remarkText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            productPrice = itemView.findViewById(R.id.recycler_cart_order_productPrice);
            productDesc = itemView.findViewById(R.id.recycler_cart_order_productDescription);
            totalPrice = itemView.findViewById(R.id.recycler_cart_order_totalPrice);
            productQty = itemView.findViewById(R.id.recycler_cart_order_quantity);
            plus = itemView.findViewById(R.id.recycler_cart_order_plus);
            minus = itemView.findViewById(R.id.recycler_cart_order_minus);
            recyclerView = itemView.findViewById(R.id.recycler_cart_order_recyclerView);
            remarkLayout = itemView.findViewById(R.id.recycler_cart_order_remarkLayout);
            remarkText = itemView.findViewById(R.id.recycler_cart_order_remarkText);
        }
    }
}
