package com.bimpos.newkioskapp.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.newkioskapp.Helpers.FormatPrice;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.models.CartProduct;

import java.util.List;

public class PrinterItemsAdapter extends RecyclerView.Adapter<PrinterItemsAdapter.ViewHolder> {

    private List<CartProduct> cartItemsList;
    private static final String TAG = "PrinterItems";

    public PrinterItemsAdapter(List<CartProduct> cartItemsList) {
        this.cartItemsList = cartItemsList;
        Log.d(TAG, "PrinterItemsAdapter: start");
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_printing_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CartProduct items = cartItemsList.get(position);
        Log.d(TAG, "onBindViewHolder: enter");
        holder.quantity.setText(String.valueOf(items.getQty()));
        holder.description.setText(items.getProd_desc());
        holder.price.setText(FormatPrice.getReceiptPrice(items.getPrice()));
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: start "+cartItemsList.size());
        return cartItemsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView quantity, description, price;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            quantity = itemView.findViewById(R.id.recycler_printing_items_quantity);
            description = itemView.findViewById(R.id.recycler_printing_items_description);
            price = itemView.findViewById(R.id.recycler_printing_items_price);
        }
    }
}
