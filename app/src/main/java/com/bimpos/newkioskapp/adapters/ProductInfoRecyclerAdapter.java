package com.bimpos.newkioskapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.models.CartComboHeader;
import com.bimpos.newkioskapp.models.CartComboModifier;
import com.bimpos.newkioskapp.models.CartQuestionHeader;
import com.bimpos.newkioskapp.models.CartQuestionModifier;
import com.bimpos.newkioskapp.models.ComboHeader;
import com.bimpos.newkioskapp.models.ComboModifier;
import com.bimpos.newkioskapp.models.QuestionHeader;
import com.bimpos.newkioskapp.models.QuestionModifier;
import com.bimpos.newkioskapp.fragments.ProductInfoFragment;

import java.util.ArrayList;
import java.util.List;

public class ProductInfoRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int QUESTION_HEADER_VIEW = 1;
    private final int COMBO_HEADER_VIEW = 2;

    private final ProductInfoFragment fragment;
    private List<Object> objectList;
    private final Context context;
    public List<CartComboHeader> cartComboHeaderList = new ArrayList<>();
    public List<CartQuestionHeader> cartQuestionHeaderList = new ArrayList<>();

    public ProductInfoRecyclerAdapter(ProductInfoFragment fragment, Context context, List<Object> objectList) {
        this.fragment = fragment;
        this.context = context;
        this.objectList = objectList;
    }

    @Override
    public int getItemViewType(int position) {
        if (objectList != null && objectList.size() != 0) {
            Object object = objectList.get(position);
            if (object instanceof QuestionHeader) {
                return QUESTION_HEADER_VIEW;
            } else {
                return COMBO_HEADER_VIEW;
            }
        }
        return 3;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == QUESTION_HEADER_VIEW) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_question_header, parent, false);
            return new QuestionHeaderViewHolder(view);
        } else if (viewType == COMBO_HEADER_VIEW) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_combo_header, parent, false);
            return new ComboHeaderViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_question_header_holder, parent, false);
            return new QuestionHeaderHolderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {

        if (objectList.size() != 0) {
            Object object = objectList.get(position);

            if (holder1 instanceof QuestionHeaderViewHolder) {
                QuestionHeaderViewHolder holder = (QuestionHeaderViewHolder) holder1;
                QuestionHeader questionHeader = (QuestionHeader) object;
                int min = questionHeader.getMin();
                int max = questionHeader.getMax();
                //check colors---------------------------------------------------
                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.LEFT_RIGHT,
                        new int[]{AppSettings.QUESTION_HEADER_LEFT_BACK_COLOR, AppSettings.QUESTION_HEADER_RIGHT_BACK_COLOR});
                gd.setCornerRadius(0f);
                holder.headerLayout.setBackground(gd);
                holder.description.setTextColor(AppSettings.QUESTION_HEADER_TEXT_COLOR);
                holder.maxItems.setTextColor(AppSettings.QUESTION_HEADER_TEXT_COLOR);
                //----------------------------------------------------------------
                holder.description.setText(questionHeader.getDescription());

                if (min == max) {
                    String text = "";
                    if (min != 0) {
                        if (min == 1) {
                            text = "Must choose " + min + " modifier";
                        } else {
                            text = "Must choose " + min + " modifiers";
                        }
                    }

                    holder.maxItems.setText(text);
                    holder.maxItems.setVisibility(View.VISIBLE);

                } else if (min == 0 && max >= 1) {
                    String text;
                    if (max == 1) {
                        text = "Choose up to " + questionHeader.getMax() + " modifier";
                    } else {
                        text = "Choose up to " + questionHeader.getMax() + " modifiers";
                    }
                    holder.maxItems.setText(text);
                    holder.maxItems.setVisibility(View.VISIBLE);
                }

                if (questionHeader.getModifiersList().size() != 0) {

                    List<CartQuestionModifier> cartQuestionModifierList = new ArrayList<>();
                    for (int i = 0; i < questionHeader.getModifiersList().size(); i++) {
                        QuestionModifier questionModifier = questionHeader.getModifiersList().get(i);
                        cartQuestionModifierList.add(new CartQuestionModifier(
                                        questionModifier.getProdNum(),
                                        questionModifier.getDescription(),
                                        false,
                                        questionModifier.getPrice(),
                                        questionModifier.getTax1(),
                                        questionModifier.getTax2(),
                                        questionModifier.getTax3(),
                                        questionModifier.getTaxEx(),
                                        questionModifier.getAvgCost(),
                                        questionModifier.getLastCost()
                                )
                        );
                    }
                    CartQuestionHeader cartQuestionHeader = new CartQuestionHeader(
                            questionHeader.getDescription(),
                            questionHeader.getMin(),
                            questionHeader.getMax(),
                            questionHeader.getRequired(),
                            cartQuestionModifierList);
                    cartQuestionHeaderList.add(cartQuestionHeader);

                    QuestionModifierAdapter questionModifierAdapter = new QuestionModifierAdapter(fragment, cartQuestionHeader);
                    holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    holder.recyclerView.setAdapter(questionModifierAdapter);
                }
            } else {

                ComboHeaderViewHolder holder = (ComboHeaderViewHolder) holder1;
                ComboHeader comboHeader = (ComboHeader) object;

                //check colors first----------------------------------------------------------------
                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.LEFT_RIGHT,
                        new int[]{AppSettings.COMBO_HEADER_LEFT_BACK_COLOR, AppSettings.COMBO_HEADER_RIGHT_BACK_COLOR});
                gd.setCornerRadius(0f);
                holder.headerLayout.setBackground(gd);
                holder.description.setTextColor(AppSettings.COMBO_HEADER_TEXT_COLOR);
                holder.maxItems.setTextColor(AppSettings.COMBO_HEADER_TEXT_COLOR);
                //-----------------------------------------------------------------------------------

                holder.description.setText(comboHeader.getDescription());

                if (comboHeader.getMin() == comboHeader.getMax()) {
                    String maxItems = "Must Choose " + comboHeader.getMax();
                    holder.maxItems.setText(maxItems);
                    holder.maxItems.setVisibility(View.VISIBLE);

                } else if (comboHeader.getMin() == 0 && comboHeader.getMax() >= 1) {
                    String text = "(Max Selection: " + comboHeader.getMax() + ")";
                    holder.maxItems.setText(text);
                    holder.maxItems.setVisibility(View.VISIBLE);
                }

                holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));

                List<CartComboModifier> cartComboModifiers = new ArrayList<>();

                for (int i = 0; i < comboHeader.getModifiersList().size(); i++) {
                    ComboModifier comboModifier = comboHeader.getModifiersList().get(i);
                    cartComboModifiers.add(new CartComboModifier(
                            comboModifier.getProdNum(),
                            comboModifier.getDescription(),
                            0,
                            comboModifier.getPrice(),
                            comboModifier.getTax1(),
                            comboModifier.getTax2(),
                            comboModifier.getTax3(),
                            comboModifier.getTaxEx(),
                            comboModifier.getAvgCost(),
                            comboModifier.getLastCost()));
                }

                CartComboHeader cartComboHeader = new CartComboHeader(
                        comboHeader.getDescription(),
                        comboHeader.getMin(),
                        comboHeader.getMax(),
                        comboHeader.getRequired(),
                        0,
                        cartComboModifiers);

                cartComboHeaderList.add(cartComboHeader);

                ComboModifierAdapter comboModifierRvAdapter = new ComboModifierAdapter(cartComboHeader, fragment);
                holder.recyclerView.setAdapter(comboModifierRvAdapter);
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Object> data) {
        this.objectList = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (objectList != null && objectList.size() != 0) {
            fragment.mainActivity.enableCartButton();
            return objectList.size();
        } else {
            return 10;
        }
    }


    static class QuestionHeaderViewHolder extends RecyclerView.ViewHolder {
        TextView description, maxItems;
        RecyclerView recyclerView;
        RelativeLayout headerLayout;

        public QuestionHeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.recycler_question_header_description);
            recyclerView = itemView.findViewById(R.id.recycler_question_header_recyclerView);
            maxItems = itemView.findViewById(R.id.recycler_question_header_maxItems);
            headerLayout = itemView.findViewById(R.id.recycler_question_header_layout);
        }
    }

    static class ComboHeaderViewHolder extends RecyclerView.ViewHolder {
        TextView description, maxItems;
        RecyclerView recyclerView;
        RelativeLayout headerLayout;

        public ComboHeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.recycler_combo_header_description);
            recyclerView = itemView.findViewById(R.id.recycler_combo_header_recyclerView);
            maxItems = itemView.findViewById(R.id.recycler_combo_header_maxItems);
            headerLayout = itemView.findViewById(R.id.recycler_combo_header_headerLayout);
        }
    }

    static class QuestionHeaderHolderViewHolder extends RecyclerView.ViewHolder {
        public QuestionHeaderHolderViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }


}
