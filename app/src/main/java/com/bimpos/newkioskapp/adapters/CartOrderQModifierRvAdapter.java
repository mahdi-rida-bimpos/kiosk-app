package com.bimpos.newkioskapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.Helpers.FormatPrice;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.models.CartQuestionModifier;

import java.util.List;

public class CartOrderQModifierRvAdapter extends RecyclerView.Adapter<CartOrderQModifierRvAdapter.ViewHolder> {

    private final List<CartQuestionModifier> cartQuestionModifierList;

    public CartOrderQModifierRvAdapter(List<CartQuestionModifier> cartQuestionModifierList) {
        this.cartQuestionModifierList = cartQuestionModifierList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cart_order_question_modifier, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CartQuestionModifier cartQuestionModifier = cartQuestionModifierList.get(position);

        //check colors------------------------------------------------
        holder.extraPrice.setTextColor(AppSettings.PRICE_TEXT_COLOR);
        //-----------------------------------------------------------

        holder.modifierDescription.setText(cartQuestionModifier.getDescription());
        if(cartQuestionModifier.getPrice()>0){
            holder.extraPrice.setText(String.format("+ %s", FormatPrice.getFormattedPrice(cartQuestionModifier.getPrice())));
        }
    }

    @Override
    public int getItemCount() {
        return cartQuestionModifierList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView modifierDescription, extraPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            extraPrice = itemView.findViewById(R.id.recycler_cart_order_question_modifier_extraPrice);
            modifierDescription = itemView.findViewById(R.id.recycler_cart_order_question_modifier_description);
        }
    }
}
