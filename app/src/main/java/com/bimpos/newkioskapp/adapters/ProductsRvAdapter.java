package com.bimpos.newkioskapp.adapters;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.newkioskapp.Helpers.FormatPrice;
import com.bimpos.newkioskapp.Helpers.GetBitmap;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.activities.MainActivity;
import com.bimpos.newkioskapp.models.Products;
import com.bumptech.glide.Glide;
import com.hardik.clickshrinkeffect.ClickShrinkEffect;

import java.util.List;

public class ProductsRvAdapter extends RecyclerView.Adapter<ProductsRvAdapter.ViewHolder> {

    private List<Products> productsList;
    private final MainActivity mainActivity;
    private static final String TAG = "ProductsRvAdapter";

    public ProductsRvAdapter(MainActivity mainActivity, List<Products> productsList) {
        this.mainActivity = mainActivity;
        this.productsList = productsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_products, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Products product = productsList.get(position);
        new ClickShrinkEffect(holder.itemView);
        holder.description.setText(product.getDescription());
        holder.price.setText(FormatPrice.getFormattedPrice(product.getPrice()));
        Log.d(TAG, "onBindViewHolder: product " + product.getDescription() + ", " + product.getHexPicture());
        holder.image.setImageDrawable(ContextCompat.getDrawable(mainActivity,R.drawable.plate_placeholder));
        if (product.getHexPicture() != null) {
            if (product.getHexPicture().length() != 0) {
                holder.image.setImageBitmap(GetBitmap.getImageBitmap(product.getHexPicture()));
            }
        }

        holder.itemView.setOnClickListener(view -> {
            if (!mainActivity.isProductOpened) {
                mainActivity.isProductOpened=true;
                Log.d(TAG, "onBindViewHolder: clicked");
                new Handler().postDelayed(() -> mainActivity.openProductFragment(product), 200);
            }
        });

    }


    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Products> newList) {
        productsList = newList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return productsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView description, price;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.recycler_view_product_image);
            description = itemView.findViewById(R.id.recycler_view_product_description);
            price = itemView.findViewById(R.id.recycler_view_product_price);

        }
    }
}
