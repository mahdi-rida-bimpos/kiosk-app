package com.bimpos.newkioskapp.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.Helpers.FormatPrice;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.fragments.ProductInfoFragment;
import com.bimpos.newkioskapp.models.CartComboHeader;
import com.bimpos.newkioskapp.models.CartComboModifier;
import com.hardik.clickshrinkeffect.ClickShrinkEffect;

import java.util.List;

public class ComboModifierAdapter extends RecyclerView.Adapter<ComboModifierAdapter.ModifierSelectionViewHolder> {

    private final CartComboHeader cartComboHeader;
    private final List<CartComboModifier> cartComboModifierList;
    private final ProductInfoFragment fragment;


    public ComboModifierAdapter(CartComboHeader cartComboHeader, ProductInfoFragment fragment) {
        this.fragment = fragment;
        this.cartComboHeader = cartComboHeader;
        cartComboModifierList = cartComboHeader.getCartComboModifiers();
    }

    @NonNull
    @Override
    public ModifierSelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_combo_modifier, parent, false);
        return new ModifierSelectionViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull ModifierSelectionViewHolder holder, int position) {

        CartComboModifier cartComboModifier = cartComboModifierList.get(position);

        //check color----------------------------------------
        holder.plus.setBackgroundColor(AppSettings.BUTTON_PLUS_ENABLED_BACK_COLOR);
        holder.plus.getDrawable().setTint(AppSettings.BUTTON_PLUS_ENABLED_TEXT_COLOR);

        holder.minus.getDrawable().setTint(AppSettings.BUTTON_MINUS_ENABLED_TEXT_COLOR);
        holder.description.setTextColor(AppSettings.COMBO_ITEM_ENABLED_TEXT_COLOR);
        holder.mainLayout.setBackgroundColor(AppSettings.COMBO_ITEM_ENABLED_BACK_COLOR);
        holder.price.setTextColor(AppSettings.PRICE_TEXT_COLOR);
        //---------------------------------------------------


        holder.description.setText(cartComboModifier.getDescription());
        int max = cartComboHeader.getMax();
        double price= cartComboModifier.getPrice();
        if(price>0){
            holder.price.setText(String.format("+ %s", FormatPrice.getFormattedPrice(price)));
        }
        holder.count.setText(String.valueOf(cartComboModifier.getCount()));

        if (cartComboModifier.getCount() == 0) {
            holder.minus.setBackgroundColor(AppSettings.BUTTON_MINUS_DISABLED_BACK_COLOR);
            holder.minus.getDrawable().setTint(AppSettings.BUTTON_MINUS_DISABLED_TEXT_COLOR);
        } else {
            holder.minus.setBackgroundColor(AppSettings.BUTTON_MINUS_ENABLED_BACK_COLOR);
            holder.minus.getDrawable().setTint(AppSettings.BUTTON_MINUS_ENABLED_TEXT_COLOR);
        }

        if (cartComboHeader.getSelectedItemCount() == max) {
            holder.plus.setBackgroundColor(AppSettings.BUTTON_PLUS_DISABLED_BACK_COLOR);
            holder.plus.getDrawable().setTint(AppSettings.BUTTON_PLUS_DISABLED_TEXT_COLOR);
            holder.description.setTextColor(AppSettings.COMBO_ITEM_DISABLED_TEXT_COLOR);
        } else {
            holder.plus.setBackgroundColor(AppSettings.BUTTON_PLUS_ENABLED_BACK_COLOR);
            holder.plus.getDrawable().setTint(AppSettings.BUTTON_PLUS_ENABLED_TEXT_COLOR);
            holder.description.setTextColor(AppSettings.COMBO_ITEM_ENABLED_TEXT_COLOR);
        }

        new ClickShrinkEffect(holder.plus);
        new ClickShrinkEffect(holder.minus);

        holder.plus.setOnClickListener(v -> {

            if (cartComboHeader.getSelectedItemCount() < max) {
                fragment.mainActivity.resetHandler();
                int count = cartComboModifier.getCount();
                count++;
                cartComboModifier.setCount(count);
                int selectedItemCount = cartComboHeader.getSelectedItemCount();
                selectedItemCount++;
                cartComboHeader.setSelectedItemCount(selectedItemCount);
                holder.count.setText(String.valueOf(count));
            }
            if(cartComboModifier.getPrice()>0){
                calculateExtraPrice();
            }
            notifyDataSetChanged();
        });

        holder.minus.setOnClickListener(v -> {
            fragment.mainActivity.resetHandler();
            int count = cartComboModifier.getCount();
            if (count > 0) {
                count--;
                cartComboModifier.setCount(count);
                holder.count.setText(String.valueOf(count));
                int selectedItemCount = cartComboHeader.getSelectedItemCount();
                selectedItemCount--;
                cartComboHeader.setSelectedItemCount(selectedItemCount);
            }
            if(cartComboModifier.getPrice()>0){
                calculateExtraPrice();
            }
            notifyDataSetChanged();
        });
    }
    public void calculateExtraPrice() {
        fragment.calculatePrice();
    }

    @Override
    public int getItemCount() {
        return cartComboModifierList.size();
    }

    static class ModifierSelectionViewHolder extends RecyclerView.ViewHolder {
        TextView description, count,price;
        ImageView plus, minus;
        RelativeLayout mainLayout;

        public ModifierSelectionViewHolder(@NonNull View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.recycler_combo_modifier_description);
            count = itemView.findViewById(R.id.recycler_combo_modifier_quantity);
            plus = itemView.findViewById(R.id.recycler_combo_modifier_plus);
            minus = itemView.findViewById(R.id.recycler_combo_modifier_minus);
            mainLayout = itemView.findViewById(R.id.recycler_combo_modifier_mainLayout);
            price = itemView.findViewById(R.id.recycler_combo_modifier_price);
        }
    }

}