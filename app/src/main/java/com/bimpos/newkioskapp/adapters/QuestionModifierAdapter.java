package com.bimpos.newkioskapp.adapters;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.Helpers.FormatPrice;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.models.CartQuestionHeader;
import com.bimpos.newkioskapp.models.CartQuestionModifier;
import com.bimpos.newkioskapp.fragments.ProductInfoFragment;

import java.util.List;


public class QuestionModifierAdapter extends RecyclerView.Adapter<QuestionModifierAdapter.ModifierSelectionViewHolder> {

    private final ProductInfoFragment fragment;
    private final CartQuestionHeader cartQuestionHeader;
    private final List<CartQuestionModifier> cartQuestionModifierList;
    private static final String TAG = "QuestionModifierRv";

    public QuestionModifierAdapter(ProductInfoFragment fragment, CartQuestionHeader cartQuestionHeader) {
        this.fragment = fragment;
        Log.d(TAG, "QuestionModifierAdapter: start");
        this.cartQuestionHeader = cartQuestionHeader;
        cartQuestionModifierList = cartQuestionHeader.getCartQuestionModifiers();
    }

    @NonNull
    @Override
    public QuestionModifierAdapter.ModifierSelectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_question_modifier, parent, false);
        return new ModifierSelectionViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull QuestionModifierAdapter.ModifierSelectionViewHolder holder, int position) {

        Log.d(TAG, "onBindViewHolder: starts");
        CartQuestionModifier cartQuestionModifier = cartQuestionModifierList.get(position);

        //check colors---------------------------------------
        holder.price.setTextColor(AppSettings.PRICE_TEXT_COLOR);
        holder.checkBox.setBackgroundTintList(ColorStateList.valueOf(AppSettings.PRICE_TEXT_COLOR));
        //---------------------------------------------------

        holder.description.setText(cartQuestionModifier.getDescription());
        if(cartQuestionModifier.getPrice()!=0){
            holder.price.setText(String.format("+ %s", FormatPrice.getFormattedPrice(cartQuestionModifier.getPrice())));
        }
        int min = cartQuestionHeader.getMin();
        int max = cartQuestionHeader.getMax();
        int required = cartQuestionHeader.getRequired();

        if(cartQuestionHeader.getCount()==max){
            if(cartQuestionModifier.isSelected()){
                holder.checkBox.setEnabled(true);
                holder.description.setTextColor(AppSettings.QUESTION_ITEM_ENABLED_TEXT_COLOR);
            }else{
                holder.checkBox.setEnabled(false);
                holder.description.setTextColor(AppSettings.QUESTION_ITEM_DISABLED_TEXT_COLOR);
            }
        }else{
            holder.checkBox.setEnabled(true);
            holder.description.setTextColor(AppSettings.QUESTION_ITEM_ENABLED_TEXT_COLOR);
        }

        holder.checkBox.setChecked(cartQuestionModifier.isSelected());

        holder.checkBox.setOnClickListener(view -> {

            fragment.mainActivity.resetHandler();

            if ((min == max) && required == 1) {
                if (holder.checkBox.isChecked()) {
                    if(cartQuestionHeader.getCount()!=1){
                        cartQuestionModifier.setSelected(true);
                        cartQuestionHeader.setCount(1);
                    }
                } else {
                    cartQuestionModifier.setSelected(false);
                    cartQuestionHeader.setCount(0);
                }
                notifyDataSetChanged();

            }

            else if ((min < max)) {

                if (holder.checkBox.isChecked()) {

                    if (cartQuestionHeader.getCount() != max) {
                        cartQuestionModifier.setSelected(true);
                        cartQuestionHeader.setCount(cartQuestionHeader.getCount() + 1);
                    } else {
                        holder.checkBox.setChecked(false);
                    }

                } else {
                    cartQuestionModifier.setSelected(false);
                    cartQuestionHeader.setCount(cartQuestionHeader.getCount() - 1);
                }
                notifyDataSetChanged();

            }

            else if (min == max && required == 0) {

                if (holder.checkBox.isChecked()) {
                    cartQuestionModifier.setSelected(true);
                    cartQuestionHeader.setCount(cartQuestionHeader.getCount() + 1);
                } else {
                    cartQuestionModifier.setSelected(false);
                    cartQuestionHeader.setCount(cartQuestionHeader.getCount() - 1);
                }
                notifyDataSetChanged();
            }

            if(cartQuestionModifier.getPrice()>0){
                calculateExtraPrice();
            }
        });

        holder.mainLayout.setOnClickListener(v -> {

            fragment.mainActivity.resetHandler();

            if ((min == max) && required == 1) {
                if (!cartQuestionModifier.isSelected()) {
                    if(cartQuestionHeader.getCount()!=1){
                        cartQuestionModifier.setSelected(true);
                        cartQuestionHeader.setCount(1);
                    }
                } else {
                    cartQuestionModifier.setSelected(false);
                    cartQuestionHeader.setCount(0);
                }
                notifyDataSetChanged();

            }

            else if ((min < max)) {

                if (!cartQuestionModifier.isSelected()) {

                    if (cartQuestionHeader.getCount() != max) {
                        cartQuestionModifier.setSelected(true);
                        cartQuestionHeader.setCount(cartQuestionHeader.getCount() + 1);
                    }
                } else {
                    cartQuestionModifier.setSelected(false);
                    cartQuestionHeader.setCount(cartQuestionHeader.getCount() - 1);
                }
                notifyDataSetChanged();

            }

            else if (min == max && required == 0) {

                if (!cartQuestionModifier.isSelected()) {
                    cartQuestionModifier.setSelected(true);
                    cartQuestionHeader.setCount(cartQuestionHeader.getCount() + 1);
                } else {
                    cartQuestionModifier.setSelected(false);
                    cartQuestionHeader.setCount(cartQuestionHeader.getCount() - 1);
                }
                notifyDataSetChanged();
            }

            if(cartQuestionModifier.getPrice()>0){
                calculateExtraPrice();
            }
        });

    }

    public void calculateExtraPrice() {
        fragment.calculatePrice();
    }

    @Override
    public int getItemCount() {
        return cartQuestionModifierList.size();
    }

    static class ModifierSelectionViewHolder extends RecyclerView.ViewHolder {
        TextView description, price;
        CheckBox checkBox;
        LinearLayout mainLayout;

        public ModifierSelectionViewHolder(@NonNull View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.recycler_question_modifier_description);
            price = itemView.findViewById(R.id.recycler_question_modifier_price);
            checkBox = itemView.findViewById(R.id.recycler_question_modifier_checkBox);
            mainLayout = itemView.findViewById(R.id.recycler_question_modifier_mainLayout);
        }
    }

}