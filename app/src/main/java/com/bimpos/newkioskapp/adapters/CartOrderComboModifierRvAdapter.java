package com.bimpos.newkioskapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.Helpers.FormatPrice;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.models.CartComboModifier;

import java.util.List;

public class CartOrderComboModifierRvAdapter extends RecyclerView.Adapter<CartOrderComboModifierRvAdapter.ViewHolder> {

    private final List<CartComboModifier> cartComboModifierList;

    public CartOrderComboModifierRvAdapter(List<CartComboModifier> cartComboModifierList) {
        this.cartComboModifierList = cartComboModifierList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cart_order_combo_modifier, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CartComboModifier cartComboModifier = cartComboModifierList.get(position);
        holder.price.setTextColor(AppSettings.PRICE_TEXT_COLOR);
        holder.modifierDescription.setText(cartComboModifier.getDescription());
        holder.qty.setText(String.valueOf(cartComboModifier.getCount()));
        if (cartComboModifier.getPrice() > 0) {
            holder.price.setText(String.format("+ %s", FormatPrice.getFormattedPrice(cartComboModifier.getPrice())));
        }
    }

    @Override
    public int getItemCount() {
        return cartComboModifierList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView modifierDescription, qty, price;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            modifierDescription = itemView.findViewById(R.id.recycler_cart_order_combo_modifier_description);
            qty = itemView.findViewById(R.id.recycler_cart_order_combo_modifier_count);
            price = itemView.findViewById(R.id.recycler_cart_order_combo_modifier_price);
        }
    }
}
