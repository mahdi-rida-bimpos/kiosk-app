package com.bimpos.newkioskapp.adapters;

import static com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.Helpers.GetBitmap;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.activities.MainActivity;
import com.bimpos.newkioskapp.models.Categories;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import java.util.List;

public class CategoriesRvAdapter extends RecyclerView.Adapter<CategoriesRvAdapter.ViewHolder> {
    private List<Categories> categoriesList;
    private int oldCatPosition = 0;
    private final Context context;
    private final MainActivity mainActivity;
    private static final String TAG = "CategoriesRvAdapter";

    public CategoriesRvAdapter(Context context, MainActivity mainActivity, List<Categories> categoriesList) {
        this.categoriesList = categoriesList;
        this.context = context;
        this.mainActivity = mainActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_categories, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        if (!mainActivity.isProductOpened) {
            Categories categories = categoriesList.get(position);

            if (categoriesList.get(holder.getAdapterPosition()).isSelected() == 1) {
                holder.cardView.setCardBackgroundColor(AppSettings.CATEGORY_SELECTED_COLOR);
            } else {
                holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }
            holder.image.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.category_placeholder));
            if (categories.getHexPicture() != null) {
                if (categories.getHexPicture().length() != 0) {
                    holder.image.setImageBitmap(GetBitmap.getImageBitmap(categories.getHexPicture()));
                }
            }

            holder.categoryName.setText(categories.getDescription());

            holder.itemView.setOnClickListener(view -> {
                if (mainActivity.canClickCategory) {
                    if (!mainActivity.isProductOpened) {
                        changeSelectedCategory(holder.getAdapterPosition());
                        mainActivity.changeCatPosition(categories, holder.getAdapterPosition());
                    }
                }
            });
        }

    }


    public void changeSelectedCategory(int position) {
        Log.d(TAG, "changeSelectedCategory: old "+oldCatPosition);
        Log.d(TAG, "changeSelectedCategory: new "+position);
        categoriesList.get(oldCatPosition).setSelected(0);
        categoriesList.get(position).setSelected(1);
        notifyItemChanged(oldCatPosition);
        notifyItemChanged(position);
        oldCatPosition = position;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Categories> newData) {
        categoriesList = newData;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView categoryName;
        CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.recycler_view_left_category_image);
            categoryName = itemView.findViewById(R.id.recycler_view_left_category_categoryName);
            cardView = itemView.findViewById(R.id.recycler_categories_cardView);
        }
    }
}
