package com.bimpos.newkioskapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.bimpos.newkioskapp.tables.CategoriesTable;
import com.bimpos.newkioskapp.tables.ComboGroupTable;
import com.bimpos.newkioskapp.tables.ComboHeaderTable;
import com.bimpos.newkioskapp.tables.ComboModifierTable;
import com.bimpos.newkioskapp.tables.ProductsTable;
import com.bimpos.newkioskapp.tables.QuestionHeaderTable;
import com.bimpos.newkioskapp.tables.QuestionModifierTable;
import com.bimpos.newkioskapp.tables.QuestionsGroupTable;


public class AppDatabase extends SQLiteOpenHelper {

    private static final String TAG = "AppDatabase";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Kiosk.db";
    public static AppDatabase instance;

    AppDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(TAG, "AppDatabase: start");
    }

    public static AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = new AppDatabase(context.getApplicationContext());
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate: start creation ");

        createCategoriesTable(db);
        CreateProductsTable(db);

        createQuestionGroupTable(db);
        createQuestionHeader(db);
        createQuestionModifiersTable(db);

        createComboGroupTable(db);
        createComboHeaderTable(db);
        createComboModifiersTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    //-table-creation-methods-----------------------------


    private void createQuestionGroupTable(SQLiteDatabase db) {
        String createQuestionGroup =
                "CREATE TABLE " + QuestionsGroupTable.TABLE_NAME + "("
                        + QuestionsGroupTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + QuestionsGroupTable.Columns.QUESTION_GROUP_ID + " INTEGER)";


        db.execSQL(createQuestionGroup);
    }

    private void createQuestionHeader(SQLiteDatabase db) {
        String createQuestionHeader =
                "CREATE TABLE " + QuestionHeaderTable.TABLE_NAME + "( "
                        + QuestionHeaderTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + QuestionHeaderTable.Columns.QUESTION_GROUP_ID + " INTEGER,"
                        + QuestionHeaderTable.Columns.QUESTION_HEADER_ID + " INTEGER,"
                        + QuestionHeaderTable.Columns.MIN + " INTEGER,"
                        + QuestionHeaderTable.Columns.MAX + " INTEGER,"
                        + QuestionHeaderTable.Columns.SEQ + " INTEGER,"
                        + QuestionHeaderTable.Columns.REQUIRED + " INTEGER,"
                        + QuestionHeaderTable.Columns.DESCRIPTION + " TEXT) ";

        db.execSQL(createQuestionHeader);
    }

    private void createQuestionModifiersTable(SQLiteDatabase db) {
        String createQuestionModifiers =
                "CREATE TABLE " + QuestionModifierTable.TABLE_NAME + "("
                        + QuestionModifierTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                        + QuestionModifierTable.Columns.PROD_NUM + " INTEGER, "
                        + QuestionModifierTable.Columns.DESCRIPTION + " TEXT, "
                        + QuestionModifierTable.Columns.PRICE + " double, "
                        + ComboModifierTable.Columns.TAX_1 + " double, "
                        + ComboModifierTable.Columns.TAX_2 + " double, "
                        + ComboModifierTable.Columns.TAX_3 + " double, "
                        + ComboModifierTable.Columns.TAX_EX + " double, "
                        + ComboModifierTable.Columns.AVG_COST + " double, "
                        + ComboModifierTable.Columns.LAST_COST + " double, "
                        + QuestionModifierTable.Columns.QUESTION_GROUP_ID + " INTEGER, "
                        + QuestionModifierTable.Columns.QUESTION_HEADER_ID + " INTEGER)";

        db.execSQL(createQuestionModifiers);
    }


    private void createComboGroupTable(SQLiteDatabase db) {
        String createQuestionGroup =
                "CREATE TABLE " + ComboGroupTable.TABLE_NAME + "("
                        + ComboGroupTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + ComboGroupTable.Columns.PROD_NUM + " INTEGER,"
                        + ComboGroupTable.Columns.COMBO_GROUP_ID + " INTEGER)";

        db.execSQL(createQuestionGroup);
    }

    private void createComboHeaderTable(SQLiteDatabase db) {
        String createQuestionHeader =
                "CREATE TABLE " + ComboHeaderTable.TABLE_NAME + "( "
                        + ComboHeaderTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + ComboHeaderTable.Columns.COMBO_GROUP_ID + " INTEGER,"
                        + ComboHeaderTable.Columns.COMBO_HEADER_ID + " INTEGER,"
                        + ComboHeaderTable.Columns.MIN + " INTEGER,"
                        + ComboHeaderTable.Columns.MAX + " INTEGER,"
                        + ComboHeaderTable.Columns.REQUIRED + " INTEGER,"
                        + ComboHeaderTable.Columns.DESCRIPTION + " TEXT) ";

        db.execSQL(createQuestionHeader);
    }

    private void createComboModifiersTable(SQLiteDatabase db) {
        String createQuestionModifiers =
                "CREATE TABLE " + ComboModifierTable.TABLE_NAME + "("
                        + ComboModifierTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                        + ComboModifierTable.Columns.PROD_NUM + " INTEGER, "
                        + ComboModifierTable.Columns.DESCRIPTION + " TEXT, "
                        + ComboModifierTable.Columns.PRICE + " double, "
                        + ComboModifierTable.Columns.TAX_1 + " double, "
                        + ComboModifierTable.Columns.TAX_2 + " double, "
                        + ComboModifierTable.Columns.TAX_3 + " double, "
                        + ComboModifierTable.Columns.TAX_EX + " double, "
                        + ComboModifierTable.Columns.AVG_COST + " double, "
                        + ComboModifierTable.Columns.LAST_COST + " double, "
                        + ComboModifierTable.Columns.COMBO_HEADER_ID + " INTEGER, "
                        + ComboModifierTable.Columns.COMBO_GROUP_ID + " INTEGER, "
                        + ComboModifierTable.Columns.SEQ + " INTEGER) ";

        db.execSQL(createQuestionModifiers);
    }


    private void CreateProductsTable(SQLiteDatabase db) {
        String createProducts =
                "CREATE TABLE " + ProductsTable.TABLE_NAME + "( "
                        + ProductsTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                        + ProductsTable.Columns.PROD_NUM + " INTEGER, "
                        + ProductsTable.Columns.CAT_ID + " INTEGER, "
                        + ProductsTable.Columns.DESCRIPTION + " TEXT, "
                        + ProductsTable.Columns.PROD_INFO + " TEXT, "
                        + ProductsTable.Columns.PRICE + " double, "
                        + ProductsTable.Columns.AVG_COST + " double, "
                        + ProductsTable.Columns.LAST_COST + " double, "
                        + ProductsTable.Columns.TAX_1 + " INTEGER, "
                        + ProductsTable.Columns.TAX_2 + " INTEGER, "
                        + ProductsTable.Columns.TAX_3 + " INTEGER, "
                        + ProductsTable.Columns.TAX_EX + " INTEGER, "
                        + ProductsTable.Columns.QUESTION_GROUP_ID + " INTEGER, "
                        + ProductsTable.Columns.IS_COMBO + " INTEGER, "
                        + ProductsTable.Columns.HEX_PICTURE + " TEXT)";

        db.execSQL(createProducts);
    }

    private void createCategoriesTable(SQLiteDatabase db) {
        String createCategories =
                "CREATE TABLE " + CategoriesTable.TABLE_NAME + "( "
                        + CategoriesTable.Columns.INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                        + CategoriesTable.Columns.CAT_ID + " INTEGER, "
                        + CategoriesTable.Columns.DESCRIPTION + " TEXT, "
                        + CategoriesTable.Columns.HEX_PICTURE + " TEXT, "
                        + CategoriesTable.Columns.SEQUENCE + " INTEGER, "
                        + CategoriesTable.Columns.SELECTED + " TEXT)";

        db.execSQL(createCategories);
    }


    //-------------------------------
}
