package com.bimpos.newkioskapp.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.bimpos.newkioskapp.models.Categories;
import com.bimpos.newkioskapp.models.ComboGroup;
import com.bimpos.newkioskapp.models.ComboHeader;
import com.bimpos.newkioskapp.models.ComboModifier;
import com.bimpos.newkioskapp.models.Products;
import com.bimpos.newkioskapp.models.QuestionGroup;
import com.bimpos.newkioskapp.models.QuestionHeader;
import com.bimpos.newkioskapp.models.QuestionModifier;
import com.bimpos.newkioskapp.tables.CategoriesTable;
import com.bimpos.newkioskapp.tables.ComboGroupTable;
import com.bimpos.newkioskapp.tables.ComboHeaderTable;
import com.bimpos.newkioskapp.tables.ComboModifierTable;
import com.bimpos.newkioskapp.tables.ProductsTable;
import com.bimpos.newkioskapp.tables.QuestionHeaderTable;
import com.bimpos.newkioskapp.tables.QuestionModifierTable;
import com.bimpos.newkioskapp.tables.QuestionsGroupTable;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper {
    private final AppDatabase database;
    private static final String TAG = "DatabaseHelper";
    Context context;

    public DatabaseHelper(Context context) {
        Log.d(TAG, "DatabaseHelper: start");
        this.context = context;
        database = AppDatabase.getInstance(context);
    }

//--cleaners-------------------------------------------------------------------

    public void cleanAllTables() {

        database.getWritableDatabase().execSQL(String.format("delete from %s;", CategoriesTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", CategoriesTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", ProductsTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", ProductsTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", QuestionsGroupTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", QuestionsGroupTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", QuestionHeaderTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", QuestionHeaderTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", QuestionModifierTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", QuestionModifierTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", ComboGroupTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", ComboGroupTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", ComboHeaderTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", ComboHeaderTable.TABLE_NAME));

        database.getWritableDatabase().execSQL(String.format("delete from %s;", ComboModifierTable.TABLE_NAME));
        database.getWritableDatabase().execSQL(String.format("update sqlite_sequence set seq=0 where name='%s';", ComboModifierTable.TABLE_NAME));

        database.close();
    }


//--categories-----------------------------------------------------------------

    public void insertCategoryList(List<Categories> categoriesList) {

        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();

        for (int i = 0; i < categoriesList.size(); i++) {
            Categories category = categoriesList.get(i);
            contentValues.put(CategoriesTable.Columns.CAT_ID, category.getCatId());
            contentValues.put(CategoriesTable.Columns.DESCRIPTION, category.getDescription());
            contentValues.put(CategoriesTable.Columns.HEX_PICTURE, category.getHexPicture());
            contentValues.put(CategoriesTable.Columns.SEQUENCE, category.getSeq());

            db.insert(CategoriesTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        database.close();
    }

    @SuppressLint("Range")
    public List<Categories> getAllCategories() {
        String query = " SELECT " + CategoriesTable.Columns.CAT_ID + ", "
                + CategoriesTable.Columns.DESCRIPTION + ", "
                + CategoriesTable.Columns.HEX_PICTURE + ", "
                + CategoriesTable.Columns.SEQUENCE + ", "
                + CategoriesTable.Columns.SELECTED
                + " FROM " + CategoriesTable.TABLE_NAME
                + " ORDER BY CAST(" + CategoriesTable.Columns.SEQUENCE + " AS INTEGER) ASC ";

        Log.d(TAG, "getAllCategories: query " + query);
        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<Categories> categoriesList = new ArrayList<>();

        while (cursor.moveToNext()) {

            Categories categories = new Categories();
            categories.setCatId(cursor.getInt(0));
            categories.setDescription(cursor.getString(1));
            categories.setHexPicture(cursor.getString(2));
            categories.setSeq(cursor.getInt(3));
            categories.setSelected(0);

            Log.d(TAG, "getAllCategories: category " + categories.toString());
            categoriesList.add(categories);
        }
        cursor.close();
        database.close();
        return categoriesList;
    }


//--products-------------------------------------------------------------------

    public void insertProductList(List<Products> productsList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < productsList.size(); i++) {
            Products products = productsList.get(i);
            contentValues.put(ProductsTable.Columns.PROD_NUM, products.getProdNum());
            contentValues.put(ProductsTable.Columns.CAT_ID, products.getCatId());
            contentValues.put(ProductsTable.Columns.DESCRIPTION, products.getDescription());
            contentValues.put(ProductsTable.Columns.PROD_INFO, products.getProdInfo());
            contentValues.put(ProductsTable.Columns.PRICE, products.getPrice());
            contentValues.put(ProductsTable.Columns.AVG_COST, products.getAvgCost());
            contentValues.put(ProductsTable.Columns.LAST_COST, products.getLastCost());
            contentValues.put(ProductsTable.Columns.TAX_1, products.getTax1());
            contentValues.put(ProductsTable.Columns.TAX_2, products.getTax2());
            contentValues.put(ProductsTable.Columns.TAX_3, products.getTax3());
            contentValues.put(ProductsTable.Columns.TAX_EX, products.getTaxEx());
            contentValues.put(ProductsTable.Columns.HEX_PICTURE, products.getHexPicture());
            contentValues.put(ProductsTable.Columns.QUESTION_GROUP_ID, products.getQuestionGroupId());
            contentValues.put(ProductsTable.Columns.IS_COMBO, products.getIsCombo());
            db.insert(ProductsTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        database.close();
    }

    @SuppressLint("Range")
    public List<Products> getProductsByCatId(int catId) {

        String query = "SELECT * FROM " + ProductsTable.TABLE_NAME
                + " WHERE " + ProductsTable.Columns.CAT_ID
                + " = " + catId;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<Products> productsList = new ArrayList<>();

        while (cursor.moveToNext()) {
            Products products = new Products(
                    cursor.getInt(cursor.getColumnIndex(ProductsTable.Columns.PROD_NUM)),
                    cursor.getInt(cursor.getColumnIndex(ProductsTable.Columns.CAT_ID)),
                    cursor.getInt(cursor.getColumnIndex(ProductsTable.Columns.QUESTION_GROUP_ID)),
                    cursor.getInt(cursor.getColumnIndex(ProductsTable.Columns.IS_COMBO)),
                    cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.DESCRIPTION)),
                    cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.PROD_INFO)),
                    cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.HEX_PICTURE)),
                    cursor.getDouble(cursor.getColumnIndex(ProductsTable.Columns.PRICE)),
                    cursor.getDouble(cursor.getColumnIndex(ProductsTable.Columns.AVG_COST)),
                    cursor.getDouble(cursor.getColumnIndex(ProductsTable.Columns.LAST_COST)),
                    cursor.getDouble(cursor.getColumnIndex(ProductsTable.Columns.TAX_1)),
                    cursor.getDouble(cursor.getColumnIndex(ProductsTable.Columns.TAX_2)),
                    cursor.getDouble(cursor.getColumnIndex(ProductsTable.Columns.TAX_3)),
                    cursor.getDouble(cursor.getColumnIndex(ProductsTable.Columns.TAX_EX))
            );

            productsList.add(products);
        }
        cursor.close();
        database.close();
        return productsList;
    }

    @SuppressLint("Range")
    public double getProductPrice(int prodNum){
        double price=0;
        String query = "SELECT "+ProductsTable.Columns.PRICE+" FROM " + ProductsTable.TABLE_NAME
                + " WHERE " + ProductsTable.Columns.PROD_NUM
                + " = " + prodNum;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);

        if (cursor.moveToNext()) {
            price=cursor.getDouble(cursor.getColumnIndex(ProductsTable.Columns.PRICE));
        }
        cursor.close();
        database.close();
        return price;
    }

//--questions------------------------------------------------------------------

    public void insertQuestionGroupList(List<QuestionGroup> questionGroupList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < questionGroupList.size(); i++) {
            QuestionGroup questionGroup = questionGroupList.get(i);
            contentValues.put(QuestionsGroupTable.Columns.QUESTION_GROUP_ID, questionGroup.getQuestionGroupId());
            db.insert(QuestionsGroupTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        database.close();
    }

    public void insertQuestionHeaderList(List<QuestionHeader> questionHeaderList) {

        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();

        for (int i = 0; i < questionHeaderList.size(); i++) {
            QuestionHeader questionHeader = questionHeaderList.get(i);
            contentValues.put(QuestionHeaderTable.Columns.QUESTION_GROUP_ID, questionHeader.getQuestionGroupId());
            contentValues.put(QuestionHeaderTable.Columns.QUESTION_HEADER_ID, questionHeader.getQuestionHeaderId());
            contentValues.put(QuestionHeaderTable.Columns.MIN, questionHeader.getMin());
            contentValues.put(QuestionHeaderTable.Columns.MAX, questionHeader.getMax());
            contentValues.put(QuestionHeaderTable.Columns.REQUIRED, questionHeader.getRequired());
            contentValues.put(QuestionHeaderTable.Columns.SEQ, questionHeader.getSeq());
            contentValues.put(QuestionHeaderTable.Columns.DESCRIPTION, questionHeader.getDescription());

            db.insert(QuestionHeaderTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        database.close();
    }

    public void insertQuestionModifierList(List<QuestionModifier> questionModifierList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();

        for (int i = 0; i < questionModifierList.size(); i++) {
            QuestionModifier modifier = questionModifierList.get(i);
            contentValues.put(QuestionModifierTable.Columns.PROD_NUM, modifier.getProdNum());
            contentValues.put(QuestionModifierTable.Columns.DESCRIPTION, modifier.getDescription());
            contentValues.put(QuestionModifierTable.Columns.PRICE, modifier.getPrice());
            contentValues.put(QuestionModifierTable.Columns.TAX_1, modifier.getTax1());
            contentValues.put(QuestionModifierTable.Columns.TAX_2, modifier.getTax2());
            contentValues.put(QuestionModifierTable.Columns.TAX_3, modifier.getTax3());
            contentValues.put(QuestionModifierTable.Columns.TAX_EX, modifier.getTaxEx());
            contentValues.put(QuestionModifierTable.Columns.AVG_COST, modifier.getAvgCost());
            contentValues.put(QuestionModifierTable.Columns.LAST_COST, modifier.getLastCost());
            contentValues.put(QuestionModifierTable.Columns.QUESTION_HEADER_ID, modifier.getQuestionHeaderId());
            contentValues.put(QuestionModifierTable.Columns.QUESTION_GROUP_ID, modifier.getQuestionGroupId());

            db.insert(QuestionModifierTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        database.close();
    }


    @SuppressLint("Range")
    public List<QuestionHeader> getQuestionHeaderByQgId(int questionGroupId) {

        String query = "SELECT * FROM " + QuestionHeaderTable.TABLE_NAME
                + " WHERE " + QuestionHeaderTable.Columns.QUESTION_GROUP_ID
                + " = " + questionGroupId
                + " ORDER BY " + QuestionHeaderTable.Columns.SEQ
                + " ASC";

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<QuestionHeader> questionHeaderList = new ArrayList<>();
        while (cursor.moveToNext()) {
            QuestionHeader questionHeader = new QuestionHeader(
                    cursor.getInt(cursor.getColumnIndex(QuestionHeaderTable.Columns.QUESTION_GROUP_ID)),
                    cursor.getInt(cursor.getColumnIndex(QuestionHeaderTable.Columns.QUESTION_HEADER_ID)),
                    cursor.getInt(cursor.getColumnIndex(QuestionHeaderTable.Columns.MIN)),
                    cursor.getInt(cursor.getColumnIndex(QuestionHeaderTable.Columns.MAX)),
                    cursor.getInt(cursor.getColumnIndex(QuestionHeaderTable.Columns.REQUIRED)),
                    cursor.getInt(cursor.getColumnIndex(QuestionHeaderTable.Columns.SEQ)),
                    cursor.getString(cursor.getColumnIndex(QuestionHeaderTable.Columns.DESCRIPTION))
            );
            questionHeaderList.add(questionHeader);
        }
        cursor.close();
        database.close();
        return questionHeaderList;
    }

    @SuppressLint("Range")
    public List<QuestionModifier> getQuestionModifierByQhId(int questionGroupId, int questionHeaderId) {

        String query = "SELECT *"
                + " FROM " + QuestionModifierTable.TABLE_NAME
                + " WHERE " + QuestionModifierTable.Columns.QUESTION_GROUP_ID
                + " = " + questionGroupId
                + " AND " + QuestionModifierTable.Columns.QUESTION_HEADER_ID
                + " = " + questionHeaderId;


        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<QuestionModifier> questionModifierList = new ArrayList<>();
        while (cursor.moveToNext()) {
            QuestionModifier questionModifier = new QuestionModifier(
                    cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.PROD_NUM)),
                    cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.QUESTION_GROUP_ID)),
                    cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.QUESTION_HEADER_ID)),
                    cursor.getDouble(cursor.getColumnIndex(QuestionModifierTable.Columns.PRICE)),
                    cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.TAX_1)),
                    cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.TAX_2)),
                    cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.TAX_3)),
                    cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.TAX_EX)),
                    cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.AVG_COST)),
                    cursor.getInt(cursor.getColumnIndex(QuestionModifierTable.Columns.LAST_COST)),
                    cursor.getString(cursor.getColumnIndex(QuestionModifierTable.Columns.DESCRIPTION)));

            questionModifierList.add(questionModifier);
        }
        cursor.close();
        database.close();
        return questionModifierList;
    }


//--combos---------------------------------------------------------------------

    public void insertComboGroupList(List<ComboGroup> comboGroupList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();

        for (int i = 0; i < comboGroupList.size(); i++) {
            ComboGroup comboGroup = comboGroupList.get(i);
            contentValues.put(ComboGroupTable.Columns.COMBO_GROUP_ID, comboGroup.getComboGroupId());
            contentValues.put(ComboGroupTable.Columns.PROD_NUM, comboGroup.getProdNum());

            db.insert(ComboGroupTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        database.close();
    }

    public void insertComboHeaderList(List<ComboHeader> comboHeaderList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < comboHeaderList.size(); i++) {
            ComboHeader comboHeader = comboHeaderList.get(i);
            contentValues.put(ComboHeaderTable.Columns.COMBO_GROUP_ID, comboHeader.getComboGroupId());
            contentValues.put(ComboHeaderTable.Columns.COMBO_HEADER_ID, comboHeader.getComboHeaderId());
            contentValues.put(ComboHeaderTable.Columns.MIN, comboHeader.getMin());
            contentValues.put(ComboHeaderTable.Columns.MAX, comboHeader.getMax());
            contentValues.put(ComboHeaderTable.Columns.REQUIRED, comboHeader.getRequired());
            contentValues.put(ComboHeaderTable.Columns.DESCRIPTION, comboHeader.getDescription());

            db.insert(ComboHeaderTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        database.close();
    }

    public void insertComboModifierList(List<ComboModifier> comboModifierList) {
        SQLiteDatabase db = database.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < comboModifierList.size(); i++) {
            ComboModifier modifier = comboModifierList.get(i);
            contentValues.put(ComboModifierTable.Columns.PROD_NUM, modifier.getProdNum());
            contentValues.put(ComboModifierTable.Columns.DESCRIPTION, modifier.getDescription());
            contentValues.put(ComboModifierTable.Columns.PRICE, modifier.getPrice());
            contentValues.put(ComboModifierTable.Columns.TAX_1, modifier.getTax1());
            contentValues.put(ComboModifierTable.Columns.TAX_2, modifier.getTax2());
            contentValues.put(ComboModifierTable.Columns.TAX_3, modifier.getTax3());
            contentValues.put(ComboModifierTable.Columns.TAX_EX, modifier.getTaxEx());
            contentValues.put(ComboModifierTable.Columns.AVG_COST, modifier.getAvgCost());
            contentValues.put(ComboModifierTable.Columns.LAST_COST, modifier.getLastCost());
            contentValues.put(ComboModifierTable.Columns.SEQ, modifier.getSeq());
            contentValues.put(ComboModifierTable.Columns.COMBO_HEADER_ID, modifier.getComboHeaderId());
            contentValues.put(ComboModifierTable.Columns.COMBO_GROUP_ID, modifier.getComboGroupId());

            db.insert(ComboModifierTable.TABLE_NAME, null, contentValues);
            contentValues.clear();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        database.close();
    }

    @SuppressLint("Range")
    public List<ComboGroup> getComboGroupByProdNum(int prodNum) {

        String query = "SELECT * FROM " + ComboGroupTable.TABLE_NAME
                + " WHERE " + ComboGroupTable.Columns.PROD_NUM
                + " = " + prodNum;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<ComboGroup> comboGroupList = new ArrayList<>();
        while (cursor.moveToNext()) {
            ComboGroup comboGroup = new ComboGroup(
                    cursor.getInt(cursor.getColumnIndex(ComboGroupTable.Columns.COMBO_GROUP_ID)),
                    cursor.getInt(cursor.getColumnIndex(ComboGroupTable.Columns.PROD_NUM))
            );
            comboGroupList.add(comboGroup);
        }
        cursor.close();
        database.close();
        return comboGroupList;
    }

    @SuppressLint("Range")
    public List<ComboHeader> getComboHeaderByGroupId(int comboGroupId) {

        String query = "SELECT * FROM " + ComboHeaderTable.TABLE_NAME
                + " WHERE " + ComboHeaderTable.Columns.COMBO_GROUP_ID
                + " = " + comboGroupId;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);
        List<ComboHeader> comboHeaderList = new ArrayList<>();
        while (cursor.moveToNext()) {
            ComboHeader comboHeader = new ComboHeader(
                    cursor.getInt(cursor.getColumnIndex(ComboHeaderTable.Columns.COMBO_GROUP_ID)),
                    cursor.getInt(cursor.getColumnIndex(ComboHeaderTable.Columns.COMBO_HEADER_ID)),
                    cursor.getInt(cursor.getColumnIndex(ComboHeaderTable.Columns.MIN)),
                    cursor.getInt(cursor.getColumnIndex(ComboHeaderTable.Columns.MAX)),
                    cursor.getInt(cursor.getColumnIndex(ComboHeaderTable.Columns.REQUIRED)),
                    cursor.getString(cursor.getColumnIndex(ComboHeaderTable.Columns.DESCRIPTION))
            );
            comboHeaderList.add(comboHeader);
        }
        cursor.close();
        database.close();
        return comboHeaderList;
    }

    @SuppressLint("Range")
    public List<ComboModifier> getComboModifierByGroupId(int comboGroupId, int comboHeaderId) {
        List<ComboModifier> comboModifierList = new ArrayList<>();
        String query = "SELECT * FROM " + ComboModifierTable.TABLE_NAME
                + " WHERE " + ComboModifierTable.Columns.COMBO_GROUP_ID
                + " = " + comboGroupId
                + " AND " + ComboModifierTable.Columns.COMBO_HEADER_ID
                + " = " + comboHeaderId;

        Cursor cursor = database.getReadableDatabase().rawQuery(query, null);

        while (cursor.moveToNext()) {
            ComboModifier comboModifier = new ComboModifier(
                    cursor.getInt(cursor.getColumnIndex(ComboModifierTable.Columns.PROD_NUM)),
                    cursor.getInt(cursor.getColumnIndex(ComboModifierTable.Columns.COMBO_GROUP_ID)),
                    cursor.getInt(cursor.getColumnIndex(ComboModifierTable.Columns.COMBO_HEADER_ID)),
                    cursor.getDouble(cursor.getColumnIndex(ComboModifierTable.Columns.PRICE)),
                    cursor.getInt(cursor.getColumnIndex(ComboModifierTable.Columns.TAX_1)),
                    cursor.getInt(cursor.getColumnIndex(ComboModifierTable.Columns.TAX_2)),
                    cursor.getInt(cursor.getColumnIndex(ComboModifierTable.Columns.TAX_3)),
                    cursor.getInt(cursor.getColumnIndex(ComboModifierTable.Columns.TAX_EX)),
                    cursor.getInt(cursor.getColumnIndex(ComboModifierTable.Columns.AVG_COST)),
                    cursor.getInt(cursor.getColumnIndex(ComboModifierTable.Columns.LAST_COST)),
                    cursor.getInt(cursor.getColumnIndex(ComboModifierTable.Columns.SEQ)),
                    cursor.getString(cursor.getColumnIndex(ComboModifierTable.Columns.DESCRIPTION))
            );
            comboModifierList.add(comboModifier);
        }
        cursor.close();
        database.close();
        return comboModifierList;
    }

}


















