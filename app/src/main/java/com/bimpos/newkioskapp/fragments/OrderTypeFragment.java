package com.bimpos.newkioskapp.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.Helpers.Constant;
import com.bimpos.newkioskapp.Helpers.CustomDialog;
import com.bimpos.newkioskapp.Helpers.GetBitmap;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.activities.MainActivity;
import com.bimpos.newkioskapp.activities.SettingsActivity;
import com.bimpos.newkioskapp.databinding.FragmentOrderTypeBinding;
import com.bimpos.newkioskapp.models.Cart;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hardik.clickshrinkeffect.ClickShrinkEffect;

import java.util.Timer;
import java.util.TimerTask;

public class OrderTypeFragment extends Fragment implements CustomDialog.DialogEvents {

    private static final String TAG = "OrderTypeFragment";
    private FragmentOrderTypeBinding binding;
    private final OnOrderTypeClicked listener;
    private MainActivity mainActivity;
    public boolean canClick = true;
    private final Cart cartData;
    private int count = 0;
    private Timer timer;
    private final String passwordText;
    private CustomDialog dialog;


    public interface OnOrderTypeClicked {

        void onOrderTypeClicked();
    }

    public OrderTypeFragment(OnOrderTypeClicked listener,MainActivity mainActivity, SharedPreferences preferences) {
        cartData = Cart.getCartData();
        this.listener = listener;
        this.mainActivity = mainActivity;
        passwordText = preferences.getString(Constant.PASSWORD, "");
        Log.d(TAG, "OrderTypeFragment: password " + passwordText);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentOrderTypeBinding.inflate(getLayoutInflater());

        initVariables();
        checkOrderTypes();

        binding.mainLayout.setOnTouchListener((view, motionEvent) -> {
            Log.d(TAG, "onCreateView: event " + motionEvent.getAction());
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                mainActivity.removeHandler();
                startCounting();
            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                mainActivity.resumeHandler();
                timer.cancel();
                count = 0;
            }
            return true;
        });


        binding.eatText.setText(AppSettings.GREETING_ORDER_TYPE_SCREEN);

        Glide.with(this)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(GetBitmap.getImageBytes(AppSettings.RESTAURANT_IMAGE))
                .placeholder(R.drawable.bimposlogo)
                .into(binding.restaurantPicture);

        binding.eatInButton.setOnClickListener(view -> {
            if (canClick) {
                binding.takeAwayButton.setOnClickListener(null);
                canClick = false;
                binding.eatInCheckedIcon.animate().scaleX(1).scaleY(1).setDuration(50).start();
                binding.takeAwayCheckedIcon.animate().scaleX(0).scaleY(0).setDuration(50).start();
                cartData.setOrderType(AppSettings.DINE_IN);
                new Handler().postDelayed(listener::onOrderTypeClicked, 200);
            }
        });

        binding.takeAwayButton.setOnClickListener(view -> {
            if (canClick) {
                binding.eatInButton.setOnClickListener(null);
                canClick = false;
                cartData.setOrderType(AppSettings.TAKE_AWAY);
                binding.takeAwayCheckedIcon.animate().scaleX(1).scaleY(1).setDuration(50).start();
                binding.eatInCheckedIcon.animate().scaleX(0).scaleY(0).setDuration(50).start();
                new Handler().postDelayed(listener::onOrderTypeClicked, 200);
            }
        });

        return binding.getRoot();
    }

    private void checkOrderTypes() {
        if (AppSettings.ORDER_TYPE_MAP.size() == 0) {
            showDialog();
        }
        if (AppSettings.ORDER_TYPE_MAP.size() == 1) {
            for (int key : AppSettings.ORDER_TYPE_MAP.keySet()) {
                cartData.setOrderType(key);
            }
            listener.onOrderTypeClicked();

        } else {
            if (AppSettings.ORDER_TYPE_MAP.containsKey(AppSettings.DINE_IN)) {
                binding.eatInText.setText(AppSettings.ORDER_TYPE_MAP.get(AppSettings.DINE_IN));
                binding.eatInButton.setVisibility(View.VISIBLE);
            } else {
                binding.eatInButton.setVisibility(View.GONE);
            }

            if (AppSettings.ORDER_TYPE_MAP.containsKey(AppSettings.TAKE_AWAY)) {
                binding.takeAwayText.setText(AppSettings.ORDER_TYPE_MAP.get(AppSettings.TAKE_AWAY));
                binding.takeAwayButton.setVisibility(View.VISIBLE);
            } else {
                binding.takeAwayButton.setVisibility(View.GONE);
            }
        }
    }

    private void showDialog() {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "There is no order type in your database, we can't complete");
        arguments.putString(CustomDialog.DIALOG_TITLE, "No Order Type");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "exit");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "null");
        arguments.putInt(CustomDialog.DIALOG_ID, 1);
        dialog.setArguments(arguments);
        dialog.show(requireActivity().getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    private void initVariables() {
        new ClickShrinkEffect(binding.eatInButton);
        new ClickShrinkEffect(binding.takeAwayButton);
        dialog = new CustomDialog(this);
    }

    private void startCounting() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                count++;
                Log.d(TAG, "run: count " + count);
                if (count == 3) {
                    Log.d(TAG, "run: enter settings");
                    timer.cancel();
                    count = 0;
                    requireActivity().runOnUiThread(() -> {
                        if (passwordText.length() == 0) {
                            gotoSettingsActivity();
                        } else {
                            showPasswordDialog();
                        }
                    });
                }
            }
        }, 0, 1000);
    }

    private void showPasswordDialog() {

        Log.d(TAG, "showPasswordDialog: start");
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.custom_password_layout, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        AlertDialog dialog = builder.create();
        EditText password = view.findViewById(R.id.custom_password_layout_password);
        password.setInputType(InputType.TYPE_CLASS_NUMBER);
        password.requestFocus();
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d(TAG, "onTextChanged: s" + charSequence);
                if (charSequence.toString().equalsIgnoreCase(passwordText)) {
                    Log.d(TAG, "onTextChanged: enter");
                    dialog.dismiss();
                    password.clearFocus();
                    gotoSettingsActivity();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        dialog.show();

    }

    private void gotoSettingsActivity() {
        requireActivity().stopLockTask();
        mainActivity.removeHandler();
        startActivity(new Intent(getActivity(), SettingsActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        requireActivity().finish();
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {

    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }
}