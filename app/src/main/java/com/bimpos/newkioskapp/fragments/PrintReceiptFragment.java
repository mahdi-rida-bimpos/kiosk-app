package com.bimpos.newkioskapp.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bimpos.newkioskapp.activities.MainActivity;
import com.bimpos.newkioskapp.adapters.PrinterItemsAdapter;
import com.bimpos.newkioskapp.databinding.FragmentPrintReceiptBinding;
import com.bimpos.newkioskapp.models.Cart;
import com.bimpos.newkioskapp.printing.PrinterClass;

public class PrintReceiptFragment extends Fragment {

    public FragmentPrintReceiptBinding binding;
    private MainActivity mainActivity;
    private Cart cart;
    private static final String TAG = "PrintReceipt";


    public PrintReceiptFragment(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentPrintReceiptBinding.inflate(getLayoutInflater());
        cart = Cart.getCartData();
        PrinterItemsAdapter adapter = new PrinterItemsAdapter(cart.getCartItemsList());

        binding.layoutPrinterRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.layoutPrinterRecyclerView.setAdapter(adapter);

        binding.print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrinterClass testFunction = new PrinterClass();
                testFunction.printReceipt(mainActivity, 1);
            }
        });


        return binding.getRoot();
    }


}