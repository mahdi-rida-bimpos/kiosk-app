package com.bimpos.newkioskapp.fragments;

import static android.app.Activity.RESULT_OK;

import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.newkioskapp.Helpers.Constant;
import com.bimpos.newkioskapp.adapters.BannersAdapter;
import com.bimpos.newkioskapp.databinding.ViewPagerBannerBinding;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class BannerFragment extends Fragment {

    private ViewPagerBannerBinding binding;

    private static final String TAG = "BannerFragment";
    private final int PICK_IMAGE_MULTIPLE = 1;
    public List<Uri> uriList = new ArrayList<>();
    private BannersAdapter adapter;
    // list of file paths
    File[] listFile;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = ViewPagerBannerBinding.inflate(getLayoutInflater());

        initRecyclerView();
        checkExistingImages();

        binding.selectImage.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE);
        });
        return binding.getRoot();
    }

    private void checkExistingImages() {
        String projectRoot = Environment.getExternalStorageDirectory().getPath();
        File rootFolder = new File(projectRoot, Constant.ROOT_FOLDER);
        File bannersFolder = new File(rootFolder, Constant.BANNERS_FOLDER);
        if (bannersFolder.isDirectory()) {
            listFile = bannersFolder.listFiles();
            assert listFile != null;
            if (listFile.length != 0) {
                for (File file : listFile) {
                    uriList.add(Uri.fromFile(file));
                }
                Log.d(TAG, "checkExistingImages: images found "+uriList.size());
                adapter.setData(uriList);
            }
        }
    }

    private void initRecyclerView() {
        uriList = new ArrayList<>();
        adapter = new BannersAdapter( new ArrayList<>());
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        manager.setOrientation(RecyclerView.HORIZONTAL);
        binding.recyclerView.setLayoutManager(manager);
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        // When an Image is picked
        if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && null != data) {

            // Get the Image from data
            if (data.getClipData() != null) {
                Log.d(TAG, "onActivityResult: data " + data.getClipData().getItemCount());
                ClipData mClipData = data.getClipData();
                int count = data.getClipData().getItemCount();
                for (int i = 0; i < count; i++) {
                    Uri imageUri = data.getClipData().getItemAt(i).getUri();
                    if (!uriList.contains(imageUri)) {
                        if (checkFileSize(imageUri)) {
                            uriList.add(imageUri);
                        }
                    }
                }
            } else {
                Log.d(TAG, "onActivityResult: got one");
                Uri imageUri = data.getData();
                checkFileSize(imageUri);
                if (!uriList.contains(imageUri)) {
                    if (checkFileSize(imageUri)) {
                        uriList.add(imageUri);
                    }
                }
            }
            adapter.setData(uriList);

        } else {
            // show this if no image is selected
            Toast.makeText(getActivity(), "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }

    private boolean checkFileSize(Uri imageUri) {
        try {
            InputStream fileInputStream = requireActivity().getContentResolver().openInputStream(imageUri);
            int size = fileInputStream.available();
            Log.d(TAG, "onActivityResult: size " + size);
            return size <= 1000000;

        } catch (IOException e) {
            return false;
        }
    }

}
