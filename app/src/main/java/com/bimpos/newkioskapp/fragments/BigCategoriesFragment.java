package com.bimpos.newkioskapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;

import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.activities.MainActivity;
import com.bimpos.newkioskapp.adapters.BigCategoriesAdapter;
import com.bimpos.newkioskapp.database.DatabaseHelper;
import com.bimpos.newkioskapp.databinding.FragmentBigCategoriesBinding;
import com.bimpos.newkioskapp.models.Categories;
import com.hardik.clickshrinkeffect.ClickShrinkEffect;

import java.util.List;

public class BigCategoriesFragment extends Fragment implements BigCategoriesAdapter.OnBigCategoryClicked {

    private final MainActivity activity;
    private FragmentBigCategoriesBinding binding;
    private DatabaseHelper databaseHelper;
    List<Categories> categoriesList;

    public BigCategoriesFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentBigCategoriesBinding.inflate(getLayoutInflater());

        databaseHelper = new DatabaseHelper(activity);
        binding.cancelButton.setBackgroundColor(AppSettings.BOTTOM_NO_ENABLED_BACK_COLOR);
        initRecyclerView();
        initClickListeners();
        activity.canClickCategory = false;
        return binding.getRoot();
    }

    private void initClickListeners() {
        binding.cancelButton.setOnClickListener(v -> activity.openScreenSaver());
    }

    private void initRecyclerView() {

        categoriesList = databaseHelper.getAllCategories();
        GridLayoutManager manager = new GridLayoutManager(getActivity(), AppSettings.BIG_CAT_SPAN_COUNT);

        binding.recyclerView.setLayoutManager(manager);
        BigCategoriesAdapter adapter = new BigCategoriesAdapter(categoriesList,getActivity(), this, AppSettings.BIG_CAT_SPAN_COUNT);
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBigCategoryClicked(Categories categories, int position) {

        activity.closeBigCategoryFragment(categories, position);
    }
}
