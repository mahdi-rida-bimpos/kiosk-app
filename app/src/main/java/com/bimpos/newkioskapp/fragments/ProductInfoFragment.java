package com.bimpos.newkioskapp.fragments;

import static com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.Helpers.FormatPrice;
import com.bimpos.newkioskapp.Helpers.GetBitmap;
import com.bimpos.newkioskapp.Helpers.MyAnimation;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.activities.MainActivity;
import com.bimpos.newkioskapp.adapters.ProductInfoRecyclerAdapter;
import com.bimpos.newkioskapp.database.DatabaseHelper;
import com.bimpos.newkioskapp.databinding.FragmentProductInfoBinding;
import com.bimpos.newkioskapp.models.CartComboHeader;
import com.bimpos.newkioskapp.models.CartComboModifier;
import com.bimpos.newkioskapp.models.CartQuestionHeader;
import com.bimpos.newkioskapp.models.CartQuestionModifier;
import com.bimpos.newkioskapp.models.ComboGroup;
import com.bimpos.newkioskapp.models.ComboHeader;
import com.bimpos.newkioskapp.models.ComboModifier;
import com.bimpos.newkioskapp.models.Products;
import com.bimpos.newkioskapp.models.QuestionHeader;
import com.bimpos.newkioskapp.models.QuestionModifier;
import com.bumptech.glide.Glide;
import com.hardik.clickshrinkeffect.ClickShrinkEffect;

import java.util.ArrayList;
import java.util.List;

public class ProductInfoFragment extends Fragment {


    public FragmentProductInfoBinding binding;
    public final MainActivity mainActivity;
    public Products product;
    private Animation animation;
    private static final String TAG = "ProductInfo";
    private DatabaseHelper databaseHelper;
    public ProductInfoRecyclerAdapter recyclerAdapter;
    List<Object> objectList = new ArrayList<>();


    public ProductInfoFragment(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentProductInfoBinding.inflate(getLayoutInflater());
        initVariables();
        checkRemarkLayout();
        checkColors();
        initRecyclerView();
        getProduct();
        initClickListeners();
        mainActivity.isProductFragmentOpened = true;

        return binding.getRoot();
    }

    private void checkRemarkLayout() {
        if(AppSettings.REMARK_NUM_VALUE>0){
            binding.remarkLayout.setVisibility(View.VISIBLE);
        }else{
            binding.remarkLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        mainActivity.isProductFragmentOpened = false;
        super.onDestroy();
    }

    private void checkColors() {
        binding.plusBtn.setBackgroundColor(AppSettings.BUTTON_PLUS_ENABLED_BACK_COLOR);
        binding.plusBtn.getDrawable().setTint(AppSettings.BUTTON_PLUS_ENABLED_TEXT_COLOR);
        binding.minusBtn.setBackgroundColor(AppSettings.BUTTON_MINUS_ENABLED_BACK_COLOR);
        binding.minusBtn.getDrawable().setTint(AppSettings.BUTTON_MINUS_ENABLED_TEXT_COLOR);
        binding.totalPrice.setTextColor(AppSettings.PRICE_TEXT_COLOR);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (product.getQuestionGroupId() == 0 & product.getIsCombo() == 0) {
            binding.recyclerView.setVisibility(View.GONE);
        } else {
            new Handler().postDelayed(this::getDataFromDb, 1000);
        }
    }

    private void getDataFromDb() {
        List<QuestionHeader> questionHeaderList = new ArrayList<>();
        List<ComboHeader> comboHeaderList;

        //fetch combo
        if (product.getIsCombo() == 1) {
            Log.d(TAG, "getDataFromDb: get combo");
            int prodNum = product.getProdNum();
            List<ComboGroup> comboGroupList = databaseHelper.getComboGroupByProdNum(prodNum);
            for (int i = 0; i < comboGroupList.size(); i++) {
                int comboGroupId = comboGroupList.get(i).getComboGroupId();
                comboHeaderList = databaseHelper.getComboHeaderByGroupId(comboGroupId);
                for (int ii = 0; ii < comboHeaderList.size(); ii++) {
                    int comboHeaderId = comboHeaderList.get(ii).getComboHeaderId();
                    List<ComboModifier> comboModifierList = databaseHelper.getComboModifierByGroupId(comboGroupId, comboHeaderId);
                    comboHeaderList.get(ii).setModifiersList(comboModifierList);
                }
                Log.d(TAG, "getDataFromDb: combo header list "+comboHeaderList.size());
                objectList.addAll(comboHeaderList);
            }
        }
        //fetch questions
        if (product.getQuestionGroupId() != 0) {
            Log.d(TAG, "getDataFromDb: get question");
            int questionGroupId = product.getQuestionGroupId();
            if (questionGroupId > 0) {
                questionHeaderList = databaseHelper.getQuestionHeaderByQgId(questionGroupId);
                for (int i = 0; i < questionHeaderList.size(); i++) {
                    int questionHeaderId = questionHeaderList.get(i).getQuestionHeaderId();
                    List<QuestionModifier> questionModifierList = databaseHelper.getQuestionModifierByQhId(questionGroupId, questionHeaderId);
                    questionHeaderList.get(i).setModifiersList(questionModifierList);
                }
            }
            Log.d(TAG, "getDataFromDb: question header list "+questionHeaderList.size());
            objectList.addAll(questionHeaderList);
        }

        if(objectList.size()==0){
            binding.recyclerView.setVisibility(View.GONE);
            mainActivity.enableCartButton();
        }else{
            recyclerAdapter.setData(objectList);
        }

    }

    private void initRecyclerView() {
        recyclerAdapter = new ProductInfoRecyclerAdapter(this, getActivity(), objectList);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recyclerView.setAdapter(recyclerAdapter);
    }

    private void initVariables() {

        databaseHelper = new DatabaseHelper(getActivity());
        new ClickShrinkEffect(binding.minusBtn);
        new ClickShrinkEffect(binding.plusBtn);
        animation = MyAnimation.getAnimation(getContext());
    }

    private void getProduct() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            product = (Products) arguments.getSerializable(Products.class.getSimpleName());
            if (product != null) {
                fillContent();
            } else {
                requireActivity().onBackPressed();
            }
        }
    }

    private void fillContent() {

        if(product.getHexPicture()!=null){
            if(product.getHexPicture().length()!=0){
                Glide.with(mainActivity)
                        .asBitmap()
                        .load(GetBitmap.getImageBytes(product.getHexPicture()))
                        .transition(withCrossFade())
                        .placeholder(R.drawable.category_placeholder)
                        .into(binding.productImage);
            }
        }
        binding.productDescription.setText(product.getDescription());
        binding.totalPrice.setText(FormatPrice.getFormattedPrice(product.getPrice()));
    }

    private void initClickListeners() {

        binding.plusBtn.setOnClickListener(view -> {
            mainActivity.resetHandler();
            int quantity = Integer.parseInt(binding.quantity.getText().toString());
            if(quantity<99){
                quantity++;
                binding.quantity.setText(String.valueOf(quantity));
                mainActivity.productCount++;
                calculatePrice();
            }
        });

        binding.minusBtn.setOnClickListener(view -> {
            mainActivity.resetHandler();
            int quantity = Integer.parseInt(binding.quantity.getText().toString());
            if (quantity > 1) {
                quantity--;
                binding.quantity.setText(String.valueOf(quantity));
                mainActivity.productCount--;
                calculatePrice();
            }
        });

    }

    public void calculatePrice() {
        int modifiersPrice = 0;
        List<CartQuestionHeader> cartQuestionHeaderList = recyclerAdapter.cartQuestionHeaderList;
        if (cartQuestionHeaderList != null) {
            for (int i = 0; i < cartQuestionHeaderList.size(); i++) {
                CartQuestionHeader cartQuestionHeader = cartQuestionHeaderList.get(i);
                for (int j = 0; j < cartQuestionHeader.getCartQuestionModifiers().size(); j++) {
                    CartQuestionModifier cartQuestionModifier = cartQuestionHeader.getCartQuestionModifiers().get(j);
                    if (cartQuestionModifier.isSelected()) {
                        double price = cartQuestionModifier.getPrice();
                        modifiersPrice += price;
                    }
                }
            }
        }

        List<CartComboHeader> cartComboHeaderList = recyclerAdapter.cartComboHeaderList;
        if (cartComboHeaderList != null) {
            for (int i = 0; i < cartComboHeaderList.size(); i++) {
                CartComboHeader cartComboHeader = cartComboHeaderList.get(i);
                for (int j = 0; j < cartComboHeader.getCartComboModifiers().size(); j++) {
                    CartComboModifier cartComboModifier = cartComboHeader.getCartComboModifiers().get(j);
                    if (cartComboModifier.getCount() > 0) {
                        double price = cartComboModifier.getPrice();
                        if (price > 0) {
                            price = price * cartComboModifier.getCount();
                            modifiersPrice += price;
                        }
                    }
                }
            }
        }

        int prodCount = Integer.parseInt(binding.quantity.getText().toString());
        double productPrice = product.getPrice();
        double totalPrice = (productPrice + modifiersPrice) * prodCount;

        binding.totalPrice.startAnimation(animation);
        binding.totalPrice.setText(FormatPrice.getFormattedPrice(totalPrice));
    }
}