package com.bimpos.newkioskapp.fragments;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bimpos.newkioskapp.BuildConfig;
import com.bimpos.newkioskapp.Helpers.Constant;
import com.bimpos.newkioskapp.databinding.ViewPagerSettingBinding;
import com.bimpos.newkioskapp.networkRequests.ConnectionSingleton;

public class SettingsFragment extends Fragment {

    private ViewPagerSettingBinding binding;
    private SharedPreferences prefs;
    private static final String TAG = "SettingsFragment";

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = ViewPagerSettingBinding.inflate(getLayoutInflater());

        prefs = requireActivity().getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE);

        binding.arabicCheckBox.setChecked(prefs.getInt(Constant.PRINTER_ARABIC, 0) != 0);
        binding.qrCodeCheckBox.setChecked(prefs.getInt(Constant.ENABLE_QR_CODE, 1) == 1);

        ConnectionSingleton.resetConnection();
        if (!prefs.getBoolean(Constant.FIRST_SETUP, true)) {
            fillContent();
        }
//        binding.stationId.setText("1");
//        binding.serverIp.setText("10.10.20.2");
//        binding.serverName.setText("posmo");
//        binding.serverPort.setText("49159");
//        binding.dbUserName.setText("dba");
//        binding.dbPassword.setText("6things");
//        binding.settingsPassword.setText("123");

        binding.arabicCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {

            if (isChecked) {
                prefs.edit().putInt(Constant.PRINTER_ARABIC, 1).apply();
            } else {
                prefs.edit().putInt(Constant.PRINTER_ARABIC, 0).apply();
            }

        });

        binding.qrCodeCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {

            if (isChecked) {
                prefs.edit().putInt(Constant.ENABLE_QR_CODE, 1).apply();
            } else {
                prefs.edit().putInt(Constant.ENABLE_QR_CODE, 0).apply();
            }

        });

        return binding.getRoot();
    }

    public boolean canSaveSettings() {
        String serverIp = binding.serverIp.getText().toString();
        String serverPort = binding.serverPort.getText().toString();
        String dbUserName = binding.dbUserName.getText().toString();
        String dbPassword = binding.dbPassword.getText().toString();
        String stationId = binding.stationId.getText().toString();
        String serverName = binding.serverName.getText().toString();
        String password = binding.settingsPassword.getText().toString();

        if (TextUtils.isEmpty(serverIp) || TextUtils.isEmpty(serverPort) || TextUtils.isEmpty(serverName)
                || TextUtils.isEmpty(dbPassword) || TextUtils.isEmpty(dbUserName) || TextUtils.isEmpty(stationId)
                || TextUtils.isEmpty(password)) {
            return false;
        } else {
            prefs.edit().putInt(Constant.STATION_ID, Integer.parseInt(stationId))
                    .putString(Constant.SERVER_IP, serverIp)
                    .putString(Constant.SERVER_PORT, serverPort)
                    .putString(Constant.DB_USER_NAME, dbUserName)
                    .putString(Constant.DB_PASSWORD, dbPassword)
                    .putString(Constant.SERVER_NAME, serverName)
                    .putString(Constant.PASSWORD, password)
                    .putBoolean(Constant.FIRST_SETUP, false)
                    .apply();

            return true;
        }
    }

    private void fillContent() {
        String password = prefs.getString(Constant.PASSWORD, "");
        String serverIp = prefs.getString(Constant.SERVER_IP, "");
        int stationId = prefs.getInt(Constant.STATION_ID, 0);
        String serverName = prefs.getString(Constant.SERVER_NAME, "");
        String serverPort = prefs.getString(Constant.SERVER_PORT, "");
        String databaseUserName = prefs.getString(Constant.DB_USER_NAME, "");
        String databasePassword = prefs.getString(Constant.DB_PASSWORD, "");
        binding.stationId.setText(String.valueOf(stationId));
        binding.serverIp.setText(serverIp);
        binding.serverName.setText(serverName);
        binding.serverPort.setText(serverPort);
        binding.dbUserName.setText(databaseUserName);
        binding.dbPassword.setText(databasePassword);
        binding.settingsPassword.setText(password);


    }
}
