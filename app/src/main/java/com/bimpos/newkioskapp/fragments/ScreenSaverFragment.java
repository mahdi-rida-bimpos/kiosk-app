package com.bimpos.newkioskapp.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bimpos.newkioskapp.Helpers.Constant;
import com.bimpos.newkioskapp.R;
import com.zhpan.bannerview.BannerViewPager;
import com.zhpan.bannerview.constants.IndicatorSlideMode;
import com.zhpan.bannerview.holder.ViewHolder;
import com.zhpan.bannerview.utils.BannerUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ScreenSaverFragment extends Fragment {

    private BannerViewPager<Integer, BannerViewHolder> bannerViewPager;
    private final OnScreenSaverClicked listener;
    private ImageView imageBanner;
    public static ArrayList<String> f = new ArrayList<>();// list of file paths
    private static final String TAG = "ScreenSaver";

    public interface OnScreenSaverClicked {

        void onScreenSaverClicked();
    }


    public ScreenSaverFragment(OnScreenSaverClicked listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_screen_saver, container, false);
        bannerViewPager = view.findViewById(R.id.fragment_screen_saver_bannerView);
        imageBanner = view.findViewById(R.id.fragment_screen_saver_image);

        getFromSdcard();
        initBannerView();
        return view;
    }

    private void initBannerView() {
        Log.d(TAG, "initBannerView: start");
        bannerViewPager.setIndicatorGap(BannerUtils.dp2px(0))
                .setRoundCorner(BannerUtils.dp2px(0))
                .setOnPageClickListener(position -> closeScreenSaver())
                .setHolderCreator(BannerViewHolder::new);

        bannerViewPager.setInterval(3000)
                .setPageStyle(com.zhpan.bannerview.constants.PageStyle.MULTI_PAGE)
                .setIndicatorSlideMode(IndicatorSlideMode.SMOOTH)
                .setIndicatorVisibility(View.GONE)
                .create(getList());

        imageBanner.setVisibility(View.GONE);
    }

    public void getFromSdcard() {
        Log.d(TAG, "getFromSdcard: start");
        String projectRoot = Environment.getExternalStorageDirectory().getPath();
        File rootFolder = new File(projectRoot, Constant.ROOT_FOLDER);
        File bannersFolder = new File(rootFolder, Constant.BANNERS_FOLDER);
        Log.d(TAG, "getFromSdcard: enter");
        File[] listFile = bannersFolder.listFiles();
        try {
//            assert listFile != null;
            if (listFile.length != 0) {
                f.clear();
                for (File file : listFile) {
                    f.add(file.getAbsolutePath());
                }
                Log.d(TAG, "getFromSdcard: files count "+f.size());
            } else {
                closeScreenSaver();
            }
        }catch (Exception e){
            closeScreenSaver();
        }

    }

    private List<Integer> getList() {
        List<Integer> list = new ArrayList<>();
        for(int i=0;i<f.size();i++){
            list.add(i);
        }
        Log.d(TAG, "getList: list size "+list.size());
        return list;
    }

    private void closeScreenSaver() {
        Log.d(TAG, "closeScreenSaver: start");
        listener.onScreenSaverClicked();
    }

    static class BannerViewHolder implements ViewHolder<Integer> {

        public BannerViewHolder() {
//            Log.d(TAG, "BannerViewHolder: start");
        }

        @Override
        public int getLayoutId() {
            return R.layout.item_slide_mode_screen_saver;
        }

        @Override
        public void onBind(View itemView, Integer data, int position, int size) {
            ImageView imageView = itemView.findViewById(R.id.banner_image_screen_saver);
            Bitmap bitmap = BitmapFactory.decodeFile(f.get(data));
            imageView.setImageBitmap(bitmap);
        }
    }
}
