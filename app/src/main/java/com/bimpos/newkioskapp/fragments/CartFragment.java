package com.bimpos.newkioskapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bimpos.newkioskapp.adapters.CartAdapter;
import com.bimpos.newkioskapp.databinding.FragmentCartBinding;
import com.bimpos.newkioskapp.activities.MainActivity;
import com.bimpos.newkioskapp.models.Cart;
import com.bimpos.newkioskapp.models.CartProduct;

import java.util.List;

public class CartFragment extends Fragment implements CartAdapter.OnEmptyCard {

    public FragmentCartBinding binding;
    private final MainActivity mainActivity;
    private Cart cartData;
    public CartAdapter adapter;


    public CartFragment(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = FragmentCartBinding.inflate(getLayoutInflater());

        initVariables();
        initRecyclerView();

        return binding.getRoot();
    }

    private void initRecyclerView() {
        List<CartProduct> cartItemsList = cartData.getCartItemsList();
        adapter = new CartAdapter(cartItemsList, getActivity(), this, mainActivity);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recyclerView.setAdapter(adapter);
    }


    private void initVariables() {
        cartData = Cart.getCartData();
    }

    @Override
    public void onEmptyCard() {
        requireActivity().onBackPressed();
    }

    @Override
    public void onPriceChanged() {
        setTotalPrice();
    }

    public void setTotalPrice() {
        mainActivity.checkTotalPrice();
    }
}
