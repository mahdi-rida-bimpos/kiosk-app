package com.bimpos.newkioskapp.activities;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bimpos.newkioskapp.BuildConfig;
import com.bimpos.newkioskapp.Helpers.Constant;
import com.bimpos.newkioskapp.Helpers.CustomDialog;
import com.bimpos.newkioskapp.database.DatabaseHelper;
import com.bimpos.newkioskapp.databinding.ActivitySplashBinding;
import com.bimpos.newkioskapp.models.Categories;
import com.bimpos.newkioskapp.models.ComboGroup;
import com.bimpos.newkioskapp.models.ComboHeader;
import com.bimpos.newkioskapp.models.ComboModifier;
import com.bimpos.newkioskapp.models.Products;
import com.bimpos.newkioskapp.models.QuestionHeader;
import com.bimpos.newkioskapp.models.QuestionModifier;
import com.bimpos.newkioskapp.networkRequests.ConnectionSingleton;
import com.bimpos.newkioskapp.networkRequests.GetAllProducts;
import com.bimpos.newkioskapp.networkRequests.GetAppSettings;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LauncherActivity extends AppCompatActivity
        implements ConnectionSingleton.OnConnectionComplete,
        GetAppSettings.OnGetAppSettingsComplete, GetAllProducts.OnGetAllProductsComplete, CustomDialog.DialogEvents {

    private final int DIALOG_CONNECTION_ERROR = 1;
    private final int DIALOG_PRODUCTS_ERROR = 2;
    private String USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private DatabaseHelper databaseHelper;
    private SharedPreferences prefs;
    private static final String TAG = "LauncherActivity";
    List<Categories> categoriesList = new ArrayList<>();
    List<Products> productsList = new ArrayList<>();
    List<ComboGroup> comboGroupList = new ArrayList<>();
    List<ComboHeader> comboHeaderList = new ArrayList<>();
    List<ComboModifier> comboModifierList = new ArrayList<>();
    List<QuestionHeader> questionHeaderList = new ArrayList<>();
    List<QuestionModifier> questionModifierList = new ArrayList<>();
    DisplayMetrics metrics;
    private CustomDialog dialog;
    private ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySplashBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        dialog = new CustomDialog(this);
        databaseHelper = new DatabaseHelper(this);
        Log.d(TAG, "onCreate: start");
        metrics = getResources().getDisplayMetrics();
        adjustFontScale();
        checkPermissions();
        prefs = getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE);

    }


    private void checkPermissions() {
        Log.d(TAG, "checkPermissions: start");
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                USB_PERMISSION);

        int permissionCheck1 = ContextCompat.checkSelfPermission(this,
                WRITE_EXTERNAL_STORAGE);


        if (permissionCheck != PackageManager.PERMISSION_GRANTED
                || permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "checkPermissions: ask");

            ActivityCompat.requestPermissions(this,
                    new String[]{WRITE_EXTERNAL_STORAGE, USB_PERMISSION},
                    1);
            return;
        }
        launchApp();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequestPermissionsResult: start");
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            Log.d(TAG, "onRequestPermissionsResult: granted");
            launchApp();
        }
    }

    private void launchApp() {
        Log.d(TAG, "launchApp: start");
        createRootFolder();
//        insertDatabase();
        if (prefs.getBoolean(Constant.FIRST_SETUP, true)) {
            gotoSettingsActivity();
        } else {
            connectToDb();
//            insertDatabase();
        }
    }

    private void createRootFolder() {

        String projectRoot = Environment.getExternalStorageDirectory().getPath();
        File rootFolders = new File(projectRoot, Constant.ROOT_FOLDER);
        if (rootFolders.exists()) {
            Log.d(TAG, "createRootFolder: root folder exists");
        } else {
            Log.d(TAG, "createRootFolder: root folder not found");
            if (rootFolders.mkdirs()) {
                Log.d(TAG, "createRootFolder: root folder created");
            } else {
                Log.d(TAG, "createRootFolder: root folder not created");
            }
        }

        File pictureFolder = new File(rootFolders, Constant.BANNERS_FOLDER);
        if (!pictureFolder.exists()) {
            Log.d(TAG, "onActivityResult:Pictures folder not exists");
            if (pictureFolder.mkdirs()) {
                Log.d(TAG, "onActivityResult: Pictures created");
            } else {
                Log.d(TAG, "onActivityResult: Pictures not created");
            }
        }

        File ResFolder = new File(rootFolders, Constant.RES_FOLDER);
        if (!ResFolder.exists()) {
            Log.d(TAG, "onActivityResult:ResFolder folder not exists");
            if (ResFolder.mkdirs()) {
                Log.d(TAG, "onActivityResult: ResFolder created");
            } else {
                Log.d(TAG, "onActivityResult: ResFolder not created");
            }
        }
    }

    private void gotoSettingsActivity() {
        startActivity(new Intent(this, SettingsActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

//    private void insertDatabase() {
//       AppSettings.ORDER_TYPE_MAP.put(9,"Take Away");
//        AppSettings.ORDER_TYPE_MAP.put(10,"Dine In");
//        AppSettings.TAKE_AWAY=9;
//        AppSettings.DINE_IN=10;
//
//        for (int i = 1; i < 15; i++) {
//            Categories categories = new Categories(i, "Category " + i, "", 0, i);
//            categoriesList.add(categories);
//
//            for (int ii = 1; ii < i + 5; ii++) {
//                Products products = new Products(ii, i, 0, 0, "Product " + ii, "", "", 1000.0000 * ii, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
//                if (i == 1 && ii == 1) {
//                    products.setIsCombo(1);
//                    Log.d(TAG, "insertDatabase: set combo");
////                    products.setQuestionGroupId(0);
//                }
//                if (i == 1 && ii == 2) {
//                    products.setQuestionGroupId(10);
//                }
//                productsList.add(products);
//
//            }
//        }
//
//        createQuestions();
//        createCombos();
//
//        databaseHelper.cleanAllTables();
//
//        databaseHelper.insertCategoryList(categoriesList);
//
//        databaseHelper.insertProductList(productsList);
//
//        databaseHelper.insertQuestionHeaderList(questionHeaderList);
//        databaseHelper.insertQuestionModifierList(questionModifierList);
//
//        databaseHelper.insertComboGroupList(comboGroupList);
//        databaseHelper.insertComboHeaderList(comboHeaderList);
//        databaseHelper.insertComboModifierList(comboModifierList);
//
//        gotoMainActivity();
//
//    }

    public void adjustFontScale() {
        Configuration configuration = getResources().getConfiguration();

        double font = 1.0;
        if (configuration.fontScale != font) {
            configuration.fontScale = (float) font;
        }
        double densityDpi = 160;
        if (configuration.densityDpi != densityDpi) {
            configuration.densityDpi = (int) densityDpi;
        }
        double scaledDensity = 1.0;
        if (metrics.scaledDensity != scaledDensity) {
            metrics.scaledDensity = (float) scaledDensity;
        }

        double density = 1.0;
        if (metrics.density != density) {
            metrics.density = (int) density;
        }


        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        metrics.densityDpi = 0;
        getResources().updateConfiguration(configuration, metrics);

        int padding = (int) (metrics.widthPixels * 0.1);
        binding.bimLogo.setPadding(padding, 0, padding, 0);


    }

    @Override
    public void onBackPressed() {

    }

    private void createCombos() {
        ComboGroup comboGroup = new ComboGroup(10, 1);
        comboGroupList.add(comboGroup);

        ComboHeader comboHeader = new ComboHeader(10, 100, 0, 3, 1, "Header 1");
        ComboHeader comboHeader1 = new ComboHeader(10, 101, 0, 2, 1, "Header 2");
        comboHeaderList.add(comboHeader);
        comboHeaderList.add(comboHeader1);

        ComboModifier comboModifier1 = new ComboModifier(200, 10, 100, 1000, 0, 0, 0, 1000, 0, 0, 1, "Combo 1");
        ComboModifier comboModifier2 = new ComboModifier(201, 10, 100, 0, 0, 0, 0, 0, 0, 0, 2, "Combo 2");
        ComboModifier comboModifier3 = new ComboModifier(202, 10, 100, 0, 0, 0, 0, 0, 0, 0, 3, "Combo 3");
        ComboModifier comboModifier4 = new ComboModifier(203, 10, 100, 4000, 0, 0, 0, 4000, 0, 0, 4, "Combo 4");

        ComboModifier comboModifier11 = new ComboModifier(204, 10, 101, 1000, 0, 0, 0, 1000, 0, 0, 1, "Combo 11");
        ComboModifier comboModifier21 = new ComboModifier(205, 10, 101, 2000, 0, 0, 0, 2000, 0, 0, 2, "Combo 21");
        ComboModifier comboModifier31 = new ComboModifier(206, 10, 101, 3000, 0, 0, 0, 3000, 0, 0, 3, "Combo 31");
        ComboModifier comboModifier41 = new ComboModifier(207, 10, 101, 4000, 0, 0, 0, 4000, 0, 0, 4, "Combo 41");

        comboModifierList.add(comboModifier1);
        comboModifierList.add(comboModifier2);
        comboModifierList.add(comboModifier3);
        comboModifierList.add(comboModifier4);
        comboModifierList.add(comboModifier11);
        comboModifierList.add(comboModifier21);
        comboModifierList.add(comboModifier31);
        comboModifierList.add(comboModifier41);
    }

    private void createQuestions() {
        QuestionHeader questionHeader = new QuestionHeader(10, 100, 0, 3, 1, 1, "Header 1");
        questionHeaderList.add(questionHeader);

        QuestionHeader questionHeader1 = new QuestionHeader(10, 101, 1, 1, 1, 1, "Header 2");
        questionHeaderList.add(questionHeader1);

        QuestionModifier questionModifier1 = new QuestionModifier(201, 10, 100, 2000, 0, 0, 0, 2000, 0, 0, "Modifier 1");
        QuestionModifier questionModifier2 = new QuestionModifier(202, 10, 100, 3000, 0, 0, 0, 3000, 0, 0, "Modifier 2");
        QuestionModifier questionModifier3 = new QuestionModifier(203, 10, 100, 4000, 0, 0, 0, 4000, 0, 0, "Modifier 3");
        QuestionModifier questionModifier4 = new QuestionModifier(204, 10, 100, 5000, 0, 0, 0, 5000, 0, 0, "Modifier 4");
        questionModifierList.add(questionModifier1);
        questionModifierList.add(questionModifier2);
        questionModifierList.add(questionModifier3);
        questionModifierList.add(questionModifier4);

        QuestionModifier questionModifier11 = new QuestionModifier(205, 10, 101, 2000, 0, 0, 0, 2000, 0, 0, "Modifier 11");
        QuestionModifier questionModifier21 = new QuestionModifier(206, 10, 101, 3000, 0, 0, 0, 3000, 0, 0, "Modifier 21");
        QuestionModifier questionModifier31 = new QuestionModifier(207, 10, 101, 4000, 0, 0, 0, 4000, 0, 0, "Modifier 31");
        QuestionModifier questionModifier41 = new QuestionModifier(208, 10, 101, 5000, 0, 0, 0, 5000, 0, 0, "Modifier 41");
        questionModifierList.add(questionModifier11);
        questionModifierList.add(questionModifier21);
        questionModifierList.add(questionModifier31);
        questionModifierList.add(questionModifier41);
    }

    private void connectToDb() {
        ConnectionSingleton.resetConnection();
        ConnectionSingleton.initConnection(this, prefs);
    }

    @Override
    public void onConnectionComplete(int status) {
        Log.d(TAG, "onConnectionComplete: start status " + status);
        if (status == 1) {
            getAllSettings();
        } else {
            new Handler(Looper.getMainLooper()).post(() ->
                    {
                        showDialog("Note", "Unable to connect to database, please contact Bimpos support team.", "retry", DIALOG_CONNECTION_ERROR);
                    }
            );
        }
    }

    private void getAllSettings() {
        GetAppSettings getAppSettings = new GetAppSettings(this);
        getAppSettings.execute();
    }

    @Override
    public void onGetAppSettingsComplete(int status) {
        Log.d(TAG, "onGetAppSettingsComplete: " + status);
        if (status == 1) {
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    insertDatabase();
//                }
//            }, 2000);
            getAllProducts();
        } else {
            int DIALOG_SETTINGS_ERROR = 3;
            showDialog("Warning", "Unable to fetch app settings, please contact BIM POS support team", "Retry", DIALOG_SETTINGS_ERROR);
        }
    }

    private void getAllProducts() {
        GetAllProducts getCategories = new GetAllProducts(this, this);
        getCategories.execute();
    }

    @Override
    public void onGetAllProductsComplete(int status) {
        Log.d(TAG, "onGetAllProductsComplete: " + status);
        if (status == 1) {
            gotoMainActivity();
        } else if (status == -2) {
            showProductsDialog("Warning", "You have no product", "Retry", DIALOG_PRODUCTS_ERROR);
        } else {
            showDialog("Warning", "Unable to fetch data, please contact Bimpos support team.", "Retry", DIALOG_PRODUCTS_ERROR);
        }
    }


    private void gotoMainActivity() {
        Log.d(TAG, "gotoMainActivity: start");
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
//        finish();
    }

    private void showDialog(String title, String message, String positiveBtn, int dialogId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton(positiveBtn, (dialogInterface, i) -> connectToDb());
        builder.setNegativeButton("Settings", (dialog, which) -> gotoSettingsActivity());
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void showProductsDialog(String title, String message, String positiveBtn, int dialogId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton(positiveBtn, (dialogInterface, i) -> {
            dialogInterface.dismiss();
            getAllProducts();
        });
        builder.setNegativeButton("Settings", (dialog, which) -> gotoSettingsActivity());
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        dialog.dismiss();
        if (dialogId == DIALOG_CONNECTION_ERROR) {
            finish();
        } else if (dialogId == DIALOG_PRODUCTS_ERROR) {
            getAllProducts();
        } else if (dialogId == DIALOG_CONNECTION_ERROR) {
            connectToDb();
        }
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        if (dialogId == DIALOG_CONNECTION_ERROR) {
            dialog.dismiss();
            gotoSettingsActivity();
        }
    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }
}