package com.bimpos.newkioskapp.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.newkioskapp.BuildConfig;
import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.Helpers.Constant;
import com.bimpos.newkioskapp.Helpers.CustomDialog;
import com.bimpos.newkioskapp.Helpers.CustomLoading;
import com.bimpos.newkioskapp.Helpers.FormatPrice;
import com.bimpos.newkioskapp.Helpers.GetBitmap;
import com.bimpos.newkioskapp.R;
import com.bimpos.newkioskapp.adapters.CategoriesRvAdapter;
import com.bimpos.newkioskapp.adapters.ProductsRvAdapter;
import com.bimpos.newkioskapp.database.DatabaseHelper;
import com.bimpos.newkioskapp.databinding.ActivityMainBinding;
import com.bimpos.newkioskapp.fragments.BigCategoriesFragment;
import com.bimpos.newkioskapp.fragments.CartFragment;
import com.bimpos.newkioskapp.fragments.OrderTypeFragment;
import com.bimpos.newkioskapp.fragments.PrintReceiptFragment;
import com.bimpos.newkioskapp.fragments.ProductInfoFragment;
import com.bimpos.newkioskapp.fragments.ScreenSaverFragment;
import com.bimpos.newkioskapp.models.Cart;
import com.bimpos.newkioskapp.models.CartComboHeader;
import com.bimpos.newkioskapp.models.CartComboModifier;
import com.bimpos.newkioskapp.models.CartQuestionHeader;
import com.bimpos.newkioskapp.models.CartQuestionModifier;
import com.bimpos.newkioskapp.models.Categories;
import com.bimpos.newkioskapp.models.Products;
import com.bimpos.newkioskapp.networkRequests.ConnectionSingleton;
import com.bimpos.newkioskapp.networkRequests.GetAllProducts;
import com.bimpos.newkioskapp.networkRequests.GetAppSettings;
import com.bimpos.newkioskapp.networkRequests.GetUpdateLookup;
import com.bimpos.newkioskapp.networkRequests.PlaceOrder;
import com.bimpos.newkioskapp.printing.PrinterClass;
import com.bimpos.newkioskapp.receivers.NetworkChangeReceiver;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.caysn.autoreplyprint.AutoReplyPrint;
import com.dk.animation.circle.CircleAnimationUtil;
import com.hardik.clickshrinkeffect.ClickShrinkEffect;
import com.sun.jna.Pointer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements ScreenSaverFragment.OnScreenSaverClicked,
        OrderTypeFragment.OnOrderTypeClicked,
        CustomDialog.DialogEvents,
        GetAppSettings.OnGetAppSettingsComplete,
        GetAllProducts.OnGetAllProductsComplete, PlaceOrder.OnInsertComplete, GetUpdateLookup.OnGetUpdateLookup, NetworkChangeReceiver.ConnectivityReceiverListener, ConnectionSingleton.OnConnectionComplete {


    private static final String TAG = "MainActivity";
    private static final int DIALOG_ORDER_CONFIRMATION = 2;
    private static final int DIALOG_ORDER_CANCEL = 3;
    private static final int DIALOG_PRINTER_ERROR = 5;
    private final int DIALOG_MISSING_MODIFIER = 1;
    private ActivityMainBinding binding;
    private Cart cartData;
    private int currentSelectedCategory = 0;
    private boolean isUpdating = false;
    private boolean canLookup = true;
    public boolean canClickCategory = true;
    public CustomLoading loading;
    private long mLastClick = 0;
    public CustomDialog dialog;
    private Categories selectedCategory;
    public int productCount = 1;
    public boolean isProductFragmentOpened = false;
    public boolean isKeyboardOpened = false;
    private InputMethodManager imm;
    private CategoriesRvAdapter categoriesAdapter;
    private ProductsRvAdapter productsAdapter;
    private List<Categories> categoriesList = new ArrayList<>();
    private DatabaseHelper databaseHelper;
    private ProductInfoFragment productInfoFragment;
    private BigCategoriesFragment bigCategoriesFragment;
    private ScreenSaverFragment screenSaverFragment;
    public PrintReceiptFragment printReceiptFragment;
    public CartFragment cartFragment;
    private OrderTypeFragment orderTypeFragment;
    public boolean isProductOpened = false;
    private Handler mHandler;
    private int flags;
    private boolean screenSaverMode = false;
    public View view;
    private GetUpdateLookup updateLookup;
    private NetworkChangeReceiver networkChangeReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Log.d(TAG, "onCreate: start");

        openPort();
        initVariables();
        updateCart();
        checkColors();
        initTimer();
        openScreenSaver();
        initCatRecyclerView();
        initProductRecyclerView();
        initClickListener();
        getRestaurantImage();
        initLookupsTimer();
        startLockTask();

//        binding.restaurantPicture.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                List<Products> productList = databaseHelper.getProductsByCatId(categoriesList.get(0).getCatId());
//                Products products = productList.get(0);
//                double price = products.getPrice();
//                price = price + 1000;
//                productList.get(0).setPrice(price);
//                productsAdapter.setData(productList);
//            }
//        });

    }

    public void getRestaurantImage() {

        if (AppSettings.RESTAURANT_IMAGE == null) {
            binding.restaurantPicture.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.bimposlogo));
        } else {
            Glide.with(this)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .load(GetBitmap.getImageBytes(AppSettings.RESTAURANT_IMAGE))
                    .placeholder(R.drawable.bimposlogo)
                    .into(binding.restaurantPicture);
        }
    }

    private void openPort() {
        Pointer h = AutoReplyPrint.INSTANCE.CP_Port_OpenUsb("VID:0x4B43,PID:0x3830", 1);
        AutoReplyPrint.INSTANCE.CP_Port_Close(h);
    }

    private void checkColors() {
        binding.cancelButton.setBackgroundColor(AppSettings.BOTTOM_NO_ENABLED_BACK_COLOR);
        binding.cartButton.setBackgroundColor(AppSettings.BOTTOM_YES_ENABLED_BACK_COLOR);
    }

    public void removeHandler() {
        mHandler.removeCallbacks(mRunnable);
    }

    public void resumeHandler() {
        resetHandler();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "onResume: start");

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(networkChangeReceiver, filter);

        IntentFilter intentFilter = new IntentFilter(Constant.RECEIVER_PRINTER);
        registerReceiver(printerReceiver, intentFilter);

        categoriesAdapter.changeSelectedCategory(0);
        changeCatPosition(categoriesList.get(0), 0);
        hideNavigation();

        getWindow().getDecorView().setSystemUiVisibility(flags);
        getWindow().getDecorView()
                .setOnSystemUiVisibilityChangeListener(visibility -> {
                    Log.d(TAG, "onSystemUiVisibilityChange: start");
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                        getWindow().getDecorView().setSystemUiVisibility(flags);
                    }
                });
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(printerReceiver);
        unregisterReceiver(networkChangeReceiver);
    }

    public void hideNavigation() {
        try {
            getWindow().getDecorView().setSystemUiVisibility(flags);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initVariables() {
        networkChangeReceiver = new NetworkChangeReceiver(this);
        updateLookup = new GetUpdateLookup(this);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        loading = new CustomLoading(this);
        dialog = new CustomDialog(this);
        cartData = Cart.getCartData();
        databaseHelper = new DatabaseHelper(this);
        new ClickShrinkEffect(binding.cancelButton);
        new ClickShrinkEffect(binding.cartButton);
        screenSaverFragment = new ScreenSaverFragment(this);
        orderTypeFragment = new OrderTypeFragment(this, this, getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE));
        productInfoFragment = new ProductInfoFragment(this);
        cartFragment = new CartFragment(this);
        bigCategoriesFragment = new BigCategoriesFragment(this);
        printReceiptFragment = new PrintReceiptFragment(this);
        flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS |
                WindowManager.LayoutParams.SOFT_INPUT_MASK_ADJUST |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;


    }

    private void initClickListener() {
        binding.cancelButton.setOnClickListener(view -> {
            if (binding.cancelButton.getTag().toString().equalsIgnoreCase("cancel")) {
                if (cartData.getCartCount() == 0) {
                    openBigCategoryFragment();
                } else {
                    showCancelCartDialog();
                }
            } else {
                onBackPressed();
            }
        });

        binding.cartButton.setOnClickListener(view -> {
            if (SystemClock.elapsedRealtime() - mLastClick < 200) {
                return;
            }
            mLastClick = SystemClock.elapsedRealtime();
            resetHandler();

            if (binding.cartButton.getTag().toString().equalsIgnoreCase("cart")) {
                openCartFragment();
            } else if (binding.cartButton.getTag().toString().equalsIgnoreCase("add")) {
                addProduct();
            } else if (binding.cartButton.getTag().toString().equalsIgnoreCase("order")) {
                showConfirmationDialog();
            }
        });
    }

    private void initTimer() {
        mHandler = new Handler();
        mHandler.postDelayed(mRunnable, Constant.TIMEOUT);
    }

    private void initCatRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        if (Constant.CAT_ORIENTATION.equalsIgnoreCase("h")) {
            layoutManager.setOrientation(RecyclerView.VERTICAL);
            binding.recyclerLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        } else {
            layoutManager.setOrientation(RecyclerView.HORIZONTAL);
            binding.recyclerLinearLayout.setOrientation(LinearLayout.VERTICAL);
        }

        binding.categoryRecyclerView.setLayoutManager(layoutManager);
        categoriesList = databaseHelper.getAllCategories();
        categoriesAdapter = new CategoriesRvAdapter(this, this, categoriesList);
        binding.categoryRecyclerView.setAdapter(categoriesAdapter);
        binding.categoryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                resetHandler();
                super.onScrolled(recyclerView, dx, dy);
            }
        });

    }

    private void initProductRecyclerView() {
        productsAdapter = new ProductsRvAdapter(this, new ArrayList<>());
        binding.productsRecyclerView.setLayoutManager(new GridLayoutManager(this, AppSettings.PRODUCT_SPAN_COUNT, RecyclerView.VERTICAL, false));
        binding.productsRecyclerView.setAdapter(productsAdapter);
        binding.productsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                resetHandler();
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void showCancelCartDialog() {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "Are you sure you want to cancel the order ?");
        arguments.putString(CustomDialog.DIALOG_TITLE, "Cancel Order");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "yes");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "no");
        arguments.putInt(CustomDialog.DIALOG_ID, DIALOG_ORDER_CANCEL);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    private void showConfirmationDialog() {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, "Are you sure you want to place order ?");
        arguments.putString(CustomDialog.DIALOG_TITLE, "Confirm Order");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "yes");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "no");
        arguments.putInt(CustomDialog.DIALOG_ID, DIALOG_ORDER_CONFIRMATION);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    private void showErrorPrintingDialog(String message) {
        if (loading.isShowing()) {
            loading.dismiss();
        }
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, message);
        arguments.putString(CustomDialog.DIALOG_TITLE, "Printer Issue");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "Retry");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "Exit");
        arguments.putInt(CustomDialog.DIALOG_ID, DIALOG_PRINTER_ERROR);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    private void addProduct() {

        resetHandler();
        List<CartQuestionModifier> newCartQuestionModifierList;
        List<CartQuestionHeader> newCartQuestionHeaderList = new ArrayList<>();
        List<CartComboModifier> newCartComboModifierList;
        List<CartComboHeader> newCartComboHeaderList = new ArrayList<>();

        String remark = "";

        if (productInfoFragment.binding.remarkText.getText().toString().length() != 0) {
            remark = productInfoFragment.binding.remarkText.getText().toString();
        }
        //check required things
        if (productInfoFragment.product.getQuestionGroupId() > 0) {

            for (int i = 0; i < productInfoFragment.recyclerAdapter.cartQuestionHeaderList.size(); i++) {
                CartQuestionHeader cartQuestionHeader = productInfoFragment.recyclerAdapter.cartQuestionHeaderList.get(i);

                if (cartQuestionHeader.getRequired() == 1 && cartQuestionHeader.getCount() == 0) {
                    showAlertDialog();
                    return;
                }
                if (cartQuestionHeader.getCount() > 0) {
                    newCartQuestionModifierList = new ArrayList<>();
                    for (int j = 0; j < cartQuestionHeader.getCartQuestionModifiers().size(); j++) {
                        CartQuestionModifier modifier = cartQuestionHeader.getCartQuestionModifiers().get(j);
                        if (modifier.isSelected()) {
                            newCartQuestionModifierList.add(modifier);
                        }
                    }
                    newCartQuestionHeaderList.add(cartQuestionHeader);
                    newCartQuestionHeaderList.get(newCartQuestionHeaderList.indexOf(cartQuestionHeader)).setCartQuestionModifiers(newCartQuestionModifierList);
                }
            }
        }

        if (productInfoFragment.product.getIsCombo() == 1) {

            for (int i = 0; i < productInfoFragment.recyclerAdapter.cartComboHeaderList.size(); i++) {
                CartComboHeader cartComboHeader = productInfoFragment.recyclerAdapter.cartComboHeaderList.get(i);

                if (cartComboHeader.getRequired() == 1 && cartComboHeader.getSelectedItemCount() == 0) {
                    showAlertDialog();
                    return;
                }
                if (cartComboHeader.getSelectedItemCount() > 0) {
                    newCartComboModifierList = new ArrayList<>();
                    for (int j = 0; j < cartComboHeader.getCartComboModifiers().size(); j++) {
                        CartComboModifier modifier = cartComboHeader.getCartComboModifiers().get(j);
                        if (modifier.getCount() > 0) {
                            newCartComboModifierList.add(modifier);
                        }
                    }
                    newCartComboHeaderList.add(cartComboHeader);
                    newCartComboHeaderList.get(newCartComboHeaderList.indexOf(cartComboHeader)).setCartComboModifiers(newCartComboModifierList);
                }
            }
        }
        cartData.addProductWithModifiers(productInfoFragment.product, remark, newCartQuestionHeaderList, newCartComboHeaderList, Integer.parseInt(productInfoFragment.binding.quantity.getText().toString()));
        onBackPressed();
        startAnimation();
    }

    private void showAlertDialog() {
        StringBuilder message = new StringBuilder();
        message.append("You must choose: ").append("\n");
        if (productInfoFragment.recyclerAdapter.cartQuestionHeaderList.size() != 0) {
            for (int i = 0; i < productInfoFragment.recyclerAdapter.cartQuestionHeaderList.size(); i++) {
                CartQuestionHeader cartQuestionHeader = productInfoFragment.recyclerAdapter.cartQuestionHeaderList.get(i);
                if (cartQuestionHeader.getRequired() == 1 && cartQuestionHeader.getCount() == 0) {
                    message.append("\n- ").append(cartQuestionHeader.getDescription());
                }
            }
        }
        if (productInfoFragment.recyclerAdapter.cartComboHeaderList.size() != 0) {
            for (int i = 0; i < productInfoFragment.recyclerAdapter.cartComboHeaderList.size(); i++) {
                CartComboHeader cartComboHeader = productInfoFragment.recyclerAdapter.cartComboHeaderList.get(i);
                if (cartComboHeader.getRequired() == 1 && cartComboHeader.getSelectedItemCount() == 0) {
                    message.append("\n- ").append(cartComboHeader.getDescription());
                }
            }
        }
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, message.toString());
        arguments.putString(CustomDialog.DIALOG_TITLE, "Note");
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "ok");
        arguments.putString(CustomDialog.DIALOG_NEGATIVE_BUTTON, "null");
        arguments.putInt(CustomDialog.DIALOG_ID, DIALOG_MISSING_MODIFIER);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(false);
    }

    public void hideKeyboard() {
        if (isProductFragmentOpened) {
            productInfoFragment.binding.remarkText.clearFocus();
            imm.hideSoftInputFromWindow(productInfoFragment.binding.remarkText.getWindowToken(), 0);
        }
        isKeyboardOpened = false;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void changeCatPosition(Categories categories, int position) {
        Log.d(TAG, "changeCatPosition: start");
        resetHandler();
        selectedCategory = categories;
        currentSelectedCategory = position;
        Log.d(TAG, "changeCatPosition: selected " + currentSelectedCategory);
        binding.categoryRecyclerView.scrollToPosition(position);
        binding.productsRecyclerView.scrollToPosition(0);
//        categoriesAdapter.notifyDataSetChanged();
        List<Products> productsList = databaseHelper.getProductsByCatId(categories.getCatId());
        productsAdapter.setData(productsList);
        binding.categoryTitle.setText(categories.getDescription());
    }

    private void disableCartButton() {
        binding.cartButton.setEnabled(false);
        binding.cartButton.setBackgroundColor(AppSettings.BOTTOM_YES_DISABLED_BACK_COLOR);
    }

    public void enableCartButton() {
        binding.cartButton.setEnabled(true);
        binding.cartButton.setBackgroundColor(AppSettings.BOTTOM_YES_ENABLED_BACK_COLOR);
    }

    @SuppressLint("SetTextI18n")
    private void updateButtons(String backTag, String cartText, String cartTag) {
//        Log.d(TAG, "updateButtons: back text : " + "back" + ", back tag: " + backTag);
        if (!screenSaverMode) {
//            Log.d(TAG, "updateButtons: change text ");
            binding.cancelButtonText.setText("back");
            binding.cartText.setText(cartText);
        }
        binding.cancelButton.setTag(backTag);
        binding.cartButton.setTag(cartTag);

    }

    @SuppressLint("NotifyDataSetChanged")
    private void updateCart() {
        Log.d(TAG, "updateCart: start");
        int cartCount = cartData.getCartCount();
        int price = cartData.getCartTotalPrice();
        if (cartCount > 0) {
            String cartText = "Total Items: " + cartCount + "\n" + FormatPrice.getFormattedPrice(price);
            updateButtons("cancel", cartText, "cart");
            binding.cartButton.setVisibility(View.VISIBLE);
            binding.cartImage.animate().alpha(1).setDuration(200).start();
        } else if (cartCount == 0) {
            binding.cartButton.setVisibility(View.GONE);
            updateButtons("cancel", "cart (0)", "home");
            binding.cartImage.animate().alpha(0).setDuration(200).start();
        }

    }

    @Override
    public void onBackPressed() {
        resetHandler();
        isProductOpened = false;
        updateCart();

        if (getSupportFragmentManager().findFragmentByTag("Item") != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
                    .remove(Objects.requireNonNull(getSupportFragmentManager().findFragmentByTag("Item")))
                    .commit();
            binding.categoryTitle.setText(selectedCategory.getDescription());
        }
        if (getSupportFragmentManager().findFragmentByTag("Cart") != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
                    .remove(Objects.requireNonNull(getSupportFragmentManager().findFragmentByTag("Cart")))
                    .commit();
            binding.categoryTotalPrice.animate().alpha(0).setDuration(200).start();
            binding.categoryTitle.animate().alpha(1).setStartDelay(300).setDuration(200).start();
        }
        enableCartButton();
    }

    public void openScreenSaver() {
        hideKeyboard();
        mHandler.removeCallbacks(mRunnable);
        cartData.clear();
        screenSaverMode = true;
        canLookup = true;
        updateButtons("cancel", "cart (0)", "");


        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.fullScreenFragment, screenSaverFragment, "ScreenSaver")
                .commit();
        binding.mainLayout.animate().alpha(0).setDuration(500).start();
        binding.fullScreenFragment.animate().alpha(1).setDuration(500).start();

        onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
        Log.d(TAG, "finish: start");
        stopLockTask();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLockTask();
    }

    @Override
    public void onScreenSaverClicked() {
        closeScreenSaver();
    }

    private void closeScreenSaver() {
        mHandler.postDelayed(mRunnable, Constant.TIMEOUT);
        screenSaverMode = false;
        openOrderTypeFragment();
    }

    private void openOrderTypeFragment() {
        Fragment productFragment = getSupportFragmentManager().findFragmentByTag("Item");
        if (productFragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .remove(Objects.requireNonNull(getSupportFragmentManager().findFragmentByTag("Item")))
                    .commit();
        }

        Fragment CartFragment = getSupportFragmentManager().findFragmentByTag("Cart");
        if (CartFragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .remove(Objects.requireNonNull(getSupportFragmentManager().findFragmentByTag("Cart")))
                    .commit();
        }

        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(R.id.fullScreenFragment, orderTypeFragment, "OrderType")
                .commit();
    }

    @Override
    public void onOrderTypeClicked() {
        orderTypeFragment.canClick = true;
        closeOrderType();
    }

    private void closeOrderType() {
        updateCart();
        openBigCategoryFragment();
    }

    private void openBigCategoryFragment() {
        Log.d(TAG, "openBigCategoryFragment: start");
        cartData.clear();
        updateButtons("cancel", "cart (0)", "");
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.fullScreenFragment, bigCategoriesFragment, "BigCategoryFragment")
                .commit();
        onBackPressed();
        binding.mainLayout.animate().alpha(0).setDuration(500).start();
        binding.fullScreenFragment.animate().alpha(1).setDuration(500).start();
    }

    public void closeBigCategoryFragment(Categories categories, int position) {
        Log.d(TAG, "closeBigCategoryFragment: start");
        categoriesAdapter.changeSelectedCategory(position);
//        selectedCategory=categories;
        currentSelectedCategory = position;
        changeCatPosition(categories, position);
        binding.mainLayout.animate().alpha(1).setDuration(500).start();
        binding.fullScreenFragment.animate().alpha(0).setDuration(500).start();
        getSupportFragmentManager()
                .beginTransaction()
                .remove(Objects.requireNonNull(getSupportFragmentManager().findFragmentByTag("BigCategoryFragment")))
                .commit();
        canClickCategory = true;
    }

    @SuppressLint("SetTextI18n")
    public void openProductFragment(Products product) {
        resetHandler();
        binding.cartButton.setVisibility(View.VISIBLE);
        if (product.getIsCombo() == 1 || product.getQuestionGroupId() > 0) {
            disableCartButton();
        }

        Bundle arguments = new Bundle();
        arguments.putSerializable(Products.class.getSimpleName(), product);
        productInfoFragment = new ProductInfoFragment(this);
        productInfoFragment.setArguments(arguments);

        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                .add(R.id.contentFragmentLayout, productInfoFragment, "Item")
                .commit();
        binding.cartImage.animate().alpha(0).setDuration(200).start();
        binding.categoryTitle.setText("Add Product");
        updateButtons("back", "Add to Cart", "add");

    }

    private void openCartFragment() {
        checkTotalPrice();
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                .add(R.id.contentFragmentLayout, cartFragment, "Cart")
                .commit();


        binding.cartImage.animate().alpha(0).setDuration(200).start();
        updateButtons("back", "Place Order", "order");
    }

    public void checkTotalPrice() {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        SpannableString totalPriceText = new SpannableString("Total Price:");
        totalPriceText.setSpan(new ForegroundColorSpan(Color.BLACK), 0, totalPriceText.length(), 0);
        builder.append(totalPriceText).append("\n");

        SpannableString priceText = new SpannableString(FormatPrice.getFormattedPrice(cartData.getCartTotalPrice()));
        priceText.setSpan(new ForegroundColorSpan(AppSettings.PRICE_TEXT_COLOR), 0, priceText.length(), 0);
        builder.append(priceText);

        binding.categoryTotalPrice.setText(builder);
        binding.categoryTitle.animate().alpha(0).setDuration(200).start();
        binding.categoryTotalPrice.animate().setStartDelay(300).alpha(1).setDuration(200).start();
    }

    private final Runnable mRunnable = this::openScreenSaver;

    public void resetHandler() {
        mHandler.removeCallbacks(mRunnable);
        mHandler.postDelayed(mRunnable, Constant.TIMEOUT);
    }

    private void startAnimation() {
        String text = "+ " + productCount;
        binding.productAnimationCount.setText(text);
        new CircleAnimationUtil()
                .attachActivity(MainActivity.this)
                .setTargetView(binding.productAnimationLayout)
                .setMoveDuration(500)
                .setBorderColor(Color.TRANSPARENT)
                .setCircleDuration(500)
                .setDestView(binding.cartImage)
                .startAnimation();
        productCount = 1;
    }

    private void placeOrder() {
        resetHandler();
        canLookup = false;
        loading.show();
        loading.startAnimation(R.raw.order_loading_pan);
        loading.setTitle("Placing Order");
        loading.setMessage("Please wait...");
        new CountDownTimer(2000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                loading.startAnimation(R.raw.printer_animation);
                insertOrderToDb();
            }
        }.start();

    }

    private final BroadcastReceiver printerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive: received ");
            resetHandler();
            int status = intent.getIntExtra("status", 0);
            String message = intent.getStringExtra("message");
            if (status == 0) {
                showErrorPrintingDialog(message);
            } else {
                loading.setMessage("Please take your receipt and proceed to the cashier to pay.\n\nThank you!");
                binding.mainLayout.animate().alpha(0).setDuration(500).start();
                loading.setTitle("Notifying cashier...");
                loading.startAnimation(R.raw.sync_animation);
                GetAppSettings getAppSettings = new GetAppSettings(MainActivity.this);
                getAppSettings.execute();
            }
        }
    };

    private void insertOrderToDb() {
//        loading.dismiss();
//        openPrintFragment();
        PlaceOrder placeOrder = new PlaceOrder(getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE), this);
        placeOrder.execute();
    }

    @Override
    public void onInsertComplete(int transactId) {
        new Thread(() -> {
            PrinterClass testFunction = new PrinterClass();
            testFunction.printReceipt(MainActivity.this, transactId);
        }).start();
    }

    private void openPrintFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.fullScreenFragment, printReceiptFragment, "Print")
                .commit();
        onBackPressed();
        binding.mainLayout.animate().alpha(0).setDuration(500).start();
        binding.fullScreenFragment.animate().alpha(1).setDuration(500).start();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown: start " + event);
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        switch (dialogId) {
            case DIALOG_MISSING_MODIFIER:
                dialog.dismiss();
                hideNavigation();
                break;

            case DIALOG_ORDER_CONFIRMATION:
                dialog.dismiss();
                new Handler().postDelayed(this::placeOrder, 500);
                break;

            case DIALOG_ORDER_CANCEL:
                dialog.dismiss();
                cartData.clear();
                updateCart();
                break;

            case DIALOG_PRINTER_ERROR:
                dialog.dismiss();
                placeOrder();
        }
    }

    @Override
    public void onNegativeDialogResult(int dialogId, Bundle args) {
        switch (dialogId) {
            case DIALOG_ORDER_CONFIRMATION:
            case DIALOG_ORDER_CANCEL:
                dialog.dismiss();
                break;

            case DIALOG_PRINTER_ERROR:
                dialog.dismiss();
                openScreenSaver();
                break;
        }

    }

    @Override
    public void onDialogCancelled(int dialogId) {

    }

    @Override
    public void onGetAppSettingsComplete(int status) {
        Log.d(TAG, "onGetAppSettingsComplete: status " + status);
        if (status == 1) {
            checkColors();
            getAllProducts();
        } else {
            loading.dismiss();
            changeCatPosition(categoriesList.get(0), 0);
            categoriesAdapter.changeSelectedCategory(0);
            openScreenSaver();
        }
    }

    private void getAllProducts() {
        Log.d(TAG, "getAllProducts: start");
        canLookup = false;
        GetAllProducts getAllProducts = new GetAllProducts(this, this);
        getAllProducts.execute();
    }

    @Override
    public void onGetAllProductsComplete(int status) {
        Log.d(TAG, "onGetAllProductsComplete: status " + status);

        if (loading.isShowing()) {
            loading.dismiss();
        }

        Fragment screenSaver = getSupportFragmentManager().findFragmentByTag("screenSaver");
        if (screenSaver == null) {
            //user mode
            if (!isUpdating) {
                //finishing order
                if (status == 1) {
                    categoriesList = databaseHelper.getAllCategories();
                    categoriesAdapter.setData(categoriesList);
                    changeCatPosition(categoriesList.get(0), 0);
                }
                openScreenSaver();
            } else {
                //updating only
                if (status == 1) {
                    Log.d(TAG, "onGetAllProductsComplete: updating only");
                    categoriesList = databaseHelper.getAllCategories();
                    categoriesAdapter.setData(categoriesList);
                    resetHandler();
                    selectedCategory = categoriesList.get(currentSelectedCategory);
                    selectedCategory.setSelected(1);
                    List<Products> productsList = databaseHelper.getProductsByCatId(selectedCategory.getCatId());
                    productsAdapter.setData(productsList);
                    //check cart existence
                    if (cartData.getCartCount() != 0) {
                        for (int i = 0; i < cartData.getCartCount(); i++) {
                            cartData.getCartItemsList().get(i).setPrice(databaseHelper.getProductPrice(cartData.getCartItemsList().get(i).getProdNum()));
                        }
                        if (getSupportFragmentManager().findFragmentByTag("Cart") != null) {
                            cartFragment.adapter.notifyDataSetChanged();
                            checkTotalPrice();

                        } else {
                            updateCart();
                        }

                    }
                    loading.dismiss();
                }
            }
        } else {
            //screen saver mode
            if (status == 1) {
                categoriesList = databaseHelper.getAllCategories();
                categoriesAdapter.setData(categoriesList);
                changeCatPosition(categoriesList.get(0), 0);
            }
        }

        isUpdating = false;
        canLookup = true;
    }

    private void initLookupsTimer() {
        Log.d(TAG, "initLookupsTimer: start");
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "run: run " + canLookup);
                if (canLookup) {
                    canLookup = false;
                    updateLookup = new GetUpdateLookup(MainActivity.this);
                    updateLookup.execute();
                }
                handler.postDelayed(this, 5000);
            }
        }, 5000);
    }

    @Override
    public void onGetUpdateLookup(int status, int lookupId) {
        Log.d(TAG, "onGetUpdateLookup: start status " + status + ", lookup id: " + lookupId);
        if (status == 1) {
            if (lookupId != AppSettings.UPDATE_LOOKUP) {
                AppSettings.UPDATE_LOOKUP = lookupId;
                isUpdating = true;
                Log.d(TAG, "onGetUpdateLookup: current category index " + currentSelectedCategory);
                runOnUiThread(() -> {
                    loading.show();
                    loading.setTitle("Updating Prices");
                    loading.setMessage("Please wait...");
                    loading.startAnimation(R.raw.sync_animation);
                });
                getAllProducts();
            } else {
                canLookup = true;
            }
        } else {
            canLookup = true;
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            initConnection();
        } else {
            loading.show();
            loading.setTitle("Reconnecting");
            loading.setMessage("Please wait...");
            loading.startAnimation(R.raw.connection_lost);
        }
    }

    private void initConnection() {
        ConnectionSingleton.initConnection(this,getSharedPreferences(BuildConfig.APPLICATION_ID,MODE_PRIVATE));
    }

    @Override
    public void onConnectionComplete(int status) {
        Log.d(TAG, "onConnectionComplete: status "+status);

        if(status==1){
            runOnUiThread(() -> {
                if (loading.isShowing()) {
                    loading.dismiss();
                }
            });
        }else{
            new Handler().postDelayed(this::initConnection,2000);
        }

    }
}