package com.bimpos.newkioskapp.activities;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.bimpos.newkioskapp.BuildConfig;
import com.bimpos.newkioskapp.Helpers.Constant;
import com.bimpos.newkioskapp.databinding.ActivitySettingsBinding;
import com.bimpos.newkioskapp.fragments.BannerFragment;
import com.bimpos.newkioskapp.fragments.SettingsFragment;
import com.google.android.material.tabs.TabLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends AppCompatActivity {

    private ActivitySettingsBinding binding;
    private SettingsFragment settingsFragment;
    private BannerFragment bannerFragment;
    String projectRoot;
    private File bannersFolder;
    private static final String TAG = "SettingsActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySettingsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        projectRoot = Environment.getExternalStorageDirectory().getPath();
        File rootFolder = new File(projectRoot, Constant.ROOT_FOLDER);
        bannersFolder = new File(rootFolder, Constant.BANNERS_FOLDER);
        Log.d(TAG, "onCreate: banners " + bannersFolder);

        int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS |
                WindowManager.LayoutParams.SOFT_INPUT_MASK_ADJUST |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        getWindow().getDecorView().setSystemUiVisibility(flags);

        initViewPager();

        binding.versionName.setText(String.format("version %s", BuildConfig.VERSION_NAME));
        binding.save.setOnClickListener(v -> saveSettings());

        binding.exit.setOnClickListener(v -> finish());
    }

    private void initViewPager() {
        settingsFragment = new SettingsFragment();
        bannerFragment = new BannerFragment();
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(settingsFragment, "Server settings");
        adapter.addFrag(bannerFragment, "Banner images");
        binding.viewPager.setAdapter(adapter);
        binding.viewPager.setOffscreenPageLimit(2);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));
    }

    private void saveSettings() {
        if (settingsFragment.canSaveSettings()) {
            binding.progressBar.setVisibility(View.GONE);
            binding.save.setEnabled(false);
            storePics();
        } else {
            Toast.makeText(this, "Please fill all fields above", Toast.LENGTH_SHORT).show();
        }
    }

    private void storePics() {
        List<Uri> uriList = bannerFragment.uriList;
        Log.d(TAG, "storePics: uri list size " + uriList.size());
        if (uriList.size() != 0) {
            for (int i = 0; i < uriList.size(); i++) {
                Uri uri = uriList.get(i);
                Log.d(TAG, "storePics: uri " + uri.toString());
                copyFile(uri, getFileName(uri));
            }
        }
        closeSettings();
    }

    public String getFileName(Uri uri) {
        String result;

        //if uri is content
        if (uri.getScheme() != null && uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    //local filesystem
                    int index = cursor.getColumnIndex("_data");
                    if (index == -1)
                        //google drive
                        index = cursor.getColumnIndex("_display_name");
                    result = cursor.getString(index);
                    if (result != null)
                        uri = Uri.parse(result);
                    else
                        return null;
                }
            } finally {
                cursor.close();
            }
        }

        result = uri.getPath();

        //get filename + ext of path
        int cut = result.lastIndexOf('/');
        if (cut != -1)
            result = result.substring(cut + 1);
        return result;
    }

    private void copyFile(Uri uri, String fileName) {

        File file = new File(bannersFolder, fileName);
        if (!file.exists()) {
            InputStream inputStream;
            try {
                File outputFile = new File(bannersFolder, fileName);

                inputStream = getContentResolver()
                        .openInputStream(uri);
                FileOutputStream fileOutputStream = new FileOutputStream(
                        outputFile);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();
            } catch (FileNotFoundException e) {
                Log.d(TAG, "copyFile: file not found " + e.getMessage());
            } catch (IOException e) {
                Log.d(TAG, "copyFile: exception " + e.getMessage());
            }
        }

    }

    public static void copyStream(InputStream input, OutputStream output) throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    private void closeSettings() {
        startActivity(new Intent(this, LauncherActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager, BEHAVIOR_SET_USER_VISIBLE_HINT);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}