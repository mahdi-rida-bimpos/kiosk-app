package com.bimpos.newkioskapp.networkRequests;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.Helpers.Constant;
import com.bimpos.newkioskapp.Helpers.FormatTime;
import com.bimpos.newkioskapp.models.Cart;
import com.bimpos.newkioskapp.models.CartComboHeader;
import com.bimpos.newkioskapp.models.CartComboModifier;
import com.bimpos.newkioskapp.models.CartProduct;
import com.bimpos.newkioskapp.models.CartQuestionHeader;
import com.bimpos.newkioskapp.models.CartQuestionModifier;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PlaceOrder extends AsyncTask<String, String, String> {


    private static final String TAG = "PlaceOrder";
    private final Cart cart;
    private final SharedPreferences preferences;
    private int transactId = 0;
    private final OnInsertComplete listener;
    private String tableName = "DELIVERY";
    private String orderDetailQuery, orderHeaderQuery, remarkDescription;

    public interface OnInsertComplete {
        void onInsertComplete(int transactId);
    }

    public PlaceOrder(SharedPreferences preferences, OnInsertComplete listener) {
        cart = Cart.getCartData();
        this.listener = listener;
        this.preferences = preferences;
        initQueries();
    }

    private void initQueries() {

        orderHeaderQuery = "INSERT INTO " + tableName + "ORDERHEADER ( MEMCODE ,AMOUNT ,TAXEX ,TAX ,PAID,BALANCE ,CURRENCY,USERID,WHOAUTH,OPENDATE,"
                + " TIM,CASH,CASHINDEX,STATION,ISSCHEDULED,DELIVERED,DORDERNUM,CONTACTID,TABLENUM,STATUSTIM,"
                + " TIMEND,WHOOPENED,OPENTIME,TAX2,TAX3,ORDERTYPE,OPENEDTIME,SEATNUM,SERVICECHARGE,DOREMIND,"
                + " GRATUITYCHARGE,ADDRESSID,GENERATED,INVOICENUM,BRANCHID,STATUS,TOBEDELOPENDATE,TOBEDELPERIOD,"
                + " POINTS,LOCKED,MODIFCOUNTER,DISCPER,DISCVALUE,MODIFSTATUS,REMINDED,RECEIVEDTIME,PENDING)"
                + " VALUES("
                + " %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
                + " '%s',%s,%s,%s,%s,%s,%s,%s,%s,'%s',"
                + " '%s',%s,'%s',%s,%s,%s,'%s',%s,%s,%s,"
                + " %s,%s,%s,%s,%s,%s,%s,%s,"
                + " %s,%s,%s,%s,%s,%s,%s,'%s',%s);";

        orderDetailQuery = "INSERT INTO " + tableName + "ORDERDETAIL ( TRANSACT ,BRANCHID ,PRODNUM ,QUAN ,PRICE,TAXEX ,TAX,TAX2,TAX3,TIM,"
                + " OPENDATE,LPRODNUM,DISCOUNT,POINTS,WHOAUTH,USERID,ITEMTYPE,SPRODNUM,AVGCOST,LASTCOST,"
                + " PRICETYPE,RETAILPRICE,DISCPER,DISCVALUE,LOYALTYID,ISPRINTED,ISGENERATED)"
                + " VALUES("
                + " %s,%s,%s,%s,%s,%s,%s,%s,%s,'%s',"
                + " %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
                + " %s,%s,%s,%s,%s,%s,%s,);";

        remarkDescription = "INSERT INTO " + tableName + "DESCRIPT (SBID,DESCRIPT,DODETAILID,BRANCHID)"
                + " VALUES(%s,'%s',%s,%s);";
    }

    @Override
    protected String doInBackground(String... strings) {

        Connection connection = ConnectionSingleton.getConnection();
        if(connection==null){
            return "connection";
        }
        if (getTime(connection)) {
            insertOrderHeader(connection);

            for (int i = 0; i < cart.getCartItemsList().size(); i++) {

                CartProduct cartProduct = cart.getCartItemsList().get(i);
                insertProduct(cartProduct, connection);

                if (cartProduct.getQuestionList().size() > 0) {

                    for (int ii = 0; ii < cartProduct.getQuestionList().size(); ii++) {
                        CartQuestionHeader header = cartProduct.getQuestionList().get(ii);
                        for (int iii = 0; iii < header.getCartQuestionModifiers().size(); iii++) {
                            CartQuestionModifier modifier = header.getCartQuestionModifiers().get(iii);
                            if (header.getRequired() == 1) {
                                insertQuestion(cartProduct, modifier, AppSettings.QUESTION_REQUIRED, connection);
                            } else {
                                insertQuestion(cartProduct, modifier, AppSettings.QUESTION_NOT_REQUIRED, connection);
                            }

                        }
                    }
                }
                if (cartProduct.getComboList().size() > 0) {

                    for (int ii = 0; ii < cartProduct.getComboList().size(); ii++) {
                        CartComboHeader header = cartProduct.getComboList().get(ii);
                        for (int iii = 0; iii < header.getCartComboModifiers().size(); iii++) {
                            CartComboModifier modifier = header.getCartComboModifiers().get(iii);
                            insertCombo(cartProduct, modifier, connection);
                        }
                    }
                }
                if (cartProduct.getRemark().length() > 0) {
                    insertRemark(cartProduct, cartProduct.getRemark(), connection);
                }
            }

            updateOrderHeader(connection);
            getShortTicketId(connection);

            return "success";
        } else {
            return "timeError";
        }

    }

    private void getShortTicketId(Connection connection) {
        Log.d(TAG, "getShortTicketId: start");
        try {
            PreparedStatement preparedStatementCurrency = connection.prepareStatement(
                    "select * from TICKETNUMBER where opendate=" + AppSettings.OPEN_DATE + " order by ID desc");
            ResultSet resultSet = preparedStatementCurrency.executeQuery();
            if (resultSet.next()) {
                AppSettings.SHORT_TICKET_ID = resultSet.getInt("ID");
            }
            Log.d(TAG, "getShortTicketId: short ticket number " + AppSettings.SHORT_TICKET_ID);
            AppSettings.SHORT_TICKET_ID=AppSettings.SHORT_TICKET_ID+1;

            String query = "INSERT INTO TICKETNUMBER (ID,BRANCHID,TRANSACT,OPENDATE,TYP)VALUES("
                    + AppSettings.SHORT_TICKET_ID + ","
                    + AppSettings.BRANCH_ID + ","
                    + transactId + ","
                    + AppSettings.OPEN_DATE + ",0)";
            PreparedStatement updateQuery = connection.prepareStatement(query);
           int status =updateQuery.executeUpdate();
            Log.d(TAG, "getShortTicketId: status "+status);
            preparedStatementCurrency.close();

        } catch (SQLException e) {
            Log.d(TAG, "getShortTicketId: error "+e.getMessage());
        }
    }

    private void updateOrderHeader(Connection connection) {
        String query = "UPDATE " + tableName + "ORDERHEADER SET STATUS = 1 WHERE TRANSACT=" + transactId;
        try {
            PreparedStatement updateQuery = connection.prepareStatement(query);
            updateQuery.executeUpdate();
            listener.onInsertComplete(transactId);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void insertOrderHeader(Connection connection) {

        try {

            String query = String.format(orderHeaderQuery,
                    0,//memcode
                    cart.getCartTotalPrice(),// amount
                    cart.getTaxEx(),// taxex
                    cart.getTax1(),// tax
                    0,// paid
                    cart.getCartTotalPrice(),// BALANCE
                    AppSettings.CURRENCY_ID,// CURRENCY
                    AppSettings.USER_ID,// USERID
                    AppSettings.USER_ID,// WHOAUTH
                    AppSettings.OPEN_DATE,// OPENDATE

                    AppSettings.TIM_STRING,// TIM //todo to be checked
                    0,//CASH
                    0,//CASHINDEX
                    preferences.getInt(Constant.STATION_ID, 0),// STATION
                    0,// ISSCHEDULED
                    0,// DELIVERED
                    0,// DORDERNUM
                    0,// CONTACTID
                    0,// TABLENUM
                    AppSettings.TIM_STRING,// STATUSTIM //todo to be checked

                    AppSettings.TIM_STRING,// TIMEND //todo to be checked
                    AppSettings.USER_ID,// WHOOPENED
                    String.valueOf(AppSettings.OPEN_TIME),// OPENTIME
                    cart.getTax2(),// TAX2
                    cart.getTax3(),// TAX3
                    cart.getOrderType(),// ORDERTYPE
                    AppSettings.TIM_STRING,// OPENEDTIME //todo to be checked
                    0,// SEATNUM
                    0,// SERVICECHARGE
                    0,// DOREMIND

                    0,// GRATUTYCHARGE
                    0,// ADDRESSID
                    0,// GENERATED
                    0,// INVOICENUM
                    AppSettings.BRANCH_ID,// BRANCHIDREMIND
                    0,// STATUS
                    AppSettings.OPEN_DATE,// TOBEDELOPENDATE
                    0,// TOBEDELPERIOD

                    0,// POINTS
                    0,// LOCKED // todo to ask
                    0,// MODIFCOUNTER
                    0,// DISCPER
                    0,// DISCVALUE
                    0,// MODIFSTATUS
                    0,// REMINDED
                    AppSettings.TIM_STRING,//RECEIVEDTIME
                    1//PENDING
            );
            Log.d(TAG, "insertOrderHeader: header query:\n-------------\n " + query + "\n");
            PreparedStatement orderHeader = connection.prepareStatement(query);

            int result = orderHeader.executeUpdate();
            transactId = getTransactId(connection);
            Log.d(TAG, "insertOrderHeader: transact id " + transactId);
            Log.d(TAG, "insertOrderHeader: result " + result);
            orderHeader.close();
        } catch (SQLException e) {
            Log.d(TAG, "insertOrderHeader: error "+e.getMessage());
        }

    }

    private void insertProduct(CartProduct cartProduct, Connection connection) {
        try {

            String query = String.format(orderDetailQuery,
                    transactId,//TRANSACT
                    AppSettings.BRANCH_ID,//BRANCHID
                    cartProduct.getProdNum(),//PRODNUM
                    cartProduct.getQty(),//QUAN
                    cartProduct.getPrice(),//PRICE
                    cartProduct.getTaxEx(),//TAXEX
                    cartProduct.getTax1(),//TAX
                    cartProduct.getTax2(),//TAX2
                    cartProduct.getTax3(),//TAX3
                    AppSettings.TIM_STRING,//TIM

                    AppSettings.OPEN_DATE,//OPENDATE
                    cartProduct.getProdNum(),//LPRODNUM
                    0,//DISCOUNT
                    0,//POINTS
                    AppSettings.USER_ID,//WHOAUTH
                    AppSettings.USER_ID,//USERID
                    AppSettings.REGULAR_PRODUCT,//ITEMTYPE
                    0,//SPRODNUM
                    cartProduct.getAvgCost(),//AVGCOST
                    cartProduct.getLastCost(),//LASTCOST

                    1,//PRICETYPE
                    cartProduct.getPrice(),//RETAILPRICE
                    0,//DISCPER
                    0,//DISCVALUE
                    0,//LOYALTYID
                    0,//ISPRINTED
                    0//ISGENERATED
            );
            Log.d(TAG, "insertProduct: product query :\n-------------\n" + query + "\n");

            PreparedStatement productDetail = connection.prepareStatement(query);
            int result = productDetail.executeUpdate();
            Log.d(TAG, "insertOrderHeader: result product " + result);
        } catch (SQLException e) {
            Log.d(TAG, "insertProduct: error " + e.getMessage());
        }
    }

    private void insertCombo(CartProduct cartProduct, CartComboModifier comboModifier, Connection connection) {
        try {

            String query = String.format(orderDetailQuery,
                    transactId,//TRANSACT
                    AppSettings.BRANCH_ID,//BRANCHID
                    comboModifier.getModifierId(),//PRODNUM
                    comboModifier.getCount(),//QUAN
                    comboModifier.getPrice(),//PRICE
                    comboModifier.getTaxEx(),//TAXEX
                    comboModifier.getTax1(),//TAX
                    comboModifier.getTax2(),//TAX2
                    comboModifier.getTax3(),//TAX3
                    AppSettings.TIM_STRING,//TIM

                    AppSettings.OPEN_DATE,//OPENDATE
                    cartProduct.getProdNum(),//LPRODNUM
                    0,//DISCOUNT
                    0,//POINTS
                    AppSettings.USER_ID,//WHOAUTH
                    AppSettings.USER_ID,//USERID
                    AppSettings.COMBO,//ITEMTYPE
                    0,//SPRODNUM
                    comboModifier.getAvgCost(),//AVGCOST
                    comboModifier.getLastCost(),//LASTCOST

                    1,//PRICETYPE
                    comboModifier.getPrice(),//RETAILPRICE
                    0,//DISCPER
                    0,//DISCVALUE
                    0,//LOYALTYID
                    0,//ISPRINTED
                    0//ISGENERATED
            );
            Log.d(TAG, "insertProduct: combo query :\n-------------\n" + query + "\n");

            PreparedStatement productDetail = connection.prepareStatement(query);
            int result = productDetail.executeUpdate();
            Log.d(TAG, "insertOrderHeader: result combo " + result);
        } catch (SQLException e) {
            Log.d(TAG, "insertProduct: error " + e.getMessage());
        }
    }

    private void insertQuestion(CartProduct cartProduct, CartQuestionModifier questionModifier, int itemType, Connection connection) {
        try {

            String query = String.format(orderDetailQuery,
                    transactId,//TRANSACT
                    AppSettings.BRANCH_ID,//BRANCHID
                    questionModifier.getModifierId(),//PRODNUM
                    1,//QUAN
                    questionModifier.getPrice(),//PRICE
                    questionModifier.getTaxEx(),//TAXEX
                    questionModifier.getTax1(),//TAX
                    questionModifier.getTax2(),//TAX2
                    questionModifier.getTax3(),//TAX3
                    AppSettings.TIM_STRING,//TIM

                    AppSettings.OPEN_DATE,//OPENDATE
                    0,//LPRODNUM
                    0,//DISCOUNT
                    0,//POINTS
                    AppSettings.USER_ID,//WHOAUTH
                    AppSettings.USER_ID,//USERID
                    itemType,//ITEMTYPE
                    cartProduct.getProdNum(),//SPRODNUM
                    questionModifier.getAvgCost(),//AVGCOST
                    questionModifier.getLastCost(),//LASTCOST

                    1,//PRICETYPE
                    questionModifier.getPrice(),//RETAILPRICE
                    0,//DISCPER
                    0,//DISCVALUE
                    0,//LOYALTYID
                    0,//ISPRINTED
                    0//ISGENERATED
            );
            Log.d(TAG, "insertProduct: question query :\n-------------\n" + query + "\n");

            PreparedStatement productDetail = connection.prepareStatement(query);
            int result = productDetail.executeUpdate();
            Log.d(TAG, "insertOrderHeader: result question " + result);
        } catch (SQLException e) {
            Log.d(TAG, "insertProduct: error " + e.getMessage());
        }
    }

    private void insertRemark(CartProduct cartProduct, String remark, Connection connection) {
        try {
            int index = 0;
            int doDetailsId = 0;

            String query = String.format(orderDetailQuery,
                    transactId,//TRANSACT
                    AppSettings.BRANCH_ID,//BRANCHID
                    AppSettings.REMARK_NUM_VALUE,//PRODNUM
                    1,//QUAN
                    0,//PRICE
                    0,//TAXEX
                    0,//TAX
                    0,//TAX2
                    0,//TAX3
                    AppSettings.TIM_STRING,//TIM

                    AppSettings.OPEN_DATE,//OPENDATE
                    cartProduct.getProdNum(),//LPRODNUM
                    0,//DISCOUNT
                    0,//POINTS
                    AppSettings.USER_ID,//WHOAUTH
                    AppSettings.USER_ID,//USERID
                    1,//ITEMTYPE
                    0,//SPRODNUM
                    0,//AVGCOST
                    0,//LASTCOST

                    1,//PRICETYPE
                    0,//RETAILPRICE
                    0,//DISCPER
                    0,//DISCVALUE
                    0,//LOYALTYID
                    0,//ISPRINTED
                    0//ISGENERATED
            );
            Log.d(TAG, "insertProduct: remark query :\n-------------\n" + query + "\n");

            PreparedStatement remarkDetail = connection.prepareStatement(query);
            int result = remarkDetail.executeUpdate();
            Log.d(TAG, "insertOrderHeader: result remark " + result);

            doDetailsId = getDodDetails(connection);

            String query1 = String.format(remarkDescription,
                    AppSettings.BRANCH_ID, //SBID
                    remark, //REMARK
                    doDetailsId, //DODETAILID,
                    AppSettings.BRANCH_ID //BRANCHID
            );
            Log.d(TAG, "insertRemark: remark description query :\n-------------\n" + query1 + "\n");

            PreparedStatement remarkDetailDescription = connection.prepareStatement(query1);
            int result1 = remarkDetailDescription.executeUpdate();
            Log.d(TAG, "insertOrderHeader: result remark description " + result);
        } catch (SQLException e) {
            Log.d(TAG, "insertProduct: error " + e.getMessage());
        }
    }

    private int getTransactId(Connection connection) {
        try {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT @@IDENTITY as TRANSACTID;");
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {

                return resultSet.getInt("TRANSACTID");
            }
            ps.close();
            return 0;
        } catch (SQLException e) {
            return 0;
        }
    }

    private int getDodDetails(Connection connection) {
        try {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT @@IDENTITY as doDetailsId;");
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {

                return resultSet.getInt("doDetailsId");
            }
            ps.close();
            return 0;
        } catch (SQLException e) {
            return 0;
        }
    }

    private boolean getTime(Connection connection) {
        try {
            PreparedStatement preparedStatementCurrency = connection.prepareStatement(
                    "select DATEFORMAT(GETDATE(), 'YYYY-MM-DD HH:mm:ss') as TIM");
            ResultSet resultSet = preparedStatementCurrency.executeQuery();
            if (resultSet.next()) {
                Log.d(TAG, "getTime: original string " + resultSet.getString("TIM"));
                AppSettings.TIM_STRING = resultSet.getString("TIM");
                Log.d(TAG, "getTime: tim " + AppSettings.TIM_STRING);
                AppSettings.OPEN_DATE = FormatTime.getOpenDate(AppSettings.TIM_STRING);
                Log.d(TAG, "getTime: open date " + AppSettings.OPEN_DATE);
                AppSettings.OPEN_TIME = FormatTime.getOpenTime(AppSettings.TIM_STRING);
                Log.d(TAG, "getTime: open time " + AppSettings.OPEN_TIME);
            }

            Log.d(TAG, "getTime: -----------------------------------");
            preparedStatementCurrency.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    protected void onPostExecute(String result) {

    }
}