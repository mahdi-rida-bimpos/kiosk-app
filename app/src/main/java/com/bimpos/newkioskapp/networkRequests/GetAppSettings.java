package com.bimpos.newkioskapp.networkRequests;

import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.Helpers.ParseColor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GetAppSettings extends AsyncTask<String, String, String> {
    private final OnGetAppSettingsComplete listener;
    private static final String TAG = "GetAppSettings";

    public interface OnGetAppSettingsComplete {
        void onGetAppSettingsComplete(int status);
    }

    public GetAppSettings(OnGetAppSettingsComplete listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... strings) {

        Connection connection = ConnectionSingleton.getConnection();
        if(connection==null){
            return "connection";
        }
        if (getBranchId(connection)&& getUpdateLookup(connection) && getRemarkOption(connection)
                && getOrderType(connection) && getColors(connection)
                && getTax(connection) && getCompanyInfo(connection)
                ) {
            getCompanyImage(connection);
            return "success";
        } else {
            return "error";
        }
    }

    private boolean getBranchId(Connection connection) {
        try {
            PreparedStatement preparedStatementCurrency = connection.prepareStatement(
                    "select BRANCHID,DESCRIPT,CURRENCIES.ID as CURRENCYID"
                            + " from MYID,CURRENCIES"
                            + " where clientID = 'BIM111'"
                            + " and"
                            + " CURRENCIES.ISDEFAULT='1' and CURRENCIES.ISACTIVE='1'");
            ResultSet resultSet = preparedStatementCurrency.executeQuery();
            while (resultSet.next()) {
                AppSettings.CURRENCY_STRING = resultSet.getString("DESCRIPT");
                AppSettings.BRANCH_ID = resultSet.getInt("BRANCHID");
                AppSettings.CURRENCY_ID = resultSet.getInt("CURRENCYID");
            }
            resultSet.close();
            preparedStatementCurrency.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    private boolean getRemarkOption(Connection connection) {
        try {
            PreparedStatement preparedStatementRemark = connection.prepareStatement(
                    "select NUMVALUE"
                            + " from SETTINGS"
                            + " where ID = 'PRQUESTION'"
                            + " and"
                            + " BRANCHID=" + AppSettings.BRANCH_ID);
            ResultSet resultSetRemark = preparedStatementRemark.executeQuery();
            if (resultSetRemark.next()) {
                AppSettings.REMARK_NUM_VALUE = resultSetRemark.getInt("NUMVALUE");
            } else {
                AppSettings.REMARK_NUM_VALUE = 0;
            }
            resultSetRemark.close();
            preparedStatementRemark.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    private boolean getTax(Connection connection) {
        try {
            PreparedStatement preparedStatementTax = connection.prepareStatement(
                    "select TAX1,TAX1DESCRIPT,TAX1SYMBOL," +
                            " TAX2,TAX2DESCRIPT,TAX2SYMBOL," +
                            " TAX3,TAX3DESCRIPT,TAX3SYMBOL " +
                            " from tax");

            ResultSet resultSetTax = preparedStatementTax.executeQuery();
            if (resultSetTax.next()) {
                AppSettings.TAX1_AMOUNT = resultSetTax.getInt("TAX1");
                AppSettings.TAX1_DESCRIPTION = resultSetTax.getString("TAX1DESCRIPT");
                AppSettings.TAX1_SYMBOL = resultSetTax.getString("TAX1SYMBOL");
                AppSettings.TAX2_AMOUNT = resultSetTax.getInt("TAX2");
                AppSettings.TAX2_DESCRIPTION = resultSetTax.getString("TAX2DESCRIPT");
                AppSettings.TAX2_SYMBOL = resultSetTax.getString("TAX2SYMBOL");
                AppSettings.TAX3_AMOUNT = resultSetTax.getInt("TAX3");
                AppSettings.TAX3_DESCRIPTION = resultSetTax.getString("TAX3DESCRIPT");
                AppSettings.TAX3_SYMBOL = resultSetTax.getString("TAX3SYMBOL");
            } else {
                return false;
            }
            resultSetTax.close();
            preparedStatementTax.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    private boolean getCompanyInfo(Connection connection) {
        try {
            PreparedStatement preparedStatementRemark = connection.prepareStatement(
                    "select COMPANY from PARAMETERS");
            ResultSet resultSetRemark = preparedStatementRemark.executeQuery();

            if (resultSetRemark.next()) {
                AppSettings.RESTAURANT_NAME= resultSetRemark.getString("COMPANY");
            } else {
                AppSettings.RESTAURANT_NAME="Company";
            }
            resultSetRemark.close();
            preparedStatementRemark.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    private boolean getUpdateLookup(Connection connection) {
        try {
            Log.d(TAG, "getUpdateLookup: start");
            PreparedStatement preparedStatementRemark = connection.prepareStatement(
                    "SELECT PRODLOOKUPID FROM UPDATELOOKUPS;");
            ResultSet resultSetRemark = preparedStatementRemark.executeQuery();
            Log.d(TAG, "getUpdateLookup: contnue");
            if (resultSetRemark.next()) {
                Log.d(TAG, "getUpdateLookup: lookup "+resultSetRemark.getInt("PRODLOOKUPID"));
                AppSettings.UPDATE_LOOKUP= resultSetRemark.getInt("PRODLOOKUPID");
                Log.d(TAG, "getUpdateLookup: update lookup "+AppSettings.UPDATE_LOOKUP);
            }
            resultSetRemark.close();
            preparedStatementRemark.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    private boolean getCompanyImage(Connection connection) {
        try {
            PreparedStatement preparedStatementRemark = connection.prepareStatement(
                    "select PICTURE from RESOURCES WHERE RESOURCEID = 'LOGO' ");
            ResultSet rs = preparedStatementRemark.executeQuery();

            if (rs.next()) {
                Log.d(TAG, "getCompanyImage: "+rs.getString("PICTURE"));
                AppSettings.RESTAURANT_IMAGE= rs.getString("PICTURE");
            }
            preparedStatementRemark.close();
            return true;
        } catch (SQLException e) {
            Log.d(TAG, "getCompanyImage: error " + e.getMessage());
            return false;
        }
    }

    private boolean getOrderType(Connection connection) {
        try {
            PreparedStatement preparedStatementOrderType = connection.prepareStatement(
                    "SELECT Sts.ID, Sts.NumValue, OrdType.Descript"
                            + " FROM Settings Sts"
                            + " inner JOIN OrderType OrdType ON Sts.NumValue = OrdType.ID"
                            + " WHERE Sts.ID IN ('KIOSKOTYPEDINEIN','KIOSKOTYPETAKEAWAY')"
                            + " and branchid=" + AppSettings.BRANCH_ID);
//            ResultSet resultSetOrderType = preparedStatementOrderType.executeQuery();
            AppSettings.ORDER_TYPE_MAP.put(12, "Dine In");
            AppSettings.ORDER_TYPE_MAP.put(11, "Take Away");
            AppSettings.DINE_IN = 12;
            AppSettings.TAKE_AWAY = 11;
//            while (resultSetOrderType.next()) {
//                AppSettings.ORDER_TYPE_MAP.put(resultSetOrderType.getInt("NUMVALUE"), resultSetOrderType.getString("DESCRIPT"));
//
//                if (resultSetOrderType.getString("ID").equalsIgnoreCase("KIOSKOTYPEDINEIN")) {
//                    AppSettings.DINE_IN = resultSetOrderType.getInt("NumValue");
//                }
//                if (resultSetOrderType.getString("ID").equalsIgnoreCase("KIOSKOTYPETAKEAWAY")) {
//                    AppSettings.TAKE_AWAY = resultSetOrderType.getInt("NumValue");
//                }
//            }

            preparedStatementOrderType.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    private boolean getColors(Connection connection) {
        PreparedStatement psGetColorsStrings;
        try {
            psGetColorsStrings = connection.prepareStatement("select  mo.propertyname, isnull(mto.propertyvalue, mo.defaultvalue, 0) as dv, " +
                    "isnull(mto.propertyvaluestring, mo.defaultvaluestring, '') as dvs from dba.mobileappobjects mo left outer join dba.mobileappthemeobjects mto" +
                    " on mo.objectid=mto.objectid and mo.appid=mto.appid and mto.themeid=5 where mo.appid=5 and mo.propertyname in " +
                    "('GREETINGTEXTORDERTYPESCREEN','POPUPHEADERVIEWBACKCOLOR', 'POPUPHEADERVIEWFORECOLOR', 'POPUP.YESBUTTON.BACKCOLOR', 'POPUP.YESBUTTON.FORECOLOR', " +
                    "'POPUP.NOBUTTON.BACKCOLOR', 'POPUP.NOBUTTON.FORECOLOR', 'PLUSBUTTON.ENABLED.BACKCOLOR', 'PLUSBUTTON.ENABLED.FORECOLOR', " +
                    "'PLUSBUTTON.DISABLED.BACKCOLOR', 'PLUSBUTTON.DISABLED.FORECOLOR',  'MINUSBUTTON.ENABLED.BACKCOLOR', 'MINUSBUTTON.ENABLED.FORECOLOR'," +
                    " 'MINUSBUTTON.DISABLED.BACKCOLOR','MINUSBUTTON.DISABLED.FORECOLOR', 'FOOTERYES.ENABLED.BACKCOLOR', 'FOOTERYES.ENABLED.FORECOLOR', 'FOOTERYES.DISABLED.BACKCOLOR', " +
                    "'FOOTERYES.DISABLED.FORECOLOR', 'FOOTERNO.ENABLED.BACKCOLOR','FOOTERNO.ENABLED.FORECOLOR', 'FOOTERNO.DISABLED.BACKCOLOR', 'FOOTERNO.DISABLED.FORECOLOR','PRICETEXTCOLOR','COMBOHEADERRIGHTCOLOR'" +
                    " ,'COMBOHEADERLEFTCOLOR','QUESTIONHEADERRIGHTCOLOR','QUESTIONHEADERLEFTCOLOR','QUESTIONHEADERFORECOLOR','COMBOHEADERFORECOLOR','QUESTIONITEMEFORECOLOR','QUESTIONITEMBACKCOLOR'," +
                    "'COMBOITEMFORECOLOR','COMBOITEMBACKCOLOR','QUESTIONITEMDISABLEDFORECOLOR','COMBOITEMDISABLEDFORECOLOR'  )");

            ResultSet resultSetColours = psGetColorsStrings.executeQuery();

            while (resultSetColours.next()) {
                if (resultSetColours.getString("propertyname").equalsIgnoreCase("GREETINGTEXTORDERTYPESCREEN")) {
                    AppSettings.GREETING_ORDER_TYPE_SCREEN = resultSetColours.getString(3);
                } else {
                    String color = resultSetColours.getString(3);
                    int colorInteger = ParseColor.parse(color);
                    switch (resultSetColours.getString("propertyname")) {

                        case "POPUPHEADERVIEWBACKCOLOR":
                            AppSettings.DIALOG_TITLE_BACK_COLOR = colorInteger;
                            break;
                        case "POPUPHEADERVIEWFORECOLOR":
                            AppSettings.DIALOG_TITLE_TEXT_COLOR = colorInteger;
                            break;
                        case "POPUP.YESBUTTON.BACKCOLOR":
                            AppSettings.DIALOG_POSITIVE_BUTTON_BACK_COLOR = colorInteger;
                            break;
                        case "POPUP.YESBUTTON.FORECOLOR":
                            AppSettings.DIALOG_POSITIVE_BUTTON_TEXT_COLOR = colorInteger;
                            break;
                        case "POPUP.NOBUTTON.BACKCOLOR":
                            AppSettings.DIALOG_NEGATIVE_BUTTON_BACK_COLOR = colorInteger;
                            break;
                        case "POPUP.NOBUTTON.FORECOLOR":
                            AppSettings.DIALOG_NEGATIVE_BUTTON_TEXT_COLOR = colorInteger;
                            break;

                        case "PLUSBUTTON.ENABLED.BACKCOLOR":
                            AppSettings.BUTTON_PLUS_ENABLED_BACK_COLOR = colorInteger;
                            break;
                        case "PLUSBUTTON.ENABLED.FORECOLOR":
                            AppSettings.BUTTON_PLUS_ENABLED_TEXT_COLOR = colorInteger;
                            break;
                        case "PLUSBUTTON.DISABLED.BACKCOLOR":
                            AppSettings.BUTTON_PLUS_DISABLED_BACK_COLOR = colorInteger;
                            break;
                        case "PLUSBUTTON.DISABLED.FORECOLOR":
                            AppSettings.BUTTON_PLUS_DISABLED_TEXT_COLOR = colorInteger;
                            break;
                        case "MINUSBUTTON.ENABLED.BACKCOLOR":
                            AppSettings.BUTTON_MINUS_ENABLED_BACK_COLOR = colorInteger;
                            break;
                        case "MINUSBUTTON.ENABLED.FORECOLOR":
                            AppSettings.BUTTON_MINUS_ENABLED_TEXT_COLOR = colorInteger;
                            break;
                        case "MINUSBUTTON.DISABLED.BACKCOLOR":
                            AppSettings.BUTTON_MINUS_DISABLED_BACK_COLOR = colorInteger;
                            break;
                        case "MINUSBUTTON.DISABLED.FORECOLOR":
                            AppSettings.BUTTON_MINUS_DISABLED_TEXT_COLOR = colorInteger;
                            break;

                        case "FOOTERYES.ENABLED.BACKCOLOR":
                            AppSettings.BOTTOM_YES_ENABLED_BACK_COLOR = colorInteger;
                            break;
                        case "FOOTERYES.ENABLED.FORECOLOR":
                            AppSettings.BOTTOM_YES_ENABLED_TEXT_COLOR = colorInteger;
                            break;
                        case "FOOTERYES.DISABLED.BACKCOLOR":
                            AppSettings.BOTTOM_YES_DISABLED_BACK_COLOR = colorInteger;
                            break;
                        case "FOOTERYES.DISABLED.FORECOLOR":
                            AppSettings.BOTTOM_YES_DISABLED_TEXT_COLOR = colorInteger;
                            break;

                        case "FOOTERNO.ENABLED.BACKCOLOR":
                            AppSettings.BOTTOM_NO_ENABLED_BACK_COLOR = colorInteger;
                            break;
                        case "FOOTERNO.ENABLED.FORECOLOR":
                            AppSettings.BOTTOM_NO_ENABLED_TEXT_COLOR = colorInteger;
                            break;
                        case "FOOTERNO.DISABLED.BACKCOLOR":
                            AppSettings.BOTTOM_NO_DISABLED_BACK_COLOR = colorInteger;
                            break;
                        case "FOOTERNO.DISABLED.FORECOLOR":
                            AppSettings.BOTTOM_NO_DISABLED_TEXT_COLOR = colorInteger;
                            break;

                        case "PRICETEXTCOLOR":
                            AppSettings.PRICE_TEXT_COLOR = colorInteger;
                            break;

                        case "COMBOHEADERRIGHTCOLOR":
                            AppSettings.COMBO_HEADER_RIGHT_BACK_COLOR = colorInteger;
                            break;
                        case "COMBOHEADERLEFTCOLOR":
                            AppSettings.COMBO_HEADER_LEFT_BACK_COLOR = colorInteger;
                            break;
                        case "COMBOHEADERFORECOLOR":
                            AppSettings.COMBO_HEADER_TEXT_COLOR = colorInteger;
                            break;
                        case "COMBOITEMDISABLEDFORECOLOR":
                            AppSettings.COMBO_ITEM_DISABLED_TEXT_COLOR = colorInteger;
                            break;
                        case "COMBOITEMFORECOLOR":
                            AppSettings.COMBO_ITEM_ENABLED_TEXT_COLOR = colorInteger;
                            break;
                        case "COMBOITEMBACKCOLOR":
                            AppSettings.COMBO_ITEM_ENABLED_BACK_COLOR = colorInteger;
                            break;


                        case "QUESTIONHEADERRIGHTCOLOR":
                            AppSettings.QUESTION_HEADER_RIGHT_BACK_COLOR = colorInteger;
                            break;
                        case "QUESTIONHEADERLEFTCOLOR":
                            AppSettings.QUESTION_HEADER_LEFT_BACK_COLOR = colorInteger;
                            break;
                        case "QUESTIONHEADERFORECOLOR":
                            AppSettings.QUESTION_HEADER_TEXT_COLOR = colorInteger;
                            break;


                        case "QUESTIONITEMEFORECOLOR":
                            AppSettings.QUESTION_ITEM_ENABLED_TEXT_COLOR = colorInteger;
                            break;
                        case "QUESTIONITEMBACKCOLOR":
                            AppSettings.QUESTION_ITEM_ENABLED_BACK_COLOR = colorInteger;
                            break;

                        case "QUESTIONITEMDISABLEDFORECOLOR":
                            AppSettings.QUESTION_ITEM_DISABLED_TEXT_COLOR = colorInteger;
                            break;
                    }
                }
            }

            resultSetColours.close();
            psGetColorsStrings.close();
            return true;
        } catch (SQLException e) {
            return false;
        }

    }

    @Override
    protected void onPostExecute(String result) {
        if (result.equalsIgnoreCase("error")) {
            listener.onGetAppSettingsComplete(0);
        } else if (result.equalsIgnoreCase("success")){
            listener.onGetAppSettingsComplete(1);
        }else{
            listener.onGetAppSettingsComplete(-1);
        }

    }
}