package com.bimpos.newkioskapp.networkRequests;


import android.content.SharedPreferences;
import android.util.Log;

import com.bimpos.newkioskapp.Helpers.Constant;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import needle.Needle;

public class ConnectionSingleton {

    private static final String TAG = "ConnectToDB";
    private static Connection connection;
    private static SharedPreferences preferences;


    public interface OnConnectionComplete {
        void onConnectionComplete(int status);
    }

    public static Connection getConnection() {
        if (connection == null) {
            Needle.onBackgroundThread().execute(() -> {
                try {
                    String serverIp = preferences.getString(Constant.SERVER_IP, "");
                    String serverName = preferences.getString(Constant.SERVER_NAME, "");
                    String serverPort = preferences.getString(Constant.SERVER_PORT, "");
                    String databaseUserName = preferences.getString(Constant.DB_USER_NAME, "");
                    String databasePassword = preferences.getString(Constant.DB_PASSWORD, "");
                    Class.forName("com.sybase.jdbc4.jdbc.SybDriver");
                    connection = DriverManager.getConnection("jdbc:sybase:Tds:" + serverIp + ":" + serverPort + "/" + serverName, databaseUserName, databasePassword);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        return connection;
    }

    public static void initConnection(OnConnectionComplete listener, SharedPreferences preferences) {
        ConnectionSingleton.preferences = preferences;
        if (connection == null) {
            String serverIp = preferences.getString(Constant.SERVER_IP, "");
            String serverName = preferences.getString(Constant.SERVER_NAME, "");
            String serverPort = preferences.getString(Constant.SERVER_PORT, "");
            String databaseUserName = preferences.getString(Constant.DB_USER_NAME, "");
            String databasePassword = preferences.getString(Constant.DB_PASSWORD, "");
            Needle.onBackgroundThread().execute(() -> {
                try {
                    Class.forName("com.sybase.jdbc4.jdbc.SybDriver");
                    Log.d(TAG, "initConnection: start");
                    DriverManager.setLoginTimeout(3);
                    connection = DriverManager.getConnection("jdbc:sybase:Tds:" + serverIp + ":" + serverPort + "/" + serverName, databaseUserName, databasePassword);
                    Log.d(TAG, "initConnection: true ");
                    listener.onConnectionComplete(1);
                } catch (Exception e) {
                    Log.d(TAG, "initConnection: error " + e.getMessage());
                    listener.onConnectionComplete(0);
                }
            });
        } else {
            listener.onConnectionComplete(1);
        }
    }

    public static void resetConnection() {
        Log.d(TAG, "resetConnection: enter");
        connection=null;
    }
}