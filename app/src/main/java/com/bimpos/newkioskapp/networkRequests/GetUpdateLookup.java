package com.bimpos.newkioskapp.networkRequests;

import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.newkioskapp.Helpers.AppSettings;
import com.bimpos.newkioskapp.Helpers.ParseColor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GetUpdateLookup extends AsyncTask<String, String, String> {
    private final OnGetUpdateLookup listener;
    private static final String TAG = "GetAppSettings";

    public interface OnGetUpdateLookup {
        void onGetUpdateLookup(int status,int lookupId);
    }

    public GetUpdateLookup(OnGetUpdateLookup listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... strings) {

        Connection connection = ConnectionSingleton.getConnection();
        if(connection==null){
            listener.onGetUpdateLookup(-1,0);
            return "connection";
        }
        if (getUpdateLookup(connection)) {
            return "success";
        } else {
            return "error";
        }
    }

    private boolean getUpdateLookup(Connection connection) {
        try {
            PreparedStatement preparedStatementRemark = connection.prepareStatement(
                    "select PRODLOOKUPID from updatelookups");
            ResultSet resultSetRemark = preparedStatementRemark.executeQuery();

            if (resultSetRemark.next()) {
                listener.onGetUpdateLookup(1,resultSetRemark.getInt("PRODLOOKUPID"));
            }
            resultSetRemark.close();
            preparedStatementRemark.close();
            return true;
        } catch (SQLException e) {
            listener.onGetUpdateLookup(0,0);
            return false;
        }
    }
}