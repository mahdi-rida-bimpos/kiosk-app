package com.bimpos.newkioskapp.networkRequests;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.bimpos.newkioskapp.Helpers.GetTaxes;
import com.bimpos.newkioskapp.database.DatabaseHelper;
import com.bimpos.newkioskapp.models.Categories;
import com.bimpos.newkioskapp.models.ComboGroup;
import com.bimpos.newkioskapp.models.ComboHeader;
import com.bimpos.newkioskapp.models.ComboModifier;
import com.bimpos.newkioskapp.models.Products;
import com.bimpos.newkioskapp.models.QuestionGroup;
import com.bimpos.newkioskapp.models.QuestionHeader;
import com.bimpos.newkioskapp.models.QuestionModifier;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GetAllProducts extends AsyncTask<String, String, String> {

    private final OnGetAllProductsComplete listener;

    private final DatabaseHelper databaseHelper;

    private static final String TAG = "GetAllProducts";

    public interface OnGetAllProductsComplete {
        void onGetAllProductsComplete(int status);
    }

    public GetAllProducts(OnGetAllProductsComplete listener, Context context) {
        databaseHelper = new DatabaseHelper(context);
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... strings) {

        List<Categories> categoriesList = new ArrayList<>();
        List<Products> productsList = new ArrayList<>();
        List<ComboGroup> comboGroupList = new ArrayList<>();
        List<ComboHeader> comboHeaderList = new ArrayList<>();
        List<ComboModifier> comboModifierList = new ArrayList<>();
        List<QuestionGroup> questionGroupList = new ArrayList<>();
        List<QuestionHeader> questionHeaderList = new ArrayList<>();
        List<QuestionModifier> questionModifierList = new ArrayList<>();

        databaseHelper.cleanAllTables();

        try {
            Connection conn = ConnectionSingleton.getConnection();
            if(conn==null){
                Log.d(TAG, "doInBackground: connection null");
                return "connection";
            }
            String queryString="select "
                    + " dba.POSCATEGORY.ID as catID,"
                    + " dba.CATPOS.CAT_POS as sequence, "
                    + " dba.POSCATEGORY.DESCRIPT as catDescription,"
                    + " dba.PRODUCTS.POSDESCRIPT as productDescription,"
                    + " dba.PRODUCTS.PRICE as productPrice,"
                    + " dba.PRODUCTS.AVGCOST as avgCost,"
                    + " dba.PRODUCTS.LASTCOST as lastCost,"
                    + " dba.PRODUCTS.TAX1 as tax1,"
                    + " dba.PRODUCTS.TAX2 as tax2,"
                    + " dba.PRODUCTS.TAX3 as tax3,"
                    + " dba.PRODUCTS.PRODNUM as prodNum,"
                    + " dba.PRODUCTS.ISCOMBO as isCombo,"
                    + " dba.PRODUCTS.PRODUCTINFO as productInfo,"
                    + " dba.CATPOS.PICTURE as categoryHexPicture,"
                    + " dba.product_pictures.PICTURE as productHexPicture,"
                    + " dba.PRODUCTS.QGROUPID as qGID"
                    + " FROM dba.MENU INNER JOIN dba.CATPOS ON dba.MENU.ID = dba.CATPOS.MENUCODE"
                    + " INNER JOIN dba.PRODPOS on dba.PRODPOS.MENUCODE = dba.MENU.ID and dba.CATPOS.POSCATID=dba.PRODPOS.PROD_CAT"
                    + " INNER JOIN dba.PRODUCTS on dba.PRODUCTS.PRODNUM=dba.PRODPOS.PROD_NUM"
                    + " left JOIN dba.PRODUCT_PICTURES on dba.PRODUCT_PICTURES.ID"
                    + " in (  select top 1 p3.ID from product_pictures p3 where prodnum=dba.PRODUCTS.PRODNUM)"
                    + " INNER JOIN dba.POSCATEGORY ON dba.CATPOS.POSCATID = dba.POSCATEGORY.ID "
                    + " WHERE dba.MENU.ISACTIVE = 1 "
                    + " AND dba.MENU.ID = 3 AND dba.POSCATEGORY.ISACTIVE = 1  ORDER BY dba.CATPOS.CAT_POS ASC";
//            String queryString="select * from products";

            PreparedStatement query =
                    conn.prepareStatement(
                            queryString
                    );

            Log.d(TAG, "doInBackground: query "+queryString);
            ResultSet catRs = query.executeQuery();
            int lastCatId = -1;

            while (catRs.next()) {
                if (catRs.getInt("catId") != lastCatId) {
                    //new category
                    Categories category = new Categories(
                            catRs.getInt("catID"),
                            catRs.getString("catDescription"),
                            catRs.getString("categoryHexPicture"),
                            0,
                            catRs.getInt("sequence"));
                    Log.d(TAG, "doInBackground: category " + category.getSeq() + " " + category.getDescription());
                    categoriesList.add(category);
                    lastCatId = category.getCatId();
                }

                Products products = new Products(catRs.getInt("prodNum"),
                        lastCatId,
                        catRs.getInt("qGID"),
                        0,
                        catRs.getString("productDescription"),
                        catRs.getString("productInfo"),
                        catRs.getString("productHexPicture"),
                        catRs.getDouble("productPrice"),
                        catRs.getDouble("avgCost"),
                        catRs.getDouble("lastCost"),
                        catRs.getDouble("tax1"),
                        catRs.getDouble("tax2"),
                        catRs.getDouble("tax3"),
                        catRs.getDouble("productPrice"));

                int totalTaxes = 0;
                if (products.getTax3() == 1) {
                    products.setTax3(GetTaxes.getTaxAmount(products.getPrice(), 3));
                    totalTaxes += products.getTax3();
                }
                if (products.getTax2() == 1) {
                    products.setTax2(GetTaxes.getTaxAmount(products.getPrice(), 2));
                    totalTaxes += products.getTax2();
                }
                if (products.getTax1() == 1) {
                    products.setTax1(GetTaxes.getTaxAmount(products.getPrice(), 1));
                    totalTaxes += products.getTax1();
                }
                products.setTaxEx(products.getPrice() - totalTaxes);
                Log.d(TAG, "doInBackground: product " + products.getDescription() + ", price :" + products.getPrice() + ", tax1 " + products.getTax1() + ", tax ex " + products.getTaxEx());

                if (catRs.getInt("isCombo") == 1) {
                    Log.d(TAG, "doInBackground: get combo");
                    products.setIsCombo(1);
                    //get combo group id
                    PreparedStatement query1 = conn.prepareStatement(
                            " select COMBOID as comboGroupId"
                                    + " from dba.PRODUCT_COMBO"
                                    + " where PRODUCT_COMBO.PRODNUM=" + products.getProdNum());
                    ResultSet comboGroupRs = query1.executeQuery();

                    while (comboGroupRs.next()) {
                        ComboGroup comboGroup = new ComboGroup(
                                comboGroupRs.getInt("comboGroupId"),
                                products.getProdNum());

                        comboGroupList.add(comboGroup);

                        //get combo header id
                        PreparedStatement query2 = conn.prepareStatement(
                                "select ID as comboHeaderId ,"
                                        + " DESCRIPT,"
                                        + " MIN,"
                                        + " MAX,"
                                        + " REQUIRED "
                                        + " from dba.COMBOHEADER "
                                        + " where ISACTIVE =1 "
                                        + " and "
                                        + " dba.COMBOHEADER.ID=" + comboGroupRs.getInt("comboGroupId"));
                        ResultSet comboHeaderRs = query2.executeQuery();

                        while (comboHeaderRs.next()) {
                            ComboHeader comboHeader = new ComboHeader(comboGroup.getComboGroupId(),
                                    comboHeaderRs.getInt("comboHeaderId"),
                                    comboHeaderRs.getInt("MIN"),
                                    comboHeaderRs.getInt("MAX"),
                                    comboHeaderRs.getInt("REQUIRED"),
                                    comboHeaderRs.getString("DESCRIPT"));
//                            Log.d(TAG, "doInBackground: combo header " + comboHeader.toString());
                            comboHeaderList.add(comboHeader);

                            //get combo modifier id
                            PreparedStatement query3 = conn.prepareStatement("select "
                                    + " PRODUCTS.DESCRIPT,"
                                    + " PRODUCTS.PRICE,"
                                    + " PRODUCTS.TAX1,"
                                    + " PRODUCTS.TAX2,"
                                    + " PRODUCTS.TAX3,"
                                    + " PRODUCTS.AVGCOST,"
                                    + " PRODUCTS.LASTCOST,"
                                    + " PRODUCTS.PRODNUM,"
                                    + " SEQ"
                                    + " from dba.PRODUCTS"
                                    + " INNER JOIN dba.COMBODETAIL on PRODUCTS.PRODNUM = COMBODETAIL.PRODNUM"
                                    + " where COMBODETAIL.COMBOID =" + comboHeaderRs.getInt("comboHeaderId")
                                    + " order by COMBOID ASC");
                            ResultSet comboModifierRs = query3.executeQuery();

                            while (comboModifierRs.next()) {
                                ComboModifier comboModifier = new ComboModifier(
                                        comboModifierRs.getInt("PRODNUM"),
                                        comboHeader.getComboGroupId(),
                                        comboHeader.getComboHeaderId(),
                                        comboModifierRs.getInt("PRICE"),
                                        comboModifierRs.getInt("TAX1"),
                                        comboModifierRs.getInt("TAX2"),
                                        comboModifierRs.getInt("TAX3"),
                                        comboModifierRs.getInt("PRICE"),
                                        comboModifierRs.getInt("AVGCOST"),
                                        comboModifierRs.getInt("LASTCOST"),
                                        comboModifierRs.getInt("SEQ"),
                                        comboModifierRs.getString("DESCRIPT"));
                                totalTaxes = 0;
                                if (comboModifier.getTax3() == 1) {
                                    comboModifier.setTax3(GetTaxes.getTaxAmount(comboModifier.getPrice(), 3));
                                    totalTaxes += comboModifier.getTax3();
                                }
                                if (comboModifier.getTax2() == 1) {
                                    comboModifier.setTax2(GetTaxes.getTaxAmount(comboModifier.getPrice(), 2));
                                    totalTaxes += comboModifier.getTax2();
                                }
                                if (comboModifier.getTax1() == 1) {
                                    comboModifier.setTax1(GetTaxes.getTaxAmount(comboModifier.getPrice(), 1));
                                    totalTaxes += comboModifier.getTax1();
                                }
                                comboModifier.setTaxEx(comboModifier.getPrice() - totalTaxes);
                                Log.d(TAG, "doInBackground: combo " + comboModifier.getDescription() + ", price :" + comboModifier.getPrice() + ", tax1 " + comboModifier.getTax1() + ", tax ex " + comboModifier.getTaxEx());
                                comboModifierList.add(comboModifier);
                            }
                            comboModifierRs.close();
                            query3.close();
                        }
                        comboHeaderRs.close();
                        query2.close();
                    }
                    comboGroupRs.close();
                    query1.close();
                }

                if (products.getQuestionGroupId() != 0) {
                    //get question group
//                    Log.d(TAG, "doInBackground: get question");
                    QuestionGroup questionGroup = new QuestionGroup(products.getQuestionGroupId());
//                    Log.d(TAG, "doInBackground: question group " + questionGroup.toString());
                    questionGroupList.add(questionGroup);

                    //get question headers
                    PreparedStatement query5 = conn.prepareStatement(
                            " select QUESTIONHEADER.DESCRIPT,"
                                    + " QUESTIONGROUPDETAIL.HEADERID as questionHeaderId,"
                                    + " QUESTIONHEADER.REQUIRED,"
                                    + " QUESTIONHEADER.MIN,"
                                    + " QUESTIONHEADER.MAX,"
                                    + " SEQ"
                                    + " from dba.QUESTIONHEADER "
                                    + " inner join dba.QUESTIONGROUPDETAIL on QUESTIONHEADER.ID = dba.QUESTIONGROUPDETAIL.HEADERID"
                                    + " where questiongroupdetail.ID=" + questionGroup.getQuestionGroupId()
                                    + " and QUESTIONHEADER.ISACTIVE=1");

                    ResultSet questionHeaderRs = query5.executeQuery();
                    while (questionHeaderRs.next()) {

                        QuestionHeader questionHeader = new QuestionHeader(
                                questionGroup.getQuestionGroupId(),
                                questionHeaderRs.getInt("questionHeaderId"),
                                questionHeaderRs.getInt("MIN"),
                                questionHeaderRs.getInt("MAX"),
                                questionHeaderRs.getInt("REQUIRED"),
                                questionHeaderRs.getInt("SEQ"),
                                questionHeaderRs.getString("DESCRIPT"));
//                        Log.d(TAG, "doInBackground: question header " + questionHeader.toString());
                        questionHeaderList.add(questionHeader);

                        //get question modifiers
                        PreparedStatement query6 = conn.prepareStatement(
                                "select DESCRIPT,PRICE,PRODNUM,TAX1,TAX2,TAX3,AVGCOST,LASTCOST "
                                        + " FROM dba.PRODUCTS "
                                        + " where PRODUCTS.PRODNUM in "
                                        + " ( select PRODNUM from dba.QUESTIONDETAIL where HEADERID = " + questionHeader.getQuestionHeaderId() + ")");

                        ResultSet questionModifierRs = query6.executeQuery();
                        while (questionModifierRs.next()) {
                            QuestionModifier questionModifier = new QuestionModifier(
                                    questionModifierRs.getInt("PRODNUM"),
                                    questionHeader.getQuestionGroupId(),
                                    questionHeader.getQuestionHeaderId(),
                                    questionModifierRs.getInt("PRICE"),
                                    questionModifierRs.getInt("TAX1"),
                                    questionModifierRs.getInt("TAX2"),
                                    questionModifierRs.getInt("TAX3"),
                                    questionModifierRs.getInt("PRICE"),
                                    questionModifierRs.getInt("AVGCOST"),
                                    questionModifierRs.getInt("LASTCOST"),
                                    questionModifierRs.getString("DESCRIPT"));

                            totalTaxes = 0;
                            if (questionModifier.getTax3() == 1) {
                                questionModifier.setTax3(GetTaxes.getTaxAmount(questionModifier.getPrice(), 3));
                                totalTaxes += questionModifier.getTax3();
                            }
                            if (questionModifier.getTax2() == 1) {
                                questionModifier.setTax2(GetTaxes.getTaxAmount(questionModifier.getPrice(), 2));
                                totalTaxes += questionModifier.getTax2();
                            }
                            if (questionModifier.getTax1() == 1) {
                                questionModifier.setTax1(GetTaxes.getTaxAmount(questionModifier.getPrice(), 1));
                                totalTaxes += questionModifier.getTax1();
                            }
                            questionModifier.setTaxEx(questionModifier.getPrice() - totalTaxes);
                            Log.d(TAG, "doInBackground: questionModifier " + questionModifier.getDescription() + ", price :" + questionModifier.getPrice() + ", tax1 " + questionModifier.getTax1() + ", tax ex " + questionModifier.getTaxEx());
                            questionModifierList.add(questionModifier);
                        }
                        questionModifierRs.close();
                        query6.close();
                    }

                    questionHeaderRs.close();
                    query5.close();
                }

                productsList.add(products);
            }

            catRs.close();
            query.close();

            Log.d(TAG, "doInBackground: category list size "+categoriesList.size());
            Log.d(TAG, "doInBackground: product list size "+productsList.size());

            if(categoriesList.size()==0){
                return "products";
            }
            databaseHelper.insertCategoryList(categoriesList);
            databaseHelper.insertProductList(productsList);
            databaseHelper.insertComboGroupList(comboGroupList);
            databaseHelper.insertComboHeaderList(comboHeaderList);
            databaseHelper.insertComboModifierList(comboModifierList);

            databaseHelper.insertQuestionGroupList(questionGroupList);
            databaseHelper.insertQuestionHeaderList(questionHeaderList);
            databaseHelper.insertQuestionModifierList(questionModifierList);

            return "success";

        } catch (SQLException e) {
            Log.d(TAG, "doInBackground: error " + e.getMessage());
            return "data";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        if (result.equalsIgnoreCase("error")) {
            listener.onGetAllProductsComplete(0);
        } else if (result.equalsIgnoreCase("success")){
            listener.onGetAllProductsComplete(1);
        }else if (result.equalsIgnoreCase("connection")){
            listener.onGetAllProductsComplete(-1);
        }else{
            listener.onGetAllProductsComplete(-2);
        }
    }
}