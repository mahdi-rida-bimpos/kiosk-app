package com.bimpos.newkioskapp.models;

import java.io.Serializable;
import java.util.List;

public class CartProduct implements Serializable {

    private final int cartId;
    private int prodNum;
    private final String prod_desc;
    private int qty;
    private final String remark;
    private double price;
    private double tax1;
    private double tax2;
    private double tax3;
    private double taxEx;
    private double avgCost;
    private double lastCost;
    private int comboGroupId;
    private int itemType;
    private final int questionGroupId;
    private final List<CartQuestionHeader> questionList;
    private final List<CartComboHeader> comboList;


    public CartProduct(int cartId, int prodNum, String prod_desc, int qty, String remark, double price, double tax1, double tax2, double tax3, double taxEx ,double avgCost,double lastCost,int comboGroupId, int questionGroupId, List<CartQuestionHeader> questionList, List<CartComboHeader> comboList) {
        this.cartId = cartId;
        this.prodNum = prodNum;
        this.prod_desc = prod_desc;
        this.qty = qty;
        this.remark = remark;
        this.price = price;
        this.comboGroupId = comboGroupId;
        this.questionGroupId = questionGroupId;
        this.questionList = questionList;
        this.comboList = comboList;
        this.tax1 = tax1;
        this.tax2 = tax2;
        this.tax3 = tax3;
        this.taxEx = taxEx;
        this.avgCost=avgCost;
        this.lastCost=lastCost;
    }

    public int getCartId() {
        return cartId;
    }

    public int getProdNum() {
        return prodNum;
    }

    public void setProdNum(int prodNum) {
        this.prodNum = prodNum;
    }

    public String getProd_desc() {
        return prod_desc;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getRemark() {
        return remark;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTax1() {
        return tax1;
    }

    public void setTax1(double tax1) {
        this.tax1 = tax1;
    }

    public double getTax2() {
        return tax2;
    }

    public void setTax2(double tax2) {
        this.tax2 = tax2;
    }

    public double getTax3() {
        return tax3;
    }

    public void setTax3(double tax3) {
        this.tax3 = tax3;
    }

    public double getTaxEx() {
        return taxEx;
    }

    public void setTaxEx(double taxEx) {
        this.taxEx = taxEx;
    }

    public double getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(double avgCost) {
        this.avgCost = avgCost;
    }

    public double getLastCost() {
        return lastCost;
    }

    public void setLastCost(double lastCost) {
        this.lastCost = lastCost;
    }

    public int getComboGroupId() {
        return comboGroupId;
    }

    public void setComboGroupId(int comboGroupId) {
        this.comboGroupId = comboGroupId;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public int getQuestionGroupId() {
        return questionGroupId;
    }

    public List<CartQuestionHeader> getQuestionList() {
        return questionList;
    }

    public List<CartComboHeader> getComboList() {
        return comboList;
    }
}
