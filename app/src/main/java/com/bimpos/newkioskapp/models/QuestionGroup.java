package com.bimpos.newkioskapp.models;

public class QuestionGroup {

    private final int questionGroupId;

    public QuestionGroup(int qGId) {
        this.questionGroupId = qGId;
    }

    public int getQuestionGroupId() {
        return questionGroupId;
    }

    @Override
    public String toString() {
        return "QuestionGroup{" +
                "questionGroupId=" + questionGroupId +
                '}';
    }
}
