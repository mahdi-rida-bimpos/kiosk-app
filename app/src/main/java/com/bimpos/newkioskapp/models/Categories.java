package com.bimpos.newkioskapp.models;

import java.io.Serializable;

public class Categories implements Serializable {

    private int catId;
    private String description;
    private String hexPicture;
    private int selected,seq;

    public Categories(int catId, String descript, String hexPicture, int selected,int seq) {
        this.catId = catId;
        this.seq = seq;
        this.description = descript;
        this.hexPicture = hexPicture;
        this.selected = selected;
    }

    public Categories() {
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHexPicture() {
        return hexPicture;
    }

    public void setHexPicture(String hexPicture) {
        this.hexPicture = hexPicture;
    }

    public int isSelected() {
        return selected;
    }

    public int getSelected() {
        return selected;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return "Categories{" +
                "catId=" + catId +
                ", description='" + description + '\'' +
                ", seq=" + seq +
                '}';
    }
}
