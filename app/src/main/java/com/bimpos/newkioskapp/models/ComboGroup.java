package com.bimpos.newkioskapp.models;

public class ComboGroup {

    private int comboGroupId,prodNum;

    public ComboGroup(int comboGroupId, int prodNum) {
        this.comboGroupId = comboGroupId;
        this.prodNum = prodNum;
    }

    public int getComboGroupId() {
        return comboGroupId;
    }

    public void setComboGroupId(int comboGroupId) {
        this.comboGroupId = comboGroupId;
    }

    public int getProdNum() {
        return prodNum;
    }

    public void setProdNum(int prodNum) {
        this.prodNum = prodNum;
    }

    @Override
    public String toString() {
        return "ComboGroup{" +
                "comboGroupId=" + comboGroupId +
                ", prodNum=" + prodNum +
                '}';
    }
}
