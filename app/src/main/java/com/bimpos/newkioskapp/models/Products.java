package com.bimpos.newkioskapp.models;

import java.io.Serializable;
import java.math.BigDecimal;

public class Products implements Serializable {

    private int prodNum, catId, questionGroupId, isCombo, comboGroupId;
    private String description, prodInfo, hexPicture;
    private double price, avgCost, lastCost, tax1, tax2, tax3, taxEx;

    public Products(int prodNum, int catId, int questionGroupId, int isCombo, String description, String prodInfo, String hexPicture,
                    double price, double avgCost, double lastCost, double tax1, double tax2, double tax3, double taxEx) {
        this.prodNum = prodNum;
        this.catId = catId;
        this.questionGroupId = questionGroupId;
        this.isCombo = isCombo;
        this.description = description;
        this.prodInfo = prodInfo;
        this.hexPicture = hexPicture;
        this.price = price;
        this.avgCost = avgCost;
        this.lastCost = lastCost;
        this.tax1 = tax1;
        this.tax2 = tax2;
        this.tax3 = tax3;
        this.taxEx = taxEx;
    }


    public Products() {
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(double avgCost) {
        this.avgCost = avgCost;
    }

    public double getLastCost() {
        return lastCost;
    }

    public void setLastCost(double lastCost) {
        this.lastCost = lastCost;
    }

    public double getTax1() {
        return tax1;
    }

    public void setTax1(double tax1) {
        this.tax1 = tax1;
    }

    public double getTax2() {
        return tax2;
    }

    public void setTax2(double tax2) {
        this.tax2 = tax2;
    }

    public double getTax3() {
        return tax3;
    }

    public void setTax3(double tax3) {
        this.tax3 = tax3;
    }

    public double getTaxEx() {
        return taxEx;
    }

    public void setTaxEx(double taxEx) {
        this.taxEx = taxEx;
    }

    public int getComboGroupId() {
        return comboGroupId;
    }

    public void setComboGroupId(int comboGroupId) {
        this.comboGroupId = comboGroupId;
    }

    public int getProdNum() {
        return prodNum;
    }

    public void setProdNum(int prodNum) {
        this.prodNum = prodNum;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public int getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(int questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public int getIsCombo() {
        return isCombo;
    }

    public void setIsCombo(int isCombo) {
        this.isCombo = isCombo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProdInfo() {
        return prodInfo;
    }

    public void setProdInfo(String prodInfo) {
        this.prodInfo = prodInfo;
    }

    public String getHexPicture() {
        return hexPicture;
    }

    public void setHexPicture(String hexPicture) {
        this.hexPicture = hexPicture;
    }
}
