package com.bimpos.newkioskapp.models;

public class CartComboModifier {

    private String description;
    private int count;
    private final int modifierId;
    private double price, tax1, tax2, tax3, taxEx, avgCost, lastCost;

    public CartComboModifier(int modifierId, String description, int count, double price, double tax1, double tax2, double tax3, double taxEx, double avgCost, double lastCost) {
        this.description = description;
        this.count = count;
        this.price = price;
        this.modifierId = modifierId;
        this.tax1 = tax1;
        this.tax2 = tax2;
        this.tax3 = tax3;
        this.taxEx = taxEx;
        this.avgCost = avgCost;
        this.lastCost = lastCost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getModifierId() {
        return modifierId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTax1() {
        return tax1;
    }

    public void setTax1(double tax1) {
        this.tax1 = tax1;
    }

    public double getTax2() {
        return tax2;
    }

    public void setTax2(double tax2) {
        this.tax2 = tax2;
    }

    public double getTax3() {
        return tax3;
    }

    public void setTax3(double tax3) {
        this.tax3 = tax3;
    }

    public double getTaxEx() {
        return taxEx;
    }

    public void setTaxEx(double taxEx) {
        this.taxEx = taxEx;
    }

    public double getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(double avgCost) {
        this.avgCost = avgCost;
    }

    public double getLastCost() {
        return lastCost;
    }

    public void setLastCost(double lastCost) {
        this.lastCost = lastCost;
    }
}
