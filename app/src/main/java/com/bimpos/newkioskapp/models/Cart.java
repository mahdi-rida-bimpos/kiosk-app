package com.bimpos.newkioskapp.models;


import android.util.Log;

import com.bimpos.newkioskapp.Helpers.Constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Cart {

    private List<CartProduct> cartItemsList = new ArrayList<>();
    private static Cart cartData = null;
    private int orderType;
    private static final String TAG = "Cart";

    public static Cart getCartData() {
        if (cartData == null) {
            cartData = new Cart();
        }
        return cartData;
    }

    public int getOrderType() {
        return orderType;
    }

    public void setOrderType(int orderType) {
        this.orderType = orderType;
    }

    public void setCartItemsList(List<CartProduct> cartItemsList) {
        this.cartItemsList = cartItemsList;
    }

    public void addProductWithModifiers(Products products, String remark, List<CartQuestionHeader> cartQuestionHeaderList, List<CartComboHeader> cartComboHeaderList, int qty) {

        cartItemsList.add(new CartProduct(
                Constant.cartId,
                products.getProdNum(),
                products.getDescription(),
                qty,
                remark,
                products.getPrice(),
                products.getTax1(),
                products.getTax2(),
                products.getTax3(),
                products.getTaxEx(),
                products.getAvgCost(),
                products.getLastCost(),
                products.getComboGroupId(),
                products.getQuestionGroupId(),
                cartQuestionHeaderList,
                cartComboHeaderList
        ));
        Constant.cartId++;
    }

    public void addItem(int cartId) {
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItems = cartItemsList.get(i);
            if (cartItems.getCartId() == cartId) {
                int qty = cartItems.getQty();
                qty++;
                cartItems.setQty(qty);
                return;
            }
        }
    }

    public void removeItem(int cartId) {
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItems = cartItemsList.get(i);
            if (cartItems.getCartId() == cartId) {
                int qty = cartItems.getQty();
                if (qty > 1) {
                    qty--;
                    cartItems.setQty(qty);
                    return;
                }
                cartItemsList.remove(cartItems);
                return;
            }
        }
    }

    public int getProductCount(int prodNum) {
        int qty = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            if (cartItem.getProdNum() == prodNum) {
                qty += cartItem.getQty();
            }
        }
        return qty;
    }

    public int getProductCountInCart(int prodNum) {
        int qty = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            if (cartItem.getProdNum() == prodNum) {
                qty++;
            }
        }
        return qty;
    }

    public List<CartProduct> getCartByProductId(int prodNum) {
        List<CartProduct> productList = new ArrayList<>();
        for (int i = 0; i < cartItemsList.size(); i++) {
            if (cartItemsList.get(i).getProdNum() == prodNum) {
                productList.add(cartItemsList.get(i));
            }
        }
        return productList;
    }

    public int getCartProductCount(int cartId) {
        int qty = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            if (cartItem.getCartId() == cartId) {
                qty += cartItem.getQty();
            }
        }
        return qty;
    }

    public void modifyCartProducts(HashMap<Integer, Integer> list) {
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartProduct = cartItemsList.get(i);
            int cartId = cartProduct.getCartId();
            if (list.containsKey(cartId) && list.get(cartId) != null) {
                if (list.get(cartId) == 0) {
                    cartItemsList.remove(cartProduct);
                } else {
                    cartItemsList.get(i).setQty(list.get(cartId));
                }

            }
        }
    }

    public int getCartCount() {
        int count = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            count += cartItem.getQty();
        }
        return count;
    }

    public int getCartTotalPrice() {
        int price = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            price += (cartItem.getPrice() * cartItem.getQty());

            if (null != cartItem.getQuestionList() && cartItem.getQuestionList().size() != 0) {
                for (int ii = 0; ii < cartItem.getQuestionList().size(); ii++) {
                    CartQuestionHeader cartQuestionHeader = cartItem.getQuestionList().get(ii);
                    for (int iii = 0; iii < cartQuestionHeader.getCartQuestionModifiers().size(); iii++) {
                        CartQuestionModifier modifier = cartQuestionHeader.getCartQuestionModifiers().get(iii);
                        price += modifier.getPrice() * cartItem.getQty();
                    }
                }
            }
            if (null != cartItem.getComboList() && cartItem.getComboList().size() != 0) {
                for (int ii = 0; ii < cartItem.getComboList().size(); ii++) {
                    CartComboHeader cartComboHeader = cartItem.getComboList().get(ii);
                    for (int iii = 0; iii < cartComboHeader.getCartComboModifiers().size(); iii++) {
                        CartComboModifier modifier = cartComboHeader.getCartComboModifiers().get(iii);
                        if(modifier.getPrice()>0){
                            price += modifier.getPrice() * cartItem.getQty() * modifier.getCount();
                        }
                    }
                }
            }
        }
        return price;
    }

    public List<CartProduct> getCartItemsList() {
        return cartItemsList;
    }

    public void clear() {
        cartItemsList.clear();
    }

    public int getTax1(){
        Log.d(TAG, "getTax1: start");
        int tax = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            tax += cartItem.getTax1()*cartItem.getQty();
            Log.d(TAG, "getTax1: product, "+cartItem.getProd_desc()+" tax 1 "+cartItem.getTax1());

            if (null != cartItem.getQuestionList() && cartItem.getQuestionList().size() != 0) {
                for (int ii = 0; ii < cartItem.getQuestionList().size(); ii++) {
                    CartQuestionHeader cartQuestionHeader = cartItem.getQuestionList().get(ii);
                    for (int iii = 0; iii < cartQuestionHeader.getCartQuestionModifiers().size(); iii++) {
                        CartQuestionModifier modifier = cartQuestionHeader.getCartQuestionModifiers().get(iii);
                        if(modifier.getPrice()>0){
                            tax += modifier.getTax1()*cartItem.getQty();
                            Log.d(TAG, "getTax1: modifier tax1 "+modifier.getTax1());
                        }
                    }
                }
            }
            if (null != cartItem.getComboList() && cartItem.getComboList().size() != 0) {
                for (int ii = 0; ii < cartItem.getComboList().size(); ii++) {
                    CartComboHeader cartComboHeader = cartItem.getComboList().get(ii);
                    for (int iii = 0; iii < cartComboHeader.getCartComboModifiers().size(); iii++) {
                        CartComboModifier modifier = cartComboHeader.getCartComboModifiers().get(iii);
                        if(modifier.getPrice()>0){
                            tax += modifier.getTax1()*cartItem.getQty();
                            Log.d(TAG, "getTax1: combo tax1 "+modifier.getTax1());
                        }
                    }
                }
            }
        }
        Log.d(TAG, "getTax1: total tax1 "+tax);
        return tax;
    }
    public int getTax2(){
        int tax = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            tax += cartItem.getTax2()*cartItem.getQty();;

            if (null != cartItem.getQuestionList() && cartItem.getQuestionList().size() != 0) {
                for (int ii = 0; ii < cartItem.getQuestionList().size(); ii++) {
                    CartQuestionHeader cartQuestionHeader = cartItem.getQuestionList().get(ii);
                    for (int iii = 0; iii < cartQuestionHeader.getCartQuestionModifiers().size(); iii++) {
                        CartQuestionModifier modifier = cartQuestionHeader.getCartQuestionModifiers().get(iii);
                        if(modifier.getPrice()>0){
                            tax += modifier.getTax2()*cartItem.getQty();;
                        }
                    }
                }
            }
            if (null != cartItem.getComboList() && cartItem.getComboList().size() != 0) {
                for (int ii = 0; ii < cartItem.getComboList().size(); ii++) {
                    CartComboHeader cartComboHeader = cartItem.getComboList().get(ii);
                    for (int iii = 0; iii < cartComboHeader.getCartComboModifiers().size(); iii++) {
                        CartComboModifier modifier = cartComboHeader.getCartComboModifiers().get(iii);
                        if(modifier.getPrice()>0){
                            tax += modifier.getTax2()*cartItem.getQty();;
                        }
                    }
                }
            }
        }
        return tax;
    }
    public int getTax3(){
        int tax = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            tax += cartItem.getTax3()*cartItem.getQty();;

            if (null != cartItem.getQuestionList() && cartItem.getQuestionList().size() != 0) {
                for (int ii = 0; ii < cartItem.getQuestionList().size(); ii++) {
                    CartQuestionHeader cartQuestionHeader = cartItem.getQuestionList().get(ii);
                    for (int iii = 0; iii < cartQuestionHeader.getCartQuestionModifiers().size(); iii++) {
                        CartQuestionModifier modifier = cartQuestionHeader.getCartQuestionModifiers().get(iii);
                        if(modifier.getPrice()>0){
                            tax += modifier.getTax3()*cartItem.getQty();;
                        }
                    }
                }
            }
            if (null != cartItem.getComboList() && cartItem.getComboList().size() != 0) {
                for (int ii = 0; ii < cartItem.getComboList().size(); ii++) {
                    CartComboHeader cartComboHeader = cartItem.getComboList().get(ii);
                    for (int iii = 0; iii < cartComboHeader.getCartComboModifiers().size(); iii++) {
                        CartComboModifier modifier = cartComboHeader.getCartComboModifiers().get(iii);
                        if(modifier.getPrice()>0){
                            tax += modifier.getTax3()*cartItem.getQty();;
                        }
                    }
                }
            }
        }
        return tax;
    }
    public int getTaxEx(){
        Log.d(TAG, "getTaxEx: start");
        int taxEx = 0;
        for (int i = 0; i < cartItemsList.size(); i++) {
            CartProduct cartItem = cartItemsList.get(i);
            taxEx += cartItem.getTaxEx()*cartItem.getQty();
            Log.d(TAG, "getTaxEx: product,"+cartItem.getProd_desc()+" taxex "+taxEx);

            if (null != cartItem.getQuestionList() && cartItem.getQuestionList().size() != 0) {
                for (int ii = 0; ii < cartItem.getQuestionList().size(); ii++) {
                    CartQuestionHeader cartQuestionHeader = cartItem.getQuestionList().get(ii);
                    for (int iii = 0; iii < cartQuestionHeader.getCartQuestionModifiers().size(); iii++) {
                        CartQuestionModifier modifier = cartQuestionHeader.getCartQuestionModifiers().get(iii);
                        if(modifier.getPrice()>0){
                            taxEx += modifier.getTaxEx()*cartItem.getQty();
                            Log.d(TAG, "getTaxEx: modifier taxex "+modifier.getTaxEx());
                        }
                    }
                }
            }
            if (null != cartItem.getComboList() && cartItem.getComboList().size() != 0) {
                for (int ii = 0; ii < cartItem.getComboList().size(); ii++) {
                    CartComboHeader cartComboHeader = cartItem.getComboList().get(ii);
                    for (int iii = 0; iii < cartComboHeader.getCartComboModifiers().size(); iii++) {
                        CartComboModifier modifier = cartComboHeader.getCartComboModifiers().get(iii);
                        if(modifier.getPrice()>0){
                            taxEx += modifier.getTaxEx()*cartItem.getQty();
                            Log.d(TAG, "getTaxEx: modifier taxex "+modifier.getTaxEx());
                        }
                    }
                }
            }
        }
        Log.d(TAG, "getTaxEx: final taxex "+taxEx);
        return taxEx;
    }
}


