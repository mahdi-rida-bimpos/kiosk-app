package com.bimpos.newkioskapp.models;

public class ComboModifier {

    private int prodNum, comboGroupId, comboHeaderId, seq;
    private double price,tax1, tax2, tax3, taxEx,avgCost,lastCost;
    private String description;

    public ComboModifier(int prodNum, int comboGroupId, int comboHeaderId, double price, double tax1, double tax2, double tax3, double taxEx,double avgCost,double lastCost,int seq, String description) {
        this.prodNum = prodNum;
        this.seq = seq;
        this.comboHeaderId = comboHeaderId;
        this.price = price;
        this.comboGroupId = comboGroupId;
        this.description = description;
        this.tax1 = tax1;
        this.tax2 = tax2;
        this.tax3 = tax3;
        this.avgCost = avgCost;
        this.lastCost = lastCost;
        this.taxEx = taxEx;
    }

    public int getProdNum() {
        return prodNum;
    }

    public void setProdNum(int prodNum) {
        this.prodNum = prodNum;
    }

    public int getComboGroupId() {
        return comboGroupId;
    }

    public void setComboGroupId(int comboGroupId) {
        this.comboGroupId = comboGroupId;
    }

    public int getComboHeaderId() {
        return comboHeaderId;
    }

    public void setComboHeaderId(int comboHeaderId) {
        this.comboHeaderId = comboHeaderId;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTax1() {
        return tax1;
    }

    public void setTax1(double tax1) {
        this.tax1 = tax1;
    }

    public double getTax2() {
        return tax2;
    }

    public void setTax2(double tax2) {
        this.tax2 = tax2;
    }

    public double getTax3() {
        return tax3;
    }

    public void setTax3(double tax3) {
        this.tax3 = tax3;
    }

    public double getTaxEx() {
        return taxEx;
    }

    public void setTaxEx(double taxEx) {
        this.taxEx = taxEx;
    }

    public double getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(double avgCost) {
        this.avgCost = avgCost;
    }

    public double getLastCost() {
        return lastCost;
    }

    public void setLastCost(double lastCost) {
        this.lastCost = lastCost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
