package com.bimpos.newkioskapp.models;

import java.util.ArrayList;
import java.util.List;

public class ComboHeader {

    private final int comboGroupId;
    private final int comboHeaderId;
    private final int min;
    private final int max;
    private final int required;
    private final String description;
    private List<ComboModifier> modifiersList;

    public ComboHeader(int comboGroupId, int comboHeaderId, int min, int max, int required, String description) {
        this.comboGroupId = comboGroupId;
        this.comboHeaderId = comboHeaderId;
        this.min = min;
        this.max = max;
        this.required = required;
        this.description = description;
        modifiersList = new ArrayList<>();
    }

    public int getComboGroupId() {
        return comboGroupId;
    }

    public int getComboHeaderId() {
        return comboHeaderId;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public int getRequired() {
        return required;
    }

    public String getDescription() {
        return description;
    }

    public List<ComboModifier> getModifiersList() {
        return modifiersList;
    }

    public void setModifiersList(List<ComboModifier> modifiersList) {
        this.modifiersList = modifiersList;
    }

    @Override
    public String toString() {
        return "ComboHeader{" +
                "comboGroupId=" + comboGroupId +
                ", comboHeaderId=" + comboHeaderId +
                ", min=" + min +
                ", max=" + max +
                ", required=" + required +
                ", description='" + description + '\'' +
                ", modifiersList=" + modifiersList +
                '}';
    }
}
